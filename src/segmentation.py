import utils
import pandas as pd
import numpy as np
import datetime as dt
import calendar


ACC_TH_PATH = '../data/masterdata/Acc thresholds.csv'
RESULT_FILE_PATH = r'../results/outputs/UplifttoolOutput_segment.csv'
EVALS_FILE_PATH = r'../results/outputs/Evals_segment.csv'


def get_latest_result_and_prev_fc_metric(inno_keys, chess):
    """
    The function reads the latest forecast (from output file UplifttoolOutput.csv) for every pair (key, date) and aggregates it to the
    forecast for every quadruple (Chain, Sku/линейка, FT_SHIP_START, SLOT_START).
    
    :return: pd.DataFrame with forecast in corresponding format and result of get_prev_fc_metrics() call
    """
    columns = ['Date', 'key', 'Ordered', 'Partition', 'SHIP_START', 'FT_SHIP_START', 'Forecast', 'Discount on date', 'price_discount', 'seasonal']
    
    # condition on isin 'Metro_traiders' in column
    try:
        res = pd.read_csv('../results/outputs/UplifttoolOutput.csv', usecols = columns + ['Metro_traiders'])
        res['Metro_traiders'] = res['Metro_traiders'].apply(lambda x: str(x))
    except:
        res = pd.read_csv('../results/outputs/UplifttoolOutput.csv', usecols = columns)
    #res = pd.read_csv('../results/outputs/UplifttoolOutput.csv', usecols = columns)

    # drop inno
    res = res[~res['key'].isin(inno_keys)]

    res['Date'] = pd.to_datetime(res['Date'], format='%Y-%m-%d')
    res['FT_SHIP_START'] = np.where(res['Partition']=='train', res['SHIP_START'], res['FT_SHIP_START'])

    # condition isin 'Metro_traiders' in column
    try:
        segment_res = pd.read_csv(RESULT_FILE_PATH, usecols=['Date', 'key', 'Forecast', 'Partition', 'Metro_traiders'])
        segment_res['Metro_traiders'] = segment_res['Metro_traiders'].apply(lambda x: str(x))
    except:
        segment_res = pd.read_csv(RESULT_FILE_PATH, usecols=['Date', 'key', 'Forecast', 'Partition'])
    #segment_res = pd.read_csv(RESULT_FILE_PATH, usecols=['Date', 'key', 'Forecast', 'Partition'])
        
    segment_res.rename(columns={'Forecast': 'Prev_FC', 'Partition':'Seg_Partition'}, inplace=True)
    segment_res['Date'] = pd.to_datetime(segment_res['Date'], format='%Y-%m-%d')
    
    # condition for metro traiders slots for split
    if ('Metro_traiders' not in segment_res.columns):
        segment_res['Metro_traiders'] = 'False'
    else:
        segment_res.loc[segment_res['Metro_traiders']=='0', 'Metro_traiders'] = 'False'
    if ('Metro_traiders' not in res.columns):
        res['Metro_traiders'] = 'False'
    else:
        res.loc[res['Metro_traiders']=='0', 'Metro_traiders'] = 'False'

    # add metro_traiders for merge slots
    res = res.merge(segment_res, how='left', on=['key', 'Date', 'Metro_traiders'])
    #res = res.merge(segment_res, how='left', on=['key', 'Date'])
    res['Prev_FC'].fillna(0, inplace=True)

    res = utils.add_cat_cust(res, chess=chess)

    cols_for_metric = ['key', 'Seg_Partition', 'SHIP_START', 'FT_SHIP_START', 'Date',
                       'Ordered', 'Prev_FC']
    prev_fc = res[cols_for_metric].copy()
    prev_fc.rename(columns={'Prev_FC': 'Forecast', 'Seg_Partition':'Partition'}, inplace=True)
    prev_fc['FT_SHIP_START'] = pd.to_datetime(prev_fc['FT_SHIP_START'], errors = 'coerce')
    prev_fc['FT_SHIP_START'].fillna(0, inplace=True)
    prev_fc['SHIP_START'] = pd.to_datetime(prev_fc['SHIP_START'], errors = 'coerce')
    prev_fc['SHIP_START'].fillna(0, inplace=True)
#     prev_fc['FT_SHIP_START'] = prev_fc['FT_SHIP_START'].str.slice(0, 10)
    metric = get_prev_fc_metrics(prev_fc)

    res = res.groupby(['Date', 'Sku/линейка', 'PRD_ProdType', 'PRODUCT_CODE', 'Chain', 'FT_SHIP_START'], as_index=False).agg({'Partition':'first',
                                                                                                            'Discount on date':'mean', 
                                                                                                            'Ordered':'sum', 
                                                                                                            'Forecast':'sum', 
                                                                                                            'Prev_FC':'sum', 
                                                                                                            'seasonal':'max', 
                                                                                                            'price_discount':'mean'})
                    
    res['Segment']='not_defined'
    return res, metric
    

    
def get_seasonal_df(res):
    """
    The function collects the data frame with columns ['PRODUCT_CODE', 'Chain', 'seasonal_alert'] with max values of columns 'seasonal' 
    for every pair of (PRODUCT_CODE, Chain) from input data frame (see the function get_latest_results from segmentation.py).
    
    :param res: pd.DataFrame (required columns: 'PRODUCT_CODE', 'Chain', 'Partition', 'seasonal')
    
    :return: pd.DataFrame in corresponding format
    """
    s_df = res[res['Partition']=='fc'].groupby(['PRODUCT_CODE', 'Chain'], as_index=False).agg({'seasonal':'max'})
    s_df.rename(columns={'seasonal':'seasonal_alert'}, inplace=True)
    return s_df
   
    
def get_disc_df(res):
    """
    The function collects the data frame with columns ['PRODUCT_CODE', 'Chain', 'Mac act disc', 'Min act disc'] with max and min values 
    of mean 'Discount on date' through all slots for every pair of (PRODUCT_CODE, Chain) from input data frame (see the function 
    get_latest_results from segmentation.py).
    
    :param res: pd.DataFrame (required columns: 'PRODUCT_CODE', 'Chain', 'FT_SHIP_START', 'Partition', 'Discount on date')
    
    :return: pd.DataFrame in corresponding format
    """
    disc = res[(res['Partition']=='train')&(res['Discount on date']>0)].groupby(['FT_SHIP_START', 
                                                                                 'PRODUCT_CODE', 
                                                                                 'Chain'], as_index=False).agg({'Discount on date':'mean'})
    disc = disc.groupby(['PRODUCT_CODE', 'Chain'], as_index=False).agg({'Discount on date':['max', 'min']})
    disc.columns=['PRODUCT_CODE', 'Chain', 'Max act disc', 'Min act disc']
    return disc


def get_BL_changes_df(res):
    """
    The function collects the data frame with columns ['PRODUCT_CODE', 'Chain', 'BL change'], where 'BL change' columns corresponds to
    the indicator of occurence of the baseline change in last half-year period for every pair of (PRODUCT_CODE, Chain) from input data 
    frame (see the function get_latest_results from segmentation.py).
    
    :param res: pd.DataFrame (required columns: 'Date', 'PRODUCT_CODE', 'Chain', 'Ordered', 'FT_SHIP_START', 'Partition')
    
    :return: pd.DataFrame in coresponding format
    """
    BL = res[(res['Partition']=='train') & (res['FT_SHIP_START'].astype(str)=='0')]
    max_date = max(BL['Date'])
    mid_date = max_date - dt.timedelta(days=30)
    min_date = max_date - dt.timedelta(days=180)
    BL = BL[BL['Date']>=min_date]

    BL['Partition'] = np.where(BL['Date']<mid_date, 'past', 'present')
    BL_present = BL[BL['Partition']=='present'].groupby(['Chain', 'PRODUCT_CODE'], as_index=False).agg({'Ordered':'mean'})
    BL_present.rename(columns={'Ordered':'BL current'}, inplace=True)
    BL_past = BL[BL['Partition']=='past'].groupby(['Chain', 'PRODUCT_CODE'], as_index=False).agg({'Ordered':'mean'})
    BL_past.rename(columns={'Ordered':'BL reference'}, inplace=True)
    BL = pd.merge(BL_present, BL_past, how='left', on=['Chain', 'PRODUCT_CODE'])
    BL['BL change'] = ((BL['BL current'] > 2 * BL['BL reference']) | (BL['BL current'] < 0.5 * BL['BL reference'])) + 0
    BL.drop(columns=['BL current', 'BL reference'], inplace=True)
    return BL    
    
    
def get_prev_fc_metrics(df):
    """
    The function reads the forecast for previous period and returns its metrics ('Prev_period_accuracy' and 'Prev_period_BIAS' columns).
    
    :param df: pd.Dataframe that could be used to call utils.metric_promo
    
    :return: pd.DataFrame in corresponding format
    """

    metrics = utils.metrics_promo(df, product=True, customer=True)
    metrics.drop(columns=['Forecast', 'Ordered', 'AE'], inplace=True)
    metrics.rename(columns={'Acc':'Prev_period_accuracy', 'BIAS':'Prev_period_BIAS'}, inplace=True)
    return metrics


def few_promo(res):
    """If there are less than three promos in history, then we consider that this is a heavy touch.

    Args:
        res (pd.Dataframe): Dataframe wuth results forecast

    Returns:
        pd.Dataframe: Dataframe with keys that have few promo slots
    """
    
    keys_with_few_promo = res.loc[(res['Partition'] == 'train'),
                                  ['PRODUCT_CODE', 'Chain', 'FT_SHIP_START']].copy()

    del res

    keys_with_few_promo['FT_SHIP_START'] = keys_with_few_promo['FT_SHIP_START'].astype(str)
    keys_with_few_promo = keys_with_few_promo.drop_duplicates()

    keys_with_few_promo = keys_with_few_promo.groupby(['PRODUCT_CODE', 'Chain'], 
                                                      as_index=False).agg({'FT_SHIP_START': 'count'})

    keys_with_few_promo = keys_with_few_promo[keys_with_few_promo['FT_SHIP_START'] <= 4]
    keys_with_few_promo = keys_with_few_promo.rename(columns={'FT_SHIP_START': 'bad_key_identif'})

    return keys_with_few_promo


def get_kam_fc():
    fintool = pd.read_csv('../data/processed/fintool.csv', encoding='windows-1251')
    fintool['Versions'] = pd.to_datetime(fintool['Versions'], format='%Y-%m-%d')
    latest_ft_version = fintool['Versions'].max()
    fintool = fintool[fintool['Versions'] == latest_ft_version]
    fintool = fintool.rename(columns={'customer code': 'CUSTOMER_CODE',
                                      'SKU': 'PRODUCT',
                                      'Shipment start date': 'SHIP_START',
                                      'Shipment end date': 'SHIP_END',
                                      'KAM Volume with uplift': 'KAM_FC'})

    fintool['CUSTOMER_CODE_LENTA'] = fintool['CUSTOMER_CODE'].map(utils.get_lenta_mapping())
    fintool['CUSTOMER_CODE'] = np.where(fintool['CUSTOMER_CODE_LENTA'].notnull(),
                                        fintool['CUSTOMER_CODE_LENTA'], fintool['CUSTOMER_CODE'])
    fintool.drop(columns=['CUSTOMER_CODE_LENTA'], inplace=True)

    agg_rule = {'Total Discount': 'max', 'KAM_FC': 'sum', 'wd': 'max', 'esp': 'max', 'Versions': 'first'}
    fintool = fintool.groupby(['CUSTOMER_CODE', 'PRODUCT', 'SHIP_START', 'SHIP_END'], as_index=False).agg(agg_rule)

    # собираю правильный конфиг кастомерки с учетом кастомер кодов, которые обозначают сеть целиком
    cust = pd.read_csv('../data/masterdata/Customer_MD.csv',
                       sep=';',
                       encoding='windows-1251', dtype={'Techbillto': 'str', 'customer code': 'str'},
                       usecols=['customer code', 'Chain'])
    cust = cust.rename(columns={'customer code': 'CUSTOMER_CODE'}).drop_duplicates()

    fintool = pd.merge(cust, fintool, how='right', on='CUSTOMER_CODE')

    fintool = fintool.dropna()
    # prod = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251', dtype={'SKU': 'str', 'PRODUCT_CODE': 'str'})
    # prod = prod[['Sku/линейка', 'PRODUCT_CODE']].drop_duplicates().rename(columns={'Sku/линейка': 'PRODUCT'})
    # fintool = pd.merge(fintool, prod, how='left', on='PRODUCT')
    fintool = fintool.rename(columns={'PRODUCT': 'Sku/линейка'})

    fintool = fintool.dropna()

    fintool['SHIP_START'] = pd.to_datetime(fintool['SHIP_START'], format='%Y-%m-%d')

    fintool = fintool.groupby(['Sku/линейка', 'Chain', 'SHIP_START'], as_index=False).agg({'KAM_FC': 'sum'})

    return fintool


def run_segmentation(chess, inno_keys):
    """
    The functions collects the results of the latest forecast with its segmentation based on the basis of the degree of touch ('No Touch',
    'Light Touch' or 'Heavy Touch'). The segmentation is taken from the file with specified path.
    
    :return: list of paths to the files with segmentation for every chain
    """
    # latest result and segmentation metrics by chain, product code
    res, metrics = get_latest_result_and_prev_fc_metric(inno_keys, chess)
    s_df = get_seasonal_df(res)
    disc = get_disc_df(res)
    BL = get_BL_changes_df(res)
    keys_with_few_promo = few_promo(res)
    
    #add seasonal
    res = pd.merge(res, s_df, how='left', on=['Chain', 'PRODUCT_CODE'])

    #add abnormal discount
    res = pd.merge(res, disc, how='left', on=['PRODUCT_CODE', 'Chain'])
    res['Abnormal discount'] = (((res['Discount on date'] - res['Max act disc']) > 5) | \
                                (( - res['Discount on date'] + res['Min act disc']) > 5)) & (res['Discount on date'] > 0) + 0
    res.drop(columns=['Max act disc', 'Min act disc'], inplace=True)

    #add BL changes
    res = pd.merge(res, BL, how='left', on=['PRODUCT_CODE', 'Chain'])
    res = res.fillna(0)
    
    #get acc TH
    acc_th = pd.read_csv(ACC_TH_PATH, sep=';', decimal=',')

    #calc total Alerts
    res['Alerts'] = ((res['seasonal_alert'] + res['BL change']) > 0) + 0

    #add Acc thresholds
    res = pd.merge(res, acc_th, how='left', on='PRD_ProdType')

    # add prev period metrics
    res = pd.merge(res, metrics, how='left', on=['PRODUCT_CODE', 'Chain'])

    #cals Acc, BIAS estimation
    res['BIAS'] = np.where(res['Prev_period_BIAS']<=0.2, 'Good', 'Bad')
    res['Accuracy'] = np.where(res['Prev_period_accuracy']>=res['Good accuracy TS'], 'Good', np.where(res['Prev_period_accuracy']<res['Normal accuracy TS'], 'Bad', 'Normal'))

    # TODO:add exclusions from asana: 

    #calc finally segment
    no_touch_condition = (res['Alerts']==0) & (res['Abnormal discount']==0) & (res['BIAS']=='Good') & (res['Accuracy'] == 'Good')
    light_touch_condition = (res['Abnormal discount']==0) & ((res['Accuracy'] == 'Good') | (res['Accuracy'] == 'Normal'))             
    res['Segment'] = np.where(no_touch_condition, 'No Touch', \
                             np.where(light_touch_condition, 'Light Touch', 'Heavy Touch'))

    # alert for keys with few promo
    res = res.merge(keys_with_few_promo, how='left', on=['PRODUCT_CODE', 'Chain'])
    res.loc[~res['bad_key_identif'].isna(), 'Segment'] = 'Heavy Touch'

    if chess.lower() == 'false':
        kam_fc = get_kam_fc()

        new_res = []
        for keys, df in res.groupby(['Sku/линейка', 'Chain', 'FT_SHIP_START'], as_index=False):
            df_kam = kam_fc[(kam_fc['Sku/линейка'] == keys[0]) & (kam_fc['Chain'] == keys[1]) &
                            (kam_fc['SHIP_START'] == keys[2])]
            if not df_kam.empty:
                df['Prev_FC'] = df['Forecast'] / df['Forecast'].sum() * df_kam['KAM_FC'].max()
            new_res.append(df)
        res = pd.concat(new_res)

    res = res[['Date', 'Sku/линейка', 'PRD_ProdType', 'Chain', 'Segment', 'Discount on date', 'Ordered', 'Forecast',
               'Prev_FC', 'FT_SHIP_START']]

    updated_files = []
    now_str = dt.datetime.now().strftime('%Y%m%d%H%M')
    for chain, mdf in res.groupby('Chain'):
        file = f'../results/outputs/to_excel/{chain} ML Forecast Viewer_{now_str}.xlsx'
        mdf.to_excel(file, index=False)
        updated_files.append(file)

    return updated_files, res


def freeze_start():
    """
    Calculates first date of freeze period based on current date: in quarter run it shoud be the date preceding the current one (yesterday).
    :return: string formated like '%Y%m%d', means freeze start date
    """

    today = dt.date.today()
    month = today.month
    year = today.year

    if month <= 4:
        month = 12 + month - 4
        year -= 1
    else:
        month -= 4

    return dt.date(year, month, 1).strftime(format='%Y%m%d')


def freeze_end():
    """
        Calculates last date of freeze period based on current date: in quarter run it shoud be the last date of the next month.
        :return: string formated like '%Y%m%d', means freeze end date
    """

    today = dt.date.today()
    month = today.month
    year = today.year

    if month <= 4:
        month = 12 + month - 4
        year -= 1
    else:
        month -= 4

    day = calendar.monthrange(year, month)[1]
    return dt.date(year, month, day).strftime(format='%Y%m%d')


def forecast_end():
    """
    Calculates last date of forecast period based on last date of freeze period: in quarter run the forecast period is equal to 3 months.
    :param freeze_end: string formated like '%Y%m%d', means last date of freeze period
    :return: string formated like '%Y%m%d', means forecast end date
    """

    today = dt.date.today()
    month = today.month
    year = today.year

    if month <= 1:
        month = 12 + month - 1
        year -= 1
    else:
        month -= 1

    day = calendar.monthrange(year, month)[1]
    return dt.date(year, month, day).strftime(format='%Y%m%d')

if __name__ == "__main__":
    run_segmentation('False')