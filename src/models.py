import numpy as np
import pandas as pd
import itertools
import xgboost as xgb

from copy import copy
from collections import namedtuple
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
from sklearn.neighbors import KNeighborsRegressor
from joblib import Parallel, delayed


from utils import transform_feautures_selector

def models_pool():
    """
    Collects all neсessary models data to list to be passed to init_models.  
    :return: list of named tuples containing models to be used in tournament, their parameters and constructors names.
    """
    ModelPoolItem = namedtuple('ModelPoolItem', ['model_name', 'params', 'constructor'])
    pool = []
    
    #TODO: sin cos transform for linear model Ridge, Lasso
    for alpha in np.logspace(-2, 0.3, num=10):
        params_ridge = dict()
        params_ridge['alpha'] = alpha
        params_ridge['normalize'] = True
        pool_item = ModelPoolItem(model_name=f'Ridge_{alpha}', params=params_ridge, constructor=Ridge)
        #pool_item = ModelPoolItem(model_name=f'Ridge_{alpha}', params=params_ridge, constructor=Ridge_transform)
        pool.append(pool_item)  

    for alpha in np.logspace(-2, 0.3, num=10):
        params_lasso = dict()
        params_lasso['alpha'] = alpha
        params_lasso['normalize'] = True
        pool_item = ModelPoolItem(model_name=f'Lasso_{alpha}', params=params_lasso, constructor=Lasso)
        #pool_item = ModelPoolItem(model_name=f'Lasso_{alpha}', params=params_lasso, constructor=Lasso_transform)
        pool.append(pool_item)
    
    params_base = dict()
    pool_item = ModelPoolItem(model_name='base_model', params=params_base, constructor=RAvgRegressor)
    pool.append(pool_item)
    
    for i in itertools.product([2, 3, 4], [0.02, 0.3]):
        params_tree = dict()
        params_tree['max_depth'] = i[0]
        params_tree['eta'] = i[1]
        pool_item = ModelPoolItem(model_name=f'simple_xgb_{i[0]}_{i[1]}', params=params_tree, constructor=Simple_XGBoost)    
        pool.append(pool_item)


    params_linear = dict()
    params_linear['eta'] = 0.3
    params_linear['booster'] = 'gblinear'
    pool_item = ModelPoolItem(model_name=f'simple_xgb_linear', params=params_linear, constructor=Linear_XGBoost)    
    pool.append(pool_item)    
        
#     for n in [3, 5, 10, 15, 20]:
#         params_KN = dict(n_neighbors = n)
#         pool_item = ModelPoolItem(model_name=f'kn_{n}', params=params_KN, constructor=KNeighborsRegressor)    
#         pool.append(pool_item)


    return pool

def init_models():
    """
    Initiates the model pool.
    :return: list of named tuples: model names and models itself.
    """
    models_list = []
    
    for model_name, params, constructor in models_pool():
        model = constructor(**params)
#         model.name_xyz = model_name #### !!!!!
        models_list.append((model_name, model))

    return models_list


class RAvgRegressor():
    """
    Custom model class. 
    """
    def __init__(self, window=3, random_state=None):
        self.model = Lasso(random_state=random_state)
        self.rolling_window = window
        self.masks_to_drop = []
        
    def fit(self, x, y):
        """
        Fit method. 
        :param x:
        :param y:
        :return: 
        """
        x = x.copy()
        columns = [col for col in x.columns if not any([mask in col for mask in self.masks_to_drop ])]
        x = x[sorted(columns)]
        
        y_ravg = y.rolling(window=self.rolling_window, min_periods=1, center=True).mean()
        self.model.fit(x, y_ravg)
                
    def predict(self, x):
        """
        Predict method. 
        :param x:
        :return: 
        """
        x = x.copy()
        columns = [col for col in x.columns if not any([mask in col for mask in self.masks_to_drop ])]
        x = x[sorted(columns)]
        
        return self.model.predict(x)
    

class Simple_XGBoost():
    """
    Custom model class. 
    """
    def __init__(self, **params):
        self.model = None
        self.params = params
        
    def fit(self, X, y):
        """
        Fit method. 
        :param X:
        :param y:
        :return: 
        """
        h=30
        self.model = xgb.XGBRegressor(**self.params)
        self.model.fit(X[:-h], y[:-h],
                       eval_metric='rmse', 
                       eval_set=[(X[-h:], y[-h:])], 
                       early_stopping_rounds=1000,
                       verbose=False)
        best_iter = self.model.get_booster().best_iteration
        self.model = xgb.XGBRegressor(**self.params, n_estimators=max(10, best_iter))
        self.model.fit(X, y, verbose=False)
        return self.model.fit(X, y, verbose=False)
        
        
    def predict(self, X):
        """
        Predict method. 
        :param X:
        :return: 
        """
        return self.model.predict(X)
    
    
class Linear_XGBoost():
    
    def __init__(self, **params):
        self.model = None
        self.params = params
        self.scalerX = None
        self.scalery = None
        
    def fit(self, X, y):
        """
        Fit method. 
        :param X:
        :param y:
        :return: 
        """
        #отскейлимся для начала   
        self.scalerX = MinMaxScaler()#default feature_range==(0, 1)
        self.scalery = MinMaxScaler()
        self.scalerX.fit(X)
        X = pd.DataFrame(self.scalerX.transform(X),columns = X.columns)
        y = self.scalery.fit_transform(y.values.reshape(-1, 1)).reshape(-1)
        
        #теперь подберем best_iter
        h=30
        self.model = xgb.XGBRegressor(**self.params)
        self.model.fit(X[:-h], y[:-h],
                       eval_metric='rmse', 
                       eval_set=[(X[-h:], y[-h:])], 
                       early_stopping_rounds=1000,
                       verbose=False)
        best_iter = self.model.get_booster().best_iteration
        #теперь учим саму модель
        self.model = xgb.XGBRegressor(**self.params, n_estimators=max(10, best_iter))
        self.model.fit(X, y, verbose=False)
        return self.model.fit(X, y, verbose=False)
        
        
    def predict(self, X):
        """
        Predict method. 
        :param X:
        :return: 
        """
        X = pd.DataFrame(self.scalerX.transform(X), columns = X.columns)
        fc = self.model.predict(X)
        return self.scalery.inverse_transform(fc.reshape(-1, 1)).reshape(-1)
    
#class Lasso_transform():
#    """
#    Lasso sin cos time-features transform model. 
#    """
#    def __init__(self, **params):
#        self.model = None
#        self.params = params
#        
#    def fit(self, x, y):
#        """
#        Fit method. 
#        :param x:
#        :param y:
#        :return: 
#        """
#        x = x.copy()
#        x = sin_cos_transform(x)
#
#        self.model = Lasso(**self.params)
#        self.model.fit(x, y)
#                
#    def predict(self, x):
#        """
#        Predict method. 
#        :param x:
#        :return: 
#        """
#        x = x.copy()
#        x = sin_cos_transform(x)
#        
#        return self.model.predict(x)
    
#class Ridge_transform():
#    """
#    Ridge sin cos time-features transform model. 
#    """
#    def __init__(self, **params):
#        self.model = None
#        self.params = params
#        
#    def fit(self, x, y):
#        """
#        Fit method. 
#        :param x:
#        :param y:
#        :return: 
#        """
#        x = x.copy()
#        x = sin_cos_transform(x)

#        self.model = Ridge(**self.params)
#        self.model.fit(x, y)
#                
#    def predict(self, x):
#        """
#        Predict method. 
#        :param x:
#        :return: 
#        """
#        x = x.copy()
#        x = sin_cos_transform(x)
#        
#        return self.model.predict(x)

