import pandas as pd
import numpy as np

import utils
import preprocessing


def tuning_low_fc(mdf):
    """increases the forecast taking into account the bayes in the last

    Args:
        mdf (DataFrame): sample dataframe 

    Returns:
        (DataFrame): modified sample dataframe 
    """
    if 'PRODUCT_CODE' not in mdf.columns:
        mdf['PRODUCT_CODE'] = mdf.key.str.slice(0, 4)

    mdf_promo_fact = mdf[mdf['SHIP_START'] != '0'].groupby(['PRODUCT_CODE', 
                                                           'SHIP_START'], as_index=False).agg({'Ordered': 'sum', 'Discount on date': 'max'})
    
    mdf_promo_forecast = mdf[mdf['FT_SHIP_START'] != '0'].groupby(['PRODUCT_CODE', 
                                                           'FT_SHIP_START'], as_index=False).agg({'Forecast': 'sum', 'Discount on date': 'max'})
    
    mdf_promo_slot = mdf_promo_fact.merge(mdf_promo_forecast, how='inner', left_on=['PRODUCT_CODE', 'SHIP_START'], 
                                       right_on=['PRODUCT_CODE', 'FT_SHIP_START'], suffixes=['_fact', '_forecast'])
    
    mdf_promo_slot['error_fact'] = (mdf_promo_slot['Ordered'] - mdf_promo_slot['Forecast']) / mdf_promo_slot['Ordered']

    mdf_promo_slot['error_fc'] = (mdf_promo_slot['Ordered'] - mdf_promo_slot['Forecast']) / mdf_promo_slot['Forecast']
    
    mdf_promo_slot['zscore_fact'] = (mdf_promo_slot['Ordered'] - mdf_promo_slot['Ordered'].median())/mdf_promo_slot['Ordered'].std(ddof=1)
    
    mdf_promo_slot['zscore_fc'] = (mdf_promo_slot['Forecast'] - mdf_promo_slot['Forecast'].median())/mdf_promo_slot['Forecast'].std(ddof=1)

    train_end_date = pd.to_datetime(mdf.loc[mdf['Partition'] == 'train', 'Date'].max(), format='%Y-%m-%d')

    mdf_promo_slot['SHIP_START'] = pd.to_datetime(mdf_promo_slot['SHIP_START'], format='%Y-%m-%d')

    # old ship starts for perek
    # ship_starts = sorted(list(mdf_promo_slot.loc[(mdf_promo_slot['SHIP_START'] >= '2020-10-01 00:00:00') &  
    #                                              (mdf_promo_slot['SHIP_START'] <= '2021-01-23 00:00:00') &
    #                                              (mdf['Partition'] == 'train') &
    #                                              (np.abs(mdf_promo_slot['zscore']) < 2), 'SHIP_START'].unique()))[-2:] 

    ship_starts = list(mdf_promo_slot.loc[(mdf_promo_slot['SHIP_START'] >= \
        pd.Timestamp(f"{(train_end_date - pd.DateOffset(months=3)).year}-{(train_end_date - pd.DateOffset(months=3)).month}-01")) &  
                                                #  (mdf_promo_slot['SHIP_START'] <= (train_end_date - pd.DateOffset(day=10))) & 
                                                 (np.abs(mdf_promo_slot['zscore_fact']) < 2) & 
                                                 (np.abs(mdf_promo_slot['zscore_fc']) < 2), 'SHIP_START'].unique())

    ft_ship_start = list(pd.to_datetime(mdf.loc[(mdf['Partition'] != 'train') & (mdf['FT_SHIP_START'] != '0'), 'FT_SHIP_START'], format='%Y-%m-%d').unique())

    ship_starts = sorted([i for i in ship_starts if i not in ft_ship_start])[-3:]

    if len(ship_starts) == 0:
        return mdf

    if mdf_promo_slot.loc[mdf_promo_slot['SHIP_START'].isin(ship_starts), 'error_fact'].median() > 0.05 and \
        mdf_promo_slot.loc[mdf_promo_slot['SHIP_START'].isin(ship_starts), 'error_fc'].median() < 1.0 and \
        mdf_promo_slot.loc[mdf_promo_slot['SHIP_START'].isin(ship_starts), 'error_fact'].median() < 1.0:
        mdf.loc[(mdf['Partition'] != 'train') & 
                (mdf['Discount on date'] > 1), 'Forecast'] = mdf.loc[(mdf['Partition'] != 'train') & (mdf['Discount on date'] > 1), 'Forecast'] / \
                        (1 - mdf_promo_slot.loc[mdf_promo_slot['SHIP_START'].isin(ship_starts), 'error_fact'].median())

    return mdf


def correct_promo_voluems(df: pd.DataFrame, inno_keys):
    """
    The function run preprocessing pipeline

    :param df:
    :return: pd.DataFrame
    """
    chain_by_tbt = pd.read_csv('../data/masterdata/Customer_MD_with_Lenta_mapping.csv', 
                                    encoding='windows-1251', sep=';', 
                                    dtype={'Techbillto':'str', 'Chain':'str'}, 
                                    usecols=['Techbillto', 
                                            'Chain']).drop_duplicates(subset='Techbillto').dropna().set_index('Techbillto')
    
    tmp = []

    for key, sample in df.groupby('key', as_index=False):

        if key not in inno_keys:
            chain_perek = chain_by_tbt['Chain'][key[5:]] == 'PEREKRESTOK'

            if chain_perek:
                tmp.append(tuning_low_fc(sample))
            else:
                tmp.append(sample)
        else:
            tmp.append(sample)

    return pd.concat(tmp)


def prepare_data_for_seas(data):
    data = utils.add_cat_cust(data)

    data = data[data['Chain'] == 'MAGNIT']

    data = data.drop(columns=['Chain'])

    inno_keys = preprocessing.get_inno_keys(data)

    data = data[~data['key'].isin(inno_keys)]

    return data


def latest_promo(data, key):
    train = data[data['Partition'] == 'train']
    ship_starts = train.loc[train['seasonal'] == 1, 'SHIP_START'].max()
    promo = train[(train['SHIP_START'] == ship_starts) & (train['seasonal'] == 1)]

    keys_voluem = train[train['key'].isin(list(promo['key'].unique()))]
    keys_voluem = keys_voluem.groupby('key', as_index=False).agg({'Ordered': 'sum'})

    ordered_key = train.loc[train['key'] == key, 'Ordered'].sum()
    keys_voluem['diff'] = abs(keys_voluem['Ordered'] - ordered_key)
    best_key = keys_voluem.sort_values('diff', ascending=True).reset_index(drop=True).loc[0, 'key']

    # защита от нескольких ключей
    train = train[train['key'] == best_key]
    promo = train[train['SHIP_START'] == ship_starts]

    start_seas = promo['Date'].min()
    end_seas = promo['Date'].max()

    baseline = train[(train['SHIP_START'].astype(str) == '0') &
                     (((train['Date'] >= start_seas - pd.DateOffset(60)) & (train['Date'] < start_seas)) |
                      (((train['Date'] > end_seas) & (train['Date'] <= end_seas + pd.DateOffset(60)))))]

    promo_mean_fact = promo['Ordered'].mean()
    promo_mean_fc = promo['Forecast'].mean()

    base_mean_fact = baseline['Ordered'].mean()
    base_mean_fc = baseline['Forecast'].mean()

    return promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc


def latest_base(data):
    train = data[data['Partition'] == 'train']

    latest_baseline = train[(train['SHIP_START'].astype(str) == '0') &
                            (train['Date'] >= train['Date'].max() - pd.DateOffset(90))]

    latest_base_mean_fact = latest_baseline['Ordered'].mean()
    latest_base_mean_fc = latest_baseline['Forecast'].mean()

    return latest_base_mean_fact, latest_base_mean_fc


def corr_fc(data, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact, base_mean_fact):
    fc = data[data['Partition'] == 'fc']

    fc_promo_ft_starts = list(fc.loc[fc['seasonal'] == 1, 'FT_SHIP_START'].unique())

    for fc_ft_start in fc_promo_ft_starts:
        fc_promo = fc[fc['FT_SHIP_START'] == fc_ft_start]

        start_seas_fc = fc_promo['Date'].min()
        end_seas_fc = fc_promo['Date'].max()

        fc_base = fc[(fc['FT_SHIP_START'].astype(str) == '0') &
                     (((fc['Date'] >= start_seas_fc - pd.DateOffset(60)) & (fc['Date'] < start_seas_fc)) |
                      (((fc['Date'] > end_seas_fc) & (fc['Date'] <= end_seas_fc + pd.DateOffset(60)))))]
        fc_base_mean = fc_base['Forecast'].mean()

        if base_mean_fact < 0.01:
            continue

        fc_base_corr = latest_base_mean_fact

        fc.loc[(fc['seasonal'] == 1) &
               (fc['FT_SHIP_START'] == fc_ft_start),
               'Forecast'] = fc.loc[(fc['seasonal'] == 1) & (fc['FT_SHIP_START'] == fc_ft_start), 'Forecast'] / \
                             fc.loc[(fc['seasonal'] == 1) & (fc['FT_SHIP_START'] == fc_ft_start), 'Forecast'].sum()

        fc.loc[(fc['seasonal'] == 1) & (fc['FT_SHIP_START'] == fc_ft_start),
               'Forecast'] = fc.loc[(fc['seasonal'] == 1) & (fc['FT_SHIP_START'] == fc_ft_start), 'Forecast'] * \
                             fc_base_corr * promo_mean_fact / base_mean_fact * len(fc_promo)

    return fc


def seasonal_correct_magnit(data):
    data["Date"] = pd.to_datetime(data["Date"], format='%Y-%m-%d')
    full_data = data.copy()
    data = prepare_data_for_seas(data)

    # проверяем есть ли Магнит
    if data.empty:
        return full_data

    keys_with_seasonal_train = set(data.loc[(data['Partition'] == 'train') & (data['seasonal'] == 1), 'key'].unique())
    keys_with_seasonal_fc = set(data.loc[(data['Partition'] == 'fc') & (data['seasonal'] == 1), 'key'].unique())

    # если нет сезонников на будущее
    if len(keys_with_seasonal_fc) == 0:
        return full_data

    list_data_cor = []

    for key, sample in data.groupby('key', as_index=False):
        if key not in keys_with_seasonal_fc:
            list_data_cor.append(sample)

        elif len(sample[sample['Partition'] == 'train']) / 2 >= len(sample[(sample['Partition'] == 'train') &
                                                                           (sample['Ordered'] > 0.1)]):
            list_data_cor.append(sample)

        elif key in keys_with_seasonal_fc and key in keys_with_seasonal_train:
            promo_mean_fact, promo_mean_fc, base_mean_fact, base_mean_fc = latest_promo(sample, key)
            latest_base_mean_fact, latest_base_mean_fc = latest_base(sample)

            fc = corr_fc(sample, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact, base_mean_fact)
            sample = sample[sample['Partition'] != 'fc'].append(fc)

            list_data_cor.append(sample)

        elif key in keys_with_seasonal_fc and key not in keys_with_seasonal_train:
            lin = sample['Sku/линейка'].unique()[0]
            prod_type = sample['PRD_ProdType'].unique()[0]
            brand = sample['PRD_UbrellaBrand'].unique()[0]

            lin_keys = set(data.loc[data['Sku/линейка'] == lin, 'key'].unique())

            prod_brand_type_keys = set(data.loc[(data['PRD_ProdType'] == prod_type) &
                                                (data['PRD_UbrellaBrand'] == brand), 'key'].unique())

            prod_keys = set(data.loc[data['PRD_ProdType'] == prod_type, 'key'].unique())

            if len(lin_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, \
                base_mean_fc = latest_promo(data[data['Sku/линейка'] == lin], key)
            elif len(prod_brand_type_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, \
                base_mean_fc = latest_promo(data[(data['PRD_ProdType'] == prod_type) &
                                                 (data['PRD_UbrellaBrand'] == brand)], key)
            elif len(prod_keys & keys_with_seasonal_train) > 0:
                promo_mean_fact, promo_mean_fc, base_mean_fact, \
                base_mean_fc = latest_promo(data[data['PRD_ProdType'] == prod_type], key)
            else:
                list_data_cor.append(sample)
                continue

            latest_base_mean_fact, latest_base_mean_fc = latest_base(sample)
            fc = corr_fc(sample, latest_base_mean_fact, latest_base_mean_fc, promo_mean_fact, base_mean_fact)
            sample = sample[sample['Partition'] != 'fc'].append(fc)

            list_data_cor.append(sample)

    data = pd.concat(list_data_cor)

    data = data[full_data.columns]

    full_data = full_data[~full_data['key'].isin(list(data['key'].unique()))].append(data)

    full_data['Date'] = full_data['Date'].dt.strftime("%Y-%m-%d")

    return full_data

def postproc_metro_traiders(df, metro_traiders):
    ''' Postprocessing function for split metro traiding promo slots
        :param
         df - input dataset,
         metro_traiders - koef of metro traiders
        :output
         df - result dataframe'''
    
    if 'Metro_traiders' in df.columns:
        
        df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
        # replace '0' to 'False'
        df['Metro_traiders'] = df['Metro_traiders'].apply(lambda x: str(x))
        df['cross_slots'] = df['cross_slots'].apply(lambda x: str(x))
        
        df.loc[df['Metro_traiders'] == '0', 'Metro_traiders']= 'False'
        df.loc[df['cross_slots'] == '0', 'cross_slots'] = 'False'
        
        # coef for metro traiders slots
        df.loc[(df['Metro_traiders'] == 'True') & (df['cross_slots'] == 'False'),
               'Forecast'] = df.loc[(df['Metro_traiders'] == 'True') & (df['cross_slots'] == 'False'), 'Forecast'] * metro_traiders
        df.loc[(df['Metro_traiders'] == 'True') & (df['cross_slots'] == 'False'),
               'Discount on date'] = df.loc[(df['Metro_traiders'] == 'True') & (df['cross_slots'] == 'False'), 'traiders_discount']
        # cross slots
        df_cross_slots = df[df['cross_slots']=='True']
        df = df[df['cross_slots']!='True']
        
        # for traiders
        df_traiders_cross_slots = df_cross_slots.copy()
        df_traiders_cross_slots['Forecast'] = df_traiders_cross_slots['Forecast'] * metro_traiders
        df_traiders_cross_slots['Metro_traiders'] = 'True'
        
        # for regular
        df_regular_cross_slots = df_cross_slots.copy()
        df_regular_cross_slots['Forecast'] = df_regular_cross_slots['Forecast'] *  (1 - metro_traiders)
        df_regular_cross_slots['Metro_traiders'] = 'False'
        
   
        # concat traiders and regular slots
        df_cross_slots = pd.concat([df_traiders_cross_slots.reset_index(drop=True), 
                                    df_regular_cross_slots])
        
        # concat splited traiders and regular slots
        df = pd.concat([df, df_cross_slots]).reset_index(drop=True)
        

        # fill discount for cross slots
        df.loc[df['cross_slots'] == 'True',
               'Discount on date'] = df.loc[df['cross_slots'] == 'True', 'traiders_discount']
        
        # fill FT_SHIP_START in metro traiders slots
        ss = df.groupby(['key', 'FT_SHIP_START', 'Metro_traiders'], as_index=False).agg({'Date': 'min'}) \
                                                                                   .rename(columns={'Date': 'Date_'})
        ss['FT_SHIP_START'] = pd.to_datetime(ss['FT_SHIP_START'], format='%Y-%m-%d', errors='coerce')
        df['FT_SHIP_START'] = pd.to_datetime(df['FT_SHIP_START'], format='%Y-%m-%d', errors='coerce')
        ss['Date_'] = pd.to_datetime(ss['Date_'], format='%Y-%m-%d')
        
        df = df.merge(ss, on=['key', 'FT_SHIP_START', 'Metro_traiders'], how='left')
        df.loc[(df['FT_SHIP_START']!=df['Date_']) &
               ~(df['FT_SHIP_START'].isna()), 'FT_SHIP_START'] = df[(df['FT_SHIP_START']!=df['Date_']) &
                                                                    ~(df['FT_SHIP_START'].isna())]['Date_']
        df.loc[(df['FT_SHIP_START'].isna()), 'FT_SHIP_START'] = '0'

        del df['Date_']
        del df['traiders_discount']
        
        return df
    else:
        return df
    
    
def run_postprocessing(data, inno_keys, metro_traiders):
    data = correct_promo_voluems(data, inno_keys)
    data = seasonal_correct_magnit(data)
    data = postproc_metro_traiders(data, metro_traiders)

    return data


if __name__ == '__main__':
    data = pd.read_csv('../results/outputs/UplifttoolOutput_wo_postprocessing.csv')

    data['Date'] = pd.to_datetime(data['Date'], format='%Y-%m-%d')

    data = utils.add_cat_cust(data)
    #data = data[data['Chain'] == 'MAGNIT']
    data = data.drop(columns=['Chain', 'Sku/линейка', 'PRD_ProdType', 'PRD_UbrellaBrand'])

    train_max = data.loc[data['Partition'] == 'train', 'Date'].max()
    keys_start_date = data.groupby('key', as_index=False).agg({'Date': 'min'})
    inno_keys = set(keys_start_date.loc[keys_start_date['Date'] >= train_max, 'key'].unique())

    data = run_postprocessing(data, inno_keys, metro_traiders=1)
    data = data.drop_duplicates()
    data.to_csv(r'../results/outputs/UplifttoolOutput_sincos.csv', index=False)