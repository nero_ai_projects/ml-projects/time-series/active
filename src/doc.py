import pandas as pd
import inspect
from inspect import getmembers, isfunction
import pdoc

import auxiliary
import checker
import danone
# import export
import features
import models
import notification
import preprocessing
import scanner
import tournament
import utils


def get_available_functions(module):
    """
    The function returns all the function objects in specified model.
    
    :param module: module (the type of object is module)
    
    :return: list of functions in given module (including imported)
    """
    members = inspect.getmembers(module)
    driver_names = [member for member, mtype in members if inspect.isfunction(mtype)]
    driver_from_name = {name: getattr(module, name) for name in driver_names}
    return driver_from_name


if __name__ == '__main__':
    """
    The following code creates the data frame in documents folder with columns ['function', 'module'].
    Also it creates the folder doc with htmls with documentations for all the modules in src.
    
    Remark: it requires installed module pdoc3, not pdoc!
    """
    all_modules = [
         auxiliary,
         checker,
         danone,
         # export,
         features,
         models,
         notification,
         preprocessing,
         scanner,
         tournament,
         utils,
    ]


    df = pd.DataFrame(columns=['function', 'module'])
    for module in all_modules:
        for function in inspect.getmembers(module, isfunction):
            df.loc[len(df)] = [function[0], module.__name__]
    df.to_csv(r'../documents/used_functions.csv', index=False)
    
    context = pdoc.Context()

    modules = [pdoc.Module(mod, context=context)
            for mod in all_modules]
    pdoc.link_inheritance(context)

    def recursive_htmls(mod):
        """
        The function creates the generator to iterate through the pairs (name of given module, name of html file to be created).
        
        :param mod: pdoc.Module class from pdoc; stores the information about Python module
        
        :return: generator of corresponding tuples
        """
        yield mod.name, mod.html()
        for submod in mod.submodules():
            yield from recursive_htmls(submod)

    for mod in modules:
        for module_name, html in recursive_htmls(mod):
            with open('../doc/' + module_name + '.html', 'w', encoding='utf8') as fd:
                fd.write(html)
