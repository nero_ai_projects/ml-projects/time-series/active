import pandas as pd
import numpy as np
import string
import logging
import traceback
import glob
import shutil
import os
import datetime as dt

import utils
import preprocessing


def combine_promo_slots_in_chess_for_fintool(df):
    """
    The function define sequences of promo windows that have overlays and and combine it in one window.
    The function sort values in the dataframe by Chain, Product_name, period.

    :param df: pandas.DataFrame
    :return: secs_list -  list of sequences.

    """

    df.sort_values(['Chain', 'Product name', 'period'], ascending=False, inplace=True, ignore_index=True)

    secs_list = []
    for key, sample in df.groupby(['Chain', 'Product name']):

        sample = sample.copy()
        sample['next_start'] = sample.start.shift(1)
        sample['delta'] = pd.to_datetime(sample.end) - pd.to_datetime(sample.next_start)
        # split condition for metro traiders
        sample['traiders_shift'] = sample.Metro_traiders.shift(1)
        sample['traiders_shift'] = (sample['traiders_shift'] == False) & (sample['Metro_traiders'] == False) | \
                                   (sample['traiders_shift'] == True) & (sample['Metro_traiders'] == True)

        sample['add_prev_row'] = False
        sample.loc[(sample['delta'] >= pd.Timedelta(1)) & (sample['traiders_shift']==True), 'add_prev_row'] = True
        #sample['add_prev_row'] = np.where((sample.delta >= pd.Timedelta(1)) & (sample.traiders_shift==True), True, False)
        

        sec = False
        start_index = None
        prev_row_index = None
        for row in sample.itertuples():
            if row.add_prev_row:
                if not sec:
                    start_index = row.Index - 1
                    sec = True
            else:
                if sec:
                    df.loc[start_index, 'start'] = df.loc[prev_row_index, 'start']
                    df.loc[start_index, 'store_start'] = df.loc[prev_row_index, 'store_start']
                    secs_list.append((start_index, prev_row_index))
                    sec = False

            prev_row_index = row.Index

        if start_index is not None and sec:
            df.loc[start_index, 'start'] = row.start
            df.loc[start_index, 'store_start'] = row.store_start
            secs_list.append((start_index, row.Index))

    return secs_list


def found_comment(df, limit_rows=6, limit_cols=3):
    for idx, vals in enumerate(df.itertuples()):
        for idx_col, label in enumerate(vals):
            if idx_col > limit_cols: break
            if type(label) is str:
                if label.lower().strip() == 'механика':
                    return idx + 1
        if idx >= limit_rows:
            return -1


def extract_data_from_chess_sheet(file, xls, sheet, ):
    df = xls.parse(sheet, encoding='windows-1251')

    header = utils.get_chess_header(df)

    table = xls.parse(sheet, header=header, encoding='windows-1251')
    table.dropna(subset=['Product name'], inplace=True)

    ship_starts_from_idx = table.columns.get_loc("Product name") + 1
    ship_starts = table.columns[ship_starts_from_idx:].values

    ship_ends = df.iloc[header - 2].values
    ship_ends = ship_ends[ship_starts_from_idx:]

    # store ship start and ship end for chess output
    store_starts = df.iloc[header - 3].values
    store_starts = store_starts[ship_starts_from_idx:]

    store_ends = df.iloc[header - 4].values
    store_ends = store_ends[ship_starts_from_idx:]

    comments = df.iloc[header - 5].values
    comments = comments[ship_starts_from_idx:]
    comments = comments.astype('str')
    # проверяем наличие столбца комментарий
    comment_header = found_comment(df)
    if comment_header == -1:
        comments[:] = None
    else:
        comments = np.where(comments == 'nan', None, comments)

    periods = ['{}|{}|{}|{}|{}'.format(*items) for items in zip(ship_starts, ship_ends, store_starts, store_ends, comments)]

    table.columns = table.columns[:ship_starts_from_idx].values.tolist() + periods

    table.rename(columns={'Сеть': 'Chain'}, inplace=True)
    cols_to_del = set([col for col in table.columns if col.startswith('Unnamed')] + \
                      [col for col in table.columns if col not in {'Chain', 'Product name'} and '|' not in col] + \
                      [col for col in table.columns if col == ' ' or 'nan' in col])

    for col in cols_to_del:
        del table[col]

    df = table.melt(id_vars=['Chain', 'Product name'], value_name='data', var_name='period')

    df.dropna(inplace=True)

    if df.empty:
        return df
    
    # flag for metron traiders slots
    df['Metro_traiders'] = False

    def metro_fix(data_str):
        if 'traders' in str(data_str):
            return True
        else:
            return False
        
    df.loc[df['data'].apply(lambda x: metro_fix(x)), 'Metro_traiders'] = True

    preprocessing.clean_chess_data_col_4_parsing(df)

    df[['start', 'end', 'store_start', 'store_end', 'Mechanics']] = df.period.str.split('|', expand=True)

    secs_list = combine_promo_slots_in_chess_for_fintool(df)

    utils.add_ship_start_in_chess(df, secs_list)

    for col in ['start', 'end', 'store_start', 'store_end']:
        # на шахматке карусели в Q4 2021 возник артефакт, дата прочиталась как 2021-09-27 00:00:00.100 из-за
        # чего не мержились слоты
        df[col] = df[col].str[:10]
        df[col] = pd.to_datetime(df[col], format='%Y-%m-%d')

    df['data_str'] = df.data.apply(lambda x: x if isinstance(x, str) else '')
    df['data_str'] = df['data_str'].str.strip()
    if df.data_str.max() == '':
        df.rename(columns={'data': 'discount'}, inplace=True)
        df['wd'] = 0
        df['esp'] = 0
        del df['data_str']
    else:
        df[['discount', 'wd', 'esp']] = df.data_str.str.split(',', expand=True)
        df['discount'] = df['discount'].str.strip(' %').replace('', '0').astype('float')
        df['discount'] = np.where(df['data_str'] == '', df.data, df.discount)

        df['wd'] = np.where(df.wd.isna() | (df.wd == ''), 'wd=0', df.wd)
        df['esp'] = np.where(df.esp.isna() | (df.esp == ''), 'esp=0', df.wd)

        df.wd = df.wd.str.split('=', expand=True)[1].str.strip(' %').astype('float') / 100
        df.esp = df.esp.str.split('=', expand=True)[1].str.strip(' %').astype('float')

        for col in ['data', 'data_str']:
            del df[col]

    for col in ['discount', 'wd', 'esp']:
        df[col] = np.where(df[col].isin(list(string.whitespace)), '0', df[col])
        df[col] = df[col].astype(float)

    # TODO: fix this hack more accurate. Find and fix the reason.
    for col in ['discount', 'wd']:
        df[col] = np.where(df[col] < 1, df[col] * 100, df[col])

    df['Mechanics'] = np.where(df['Mechanics'].isna(), None, df['Mechanics'])
    for str_for_rplc in ['None', 'Non', 'No']:
        df['Mechanics'] = np.where(df['Mechanics'] == str_for_rplc, None, df['Mechanics'])

    srez = df[(df['SHIP_START'] == '2021-07-06') & (df['Product name'] == 'TM MEAT & MEAT-VEG PUREE 100')]
    srez_agg = srez.groupby(['Chain', 'Product name', 'start'],
                        as_index=False).agg({'discount': 'mean', 'end': 'max',
                                             'store_start': 'min', 'store_end': 'max', 'Mechanics': lambda x: x[x.notna()].max()})

    df_agg = df.groupby(['Chain', 'Product name', 'start'],
                        as_index=False).agg({'discount': 'mean', 'end': 'max',
                                             'store_start': 'min', 'store_end': 'max', 'Mechanics': lambda x: x[x.notna()].max()})

    return df_agg


def get_chess_for_fintool_output():
    """
    The function create tall dataframe from chess files.
    The function scanning '../data/raw/' folder to get files list by mask 'Chess*.xlsm'.
    In every file will be processed sheets that names start with 'I - '.

    To define where table starts function use utils.get_chess_header(df) function.
    If get_chess_header couldn't get header position for sheet sheet will be skipped.

    Assumptions:
        - Ship start dates placed on the same row as 'Product name' label
        - Ship ends dates placed on previous row
        - The 'Product name' columns is the last column in the table: only dates of promo placed later

    :return: pd.DataFrame(columns=['Chain', 'product_name', 'date', 'discount', 'wd', 'esp'])
    """
    # TODO: объединенные ячейки лежат только на первой
    files = glob.glob(r'../data/raw/Chess*.xlsm')

    tmp = []
    for file in files:

        xls = pd.ExcelFile(file)
        sheets = [sheet for sheet in xls.sheet_names if sheet.startswith('I - ')]
        if 'Billa' in file:
            print(file)

        for sheet in sheets:
            try:
                df = extract_data_from_chess_sheet(file, xls, sheet, )
                if df.empty:
                    continue

                df.rename(columns={'Product name': 'Sku/линейка'}, inplace=True)


                if not df.empty:
                    if df['Chain'].values[0].upper() == 'PEREKRESTOK':
                        temp_df = df.copy()
                        temp_df['Chain'] = 'PEREKRESTOK ONLINE'
                        df = df.append(temp_df)

                tmp.append(df)

            except (ValueError, AssertionError):
                msg = f"get_chess func:  can't parse file {file} - sheet {sheet}. Sheep skipped."
                logging.error(msg)
                logging.error(traceback.format_exc())
                continue

    return pd.concat(tmp, ignore_index=True)


def cust_func():
    cust = pd.read_csv('../data/masterdata/Customer_MD.csv', sep=';', encoding='windows-1251',
                       dtype={'Techbillto': 'str', 'customer code': 'str'},
                       usecols=['Techbillto', 'customer code', 'Chain'])
    
    # Lenta change chain for chess
    dict_lenta = utils.get_lenta_chain_mapping()
    cust.loc[cust['Techbillto'].isin(dict_lenta.keys()), 'Chain'] = cust[cust['Techbillto'].isin(dict_lenta.keys())]['Techbillto'].map(dict_lenta)

    cust_with_code_on_chain = list(cust.loc[cust['Techbillto'].isna(), 'Chain'])
    print(cust_with_code_on_chain)
    cust_prep = cust[~cust['Chain'].isin(cust_with_code_on_chain)]

    cust_prep = cust_prep.append(cust.loc[(cust['Techbillto'].isna()) &
                                          (cust['Chain'].isin(cust_with_code_on_chain)),
                                          ['customer code',
                                           'Chain']].merge(cust.loc[~(cust['Techbillto'].isna()) &
                                                                    (cust['Chain'].isin(cust_with_code_on_chain)),
                                                                    ['Techbillto', 'Chain']], how='outer',
                                                           on=['Chain']))
    return cust_prep


def uploading_for_data(fact_output_name, fc_output_name, chess_fc_output_name, segment_data, chess, OUTPUT_MQ_FOLDER):
    segment_data = segment_data[segment_data['FT_SHIP_START'].astype(str) != '0'].reset_index(drop=True)
    segment_data = segment_data[['Sku/линейка', 'Chain', 'Segment',
                                 'FT_SHIP_START']].drop_duplicates()
    segment_data['FT_SHIP_START'] = pd.to_datetime(segment_data['FT_SHIP_START'],
                                                   format='%Y-%m-%d')

    columns = ['Date', 'key', 'Ordered', 'Partition', 'SHIP_START', 'FT_SHIP_START', 'Forecast', 'Discount on date',
               'price_discount', 'seasonal']
    res = pd.read_csv('../results/outputs/UplifttoolOutput.csv', usecols=columns)
    res['Date'] = pd.to_datetime(res['Date'], format='%Y-%m-%d')
    res['FT_SHIP_START'] = np.where(res['Partition'] == 'train', res['SHIP_START'], res['FT_SHIP_START'])

    train_end = res.loc[res['Partition'] == 'train', 'Date'].max()
    freeze_end = res.loc[res['Partition'] == 'freeze', 'Date'].max()

    res = res[res['FT_SHIP_START'].astype(str) != '0'].reset_index(drop=True)
    res['FT_SHIP_START'] = pd.to_datetime(res['FT_SHIP_START'], format='%Y-%m-%d')

    fintool = pd.read_csv('../data/processed/fintool.csv', dtype={'# of promo': 'str'})

    fintool = fintool.rename(columns={'customer code': 'CUSTOMER_CODE',
                                      'SKU': 'Sku/линейка',
                                      'Shipment start date': 'FT_SHIP_START',
                                      'Shipment end date': 'FT_SHIP_END',
                                      'KAM Volume with uplift': 'KAM_FC', })

    # Берём самые актуальные версии для промо
    fintool['Versions'] = pd.to_datetime(fintool['Versions'], format='%Y-%m-%d')
    version_for_promo = fintool.groupby(['CUSTOMER_CODE', '# of promo'], as_index=False).agg({'Versions': 'max'})
    fintool = fintool.merge(version_for_promo, how='inner', on=['CUSTOMER_CODE', '# of promo', 'Versions'])

    # мэппинг ленты (догадка о том, что он есть)
    #     fintool['CUSTOMER_CODE'] = fintool['CUSTOMER_CODE'].map(utils.get_lenta_mapping())

    fintool['CUSTOMER_CODE_LENTA'] = fintool['CUSTOMER_CODE'].map(utils.get_lenta_mapping())
    fintool['CUSTOMER_CODE'] = np.where(fintool['CUSTOMER_CODE_LENTA'].notnull(),
                                        fintool['CUSTOMER_CODE_LENTA'], fintool['CUSTOMER_CODE'])
    fintool.drop(columns=['CUSTOMER_CODE_LENTA'], inplace=True)

    agg_rule = {'Total Discount': 'max', 'KAM_FC': 'sum', 'wd': 'max', 'esp': 'max'}
    fintool = fintool.groupby(['CUSTOMER_CODE', 'Sku/линейка', 'FT_SHIP_START', 'FT_SHIP_END',
                               '# of promo'], as_index=False).agg(agg_rule)
    # собираю правильный конфиг кастомерки с учетом кастомер кодов, которые обозначают сеть целиком
    file = pd.read_csv('../data/masterdata/Customer_MD.csv',
                       sep=';',
                       encoding='windows-1251', dtype={'Techbillto': 'str', 'customer code': 'str'},
                       usecols=['Techbillto', 'customer code', 'Chain', 'customer name'])
    file = file.rename(columns={'Techbillto': 'TECH_BILL_TO', 'customer code': 'CUSTOMER_CODE'})
    tbt = file.dropna()
    totals = file[file['TECH_BILL_TO'].isna()]
    totals.drop(columns=['TECH_BILL_TO'], inplace=True)
    totals = pd.merge(tbt[['TECH_BILL_TO', 'Chain']], totals, how='left', on='Chain').dropna()
    cust_hier = pd.concat([tbt, totals])

    # распячиваю на все техбиллту и добавляю продуктовую
    fintool = pd.merge(cust_hier, fintool, how='left', on='CUSTOMER_CODE')
    fintool = fintool.dropna()
    tmp = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251',
                      dtype={'SKU': 'str', 'PRODUCT_CODE': 'str'})
    tmp = tmp[['Sku/линейка', 'PRODUCT_CODE']].drop_duplicates()
    fintool = pd.merge(fintool, tmp, how='left', on='Sku/линейка')
    fintool = fintool.dropna()
    # добавляю ключ и привожу к привычному формату дат
    fintool['key'] = fintool['PRODUCT_CODE'] + '_' + fintool['TECH_BILL_TO']

    fintool['FT_SHIP_START'] = pd.to_datetime(fintool['FT_SHIP_START'], format='%Y-%m-%d')
    fintool['FT_SHIP_END'] = pd.to_datetime(fintool['FT_SHIP_END'], format='%Y-%m-%d')

    res = res[['Date', 'key', 'Ordered', 'FT_SHIP_START', 'Forecast']]
    fintool = fintool[['key', 'CUSTOMER_CODE', 'FT_SHIP_START', 'FT_SHIP_END', '# of promo', 'Sku/линейка',
                       'Chain']].drop_duplicates()

    res = res.merge(fintool, how='left', on=['key', 'FT_SHIP_START'])

    # segment_data = segment_data.merge(cust_hier, how='inner', on=['Chain'])

    res = res.merge(segment_data, how='left', on=['Sku/линейка', 'Chain', 'FT_SHIP_START'])

    res = res.drop_duplicates()

    last_fact_promo = res.loc[(res['Date'] >= (train_end - pd.Timedelta("14 days"))) &
                              (res['FT_SHIP_END'] <= train_end), ['key', 'FT_SHIP_START']].drop_duplicates()

    data_fact = res.merge(last_fact_promo, how='inner', on=['key', 'FT_SHIP_START'])

    data_fact = data_fact.groupby(['# of promo', 'CUSTOMER_CODE'], as_index=False).agg({'Ordered': 'sum'})
    data_fact = data_fact.rename(columns={'CUSTOMER_CODE': 'AFT customer code', 'Ordered': 'KAM volume with uplift'})
    data_fact['Comments'] = 'факт'
    data_fact['# of promo'] = data_fact['# of promo'].apply(lambda x: int(float(x)))
    data_fact['KAM volume with uplift'] = data_fact['KAM volume with uplift'].apply(lambda x: round(x, 1))

    data_fact.to_csv(os.path.abspath(r'../results/outputs/Results_export/' + fact_output_name), index=False)
    shutil.copy(os.path.abspath(r'../results/outputs/Results_export/' + fact_output_name), 
                os.path.abspath(os.path.join(OUTPUT_MQ_FOLDER, r'Results_export', fact_output_name)))

    if chess.lower() == 'false':
        data_fc = res.loc[(res['FT_SHIP_START'] > freeze_end) &
                          (res['Segment'] == 'No Touch'), ['# of promo', 'CUSTOMER_CODE', 'Forecast']]
        data_fc = data_fc.groupby(['# of promo', 'CUSTOMER_CODE'], as_index=False).agg({'Forecast': 'sum'})
        data_fc = data_fc.rename(columns={'CUSTOMER_CODE': 'AFT customer code', 'Forecast': 'KAM volume with uplift'})
        data_fc['# of promo'] = data_fc['# of promo'].apply(lambda x: int(float(x)))
        data_fc['KAM volume with uplift'] = data_fc['KAM volume with uplift'].apply(lambda x: round(x, 1))

        data_fc.to_csv(os.path.abspath(r'../results/outputs/Results_export/' + fc_output_name), index=False)
        shutil.copy(os.path.abspath(r'../results/outputs/Results_export/' + fc_output_name), 
                    os.path.abspath(os.path.join(OUTPUT_MQ_FOLDER, r'Results_export', fc_output_name)))
    elif chess.lower() == 'true':
        data_fc = res[['Sku/линейка', 'key', 'FT_SHIP_START', 'Forecast']]

        data_fc = data_fc.drop(columns=['Sku/линейка'])
        data_fc['PRODUCT_CODE'] = data_fc['key'].str[:4]

        tmp = tmp[['Sku/линейка', 'PRODUCT_CODE']].drop_duplicates()
        data_fc = pd.merge(data_fc, tmp, how='left', on='PRODUCT_CODE')

        cust = cust_func()

        data_fc['Techbillto'] = data_fc['key'].str[5:]
        data_fc = data_fc.merge(cust, how='left', on='Techbillto')

        chess = get_chess_for_fintool_output()
        
        # fill inno in forecast
        #data_fc = fill_inno_data_fc(data_fc, chess, tmp)

        data_fc = data_fc.merge(chess, how='left', left_on=['Sku/линейка', 'Chain', 'FT_SHIP_START'],
                                right_on=['Sku/линейка', 'Chain', 'start'])

        data_fc = data_fc[data_fc['end'] > freeze_end]

        index = data_fc['end'].dropna().index
        data_fc = data_fc.loc[index]
        data_fc['Days before store starts'] = (data_fc['store_start'] - data_fc['start']).dt.days
        data_fc['Days shipment'] = (data_fc['end'] - data_fc['start']).dt.days

        data_fc['Mechanics'] = np.where(data_fc['Mechanics'].isna(), None, data_fc['Mechanics'])
        for str_for_rplc in ['None', 'Non', 'No']:
            data_fc['Mechanics'] = np.where(data_fc['Mechanics'] == str_for_rplc, None, data_fc['Mechanics'])
        data_fc = data_fc.groupby(['Sku/линейка', 'customer code', 'store_start', 'store_end'],
                                  as_index=False).agg({'Days before store starts': 'mean',
                                                       'Days shipment': 'mean', 'Forecast': 'sum',
                                                       'discount': 'mean', 'Mechanics': lambda x: x[x.notna()].max()})
        data_fc = data_fc.rename(columns={'Sku/линейка': 'SKU', 'store_start': 'Store start',
                                          'store_end': 'Store End', 'discount': 'On-invoice',
                                          'Forecast': 'KAM volume with uplift'})
        data_fc['Store format'] = 'All'

        data_fc = data_fc[['SKU', 'customer code', 'Store start', 'Store End', 'Days before store starts',
                           'Days shipment', 'KAM volume with uplift', 'On-invoice', 'Store format', 'Mechanics']]

        data_fc['Days before store starts'] = data_fc['Days before store starts'].apply(lambda x: int(float(x)))
        data_fc['Days shipment'] = data_fc['Days shipment'].apply(lambda x: int(float(x)))
        data_fc['On-invoice'] = data_fc['On-invoice'].apply(lambda x: round(x, 1))
        data_fc['KAM volume with uplift'] = data_fc['KAM volume with uplift'].apply(lambda x: round(x, 1))

        # чтобы учитывался первый день промо для логики финтула
        data_fc['Days shipment'] = data_fc['Days shipment'] + 1

        data_fc['Mechanics'] = np.where(data_fc['Mechanics'].isna(), '', data_fc['Mechanics'])
        for str_for_rplc in ['None', 'Non', 'No']:
            data_fc['Mechanics'] = np.where(data_fc['Mechanics'] == str_for_rplc, '', data_fc['Mechanics'])
        
        data_fc.to_csv(os.path.abspath(r'../results/outputs/Results_export/' + chess_fc_output_name), index=False)
        shutil.copy(os.path.abspath(r'../results/outputs/Results_export/' + chess_fc_output_name), 
                    os.path.abspath(os.path.join(OUTPUT_MQ_FOLDER, r'Results_export', chess_fc_output_name)))


def fill_inno_data_fc(data_fc, chess, prod):
    
    # all chain promo slots from chess
    inno = chess[['Sku/линейка', 'Chain', 'start']].drop_duplicates()
    
    # make inno raws simiraly forecast dataframe
    inno = inno.rename(columns={'start': 'FT_SHIP_START'})
    
    # fill PRODUCT_CODE in inno
    inno = inno.merge(prod, on='Sku/линейка', how='left').dropna()

    
    # make Chain promo slots for each Chain, customer code, Techbillto
    chain_tbt = data_fc[['Chain', 'key', 'customer code']].drop_duplicates()
    chain_tbt['Techbillto'] = chain_tbt['key'].str[5:]
    chain_tbt = chain_tbt.drop('key', axis=1)

    # make all promo from chess for all Techbillto
    inno = chain_tbt.merge(inno, on='Chain', how='left').dropna().drop_duplicates()
    inno['key'] = inno['PRODUCT_CODE'] + '_' + inno['Techbillto']

    # make slots with zero forecast for inno
    data_fc = data_fc.merge(inno, how='outer', on=['key', 'FT_SHIP_START', 'PRODUCT_CODE', 'Sku/линейка', 'Techbillto', 'customer code', 'Chain']).fillna(0)
    
    return data_fc


if __name__ == '__main__':
    import segmentation
    
    from scanner import OUTPUT_MQ_FOLDER
    
    notification = False
    uptool_date = dt.datetime.now().strftime('%d.%m.%Y_%H:%M:%S')
    now_str = dt.datetime.now().strftime('%Y%m%d%H%M')
    #OUTPUT_MQ_FOLDER = r'\10.108.1.220\Script_Folders\Uplift_Tool\Send'
    
    name_fact = f'update_fact_sale_q_{now_str}.csv'
    name_fc = f'bi_weekly_{now_str}.csv'
    name_fc_chess = f'init_quarter_{now_str}.csv'
    #name_fact = os.path.join(OUTPUT_MQ_FOLDER, 'Results_export', name_fact)
    #name_fc = os.path.join(OUTPUT_MQ_FOLDER, 'Results_export', name_fc)
    #name_fc_chess = os.path.join(OUTPUT_MQ_FOLDER, 'Results_export', name_fc_chess)

    chess = 'true'

    df = pd.read_csv('../results/outputs/UplifttoolOutput.csv')
    df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')

    #test = df[df['key'] == '0984_850011270']

    inno_keys = preprocessing.get_inno_keys(df)
    segmentation_files, res = segmentation.run_segmentation(chess,
                                                            inno_keys)


    uploading_for_data(name_fact, name_fc, name_fc_chess, res, chess, OUTPUT_MQ_FOLDER)
