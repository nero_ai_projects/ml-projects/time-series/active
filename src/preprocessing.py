import string
import traceback
import pandas as pd
import numpy as np
import datetime as dt
import glob
from collections import defaultdict
import calendar
import gc
import logging
from datetime import datetime

import danone
import utils
import features
import auxiliary


def get_new_SI(new_file_name):
    """
    Preprocesses the sell-in be-weekly file (adds mappings and promo dates), returns and writes it.
    :param new_file_name: path to the sell-in file
    :return: pandas dataframe, new sell-in in correcponding format
    """
    customer_mapping = get_customer_mapping()
    product_mapping = get_product_mapping()
    promo = get_promo_md()
    lenta_mapping = utils.get_lenta_mapping()

    columns = ['PGI_DATE', 'TECH_BILL_TO', 'ORDER_QUAN',
               'GI_QUAN', 'SKU', 'DISCOUNT', 'PGI_DATA', 'PROMO_ID']
    dtype = {
        'PGI_DATE': 'str',
        'TECH_BILL_TO': 'str',
        'SKU': 'str',
        'PROMO_ID': 'str',
        'GI_QUAN': 'float',
        'ORDER_QUAN': 'float'
    }
    agg_rules = {
        'DISCOUNT': 'max',
        'ORDER_QUAN': 'sum',
        'GI_QUAN': 'sum',
        'SHIP_END': 'max',
        'SHIP_START': 'max',
        'PROMO_ID': 'first',
                 }

    df = pd.read_csv(new_file_name, sep=';', dtype=dtype, usecols=columns)
    df = df[(df['PGI_DATA'] > 0) & (df.ORDER_QUAN >= 0)]
    del df['PGI_DATA']
    df['SKU'] = df['SKU'].astype(str).str.zfill(6)
    if df.dtypes.DISCOUNT != 'float':
        df['DISCOUNT'] = df['DISCOUNT'].apply(lambda x: float(str(x).replace(',', '.')))
        
    product_mapping['SKU'] = product_mapping['SKU'].astype(str).str.zfill(6)

    df = pd.merge(df, product_mapping, how='left', on='SKU')
    df = pd.merge(df, promo, how='left', on='PROMO_ID')

    df['SHIP_START'] = df['SHIP_START'].fillna('')
    df['SHIP_END'] = df['SHIP_END'].fillna('')
    df['KEY'] = df['PRODUCT_CODE'] + '_' + df['TECH_BILL_TO']

    df = pd.merge(df, customer_mapping, how='left', on='TECH_BILL_TO')

    # хитрость с мэппингом ленты
    df['PGI_DATE'] = pd.to_datetime(df['PGI_DATE'], format='%Y%m%d')
    df['TECH_BILL_TO_LENTA'] = df['TECH_BILL_TO'].map(lenta_mapping)
    df['TECH_BILL_TO'] = np.where(df['TECH_BILL_TO_LENTA'].notnull(),
                                     df['TECH_BILL_TO_LENTA'], df['TECH_BILL_TO'])

    df = df.groupby(['PGI_DATE', 'KEY'], as_index=False).agg(agg_rules)
    df.fillna(0, inplace=True)

    df, _ = utils.reduce_mem_usage(df)
    del promo
    del product_mapping
    del customer_mapping
    del lenta_mapping

    _ = gc.collect()

    df.to_csv('../data/processed/SI_simple/latest_SI.csv', sep=';', index=False)

    return df


@utils.gc_collect
def get_simple_SI(new_file_name=None, history_start=None, filters=None):
    """
    Preprocesses all available sell-in data: adds be-weekly update and writes it down to quarterly files.
    :param new_file_name: path to the sell-in be-weekly file
    :param history_start: string formated like '%Y%m%d', means date from which the time series will start
    :param filters: 
    :return: pandas dataframe, all available/requested sell-in in corresponding format
    """
    # TODO: history start might be None, but we convert it in datetime
    history_start = pd.to_datetime(history_start, format='%Y%m%d')

    dtype = {
        'PGI_DATE': 'str',
        'TECH_BILL_TO': 'str',
        'SKU': 'str',
        'PROMO_ID': 'str',
        'GI_QUAN': 'float',
        'ORDER_QUAN': 'float'
    }

    if new_file_name is None:
        new_file_name = max(glob.glob(r'../data/raw/Uplifttool_sellin_[0-9][0-9][0-9][0-9][0-9][0-9].csv'))

    update = get_new_SI(new_file_name=new_file_name)

    tmp = []
    for name in glob.glob('../data/processed/SI_simple/*.csv'):
        if 'latest_SI' in name:
            continue

        df = pd.read_csv(name, sep=';', dtype=dtype)
        tmp.append(df)

    old_data = pd.concat(tmp)

    update['PGI_DATE'] = pd.to_datetime(update['PGI_DATE'], format='%Y-%m-%d')
    old_data['PGI_DATE'] = pd.to_datetime(old_data['PGI_DATE'], format='%Y-%m-%d')

    min_date = update['PGI_DATE'].min()
    old_data = old_data[old_data['PGI_DATE']<min_date]

    df = pd.concat([old_data, update])

    write_si_by_yq(df, history_start)

    if filters is not None:
        df = filter_data(df, filters)

    return df


@utils.gc_collect
def write_si_by_yq(df, history_start=None):
    """
    The function split data frame by quarters and writes every part in separated file.
    The destination folder is "../data/processed/SI_simple/"

    :param df: pd.DataFrame
    :param history_start: date. Used to define what data should be stored. If None used min date from "PGI_DATE" column.
    :return: None
    """
    if history_start is None:
        year_start = df['PGI_DATE'].min().year
    else:
        year_start = history_start.year

    year_end = df['PGI_DATE'].max().year

    for y in range(year_start, year_end + 1):
        for q in range(1, 5):
            start_date, end_date = define_quarter_dates(q, y)
            si_q = df[(df['PGI_DATE'] >= start_date) & (df['PGI_DATE'] <= end_date)]
            si_q.to_csv(f'../data/processed/SI_simple/{y}Q{q}.csv', index=False, sep=';')


def restore_ship_start_end_in_si_df(df):
    """
    The function restore ship start and end for SI dataframe.
    The dataframe is pd.Dataframe that contains combined SI data (latest and previously used).
    The function define promo where SHIP_END and SHIP_START columns is empty and update these cases.

    Note: we cant restore it correctly for new data only because we don't know when promo was started

    :param df: pd.DataFrame
    :return: None
    """

    df['SHIP_END'] = np.where(df.SHIP_END == '', np.NAN, df.SHIP_END)
    df['SHIP_START'] = np.where(df.SHIP_START == '', np.NAN, df.SHIP_START)

    gr = df.groupby(['KEY', 'PROMO_ID'])
    df['end'] = gr['SHIP_END'].transform(max)
    df['start'] = gr['SHIP_START'].transform(min)

    df['SHIP_END'] = np.where(df.SHIP_END.isna() & ~df.PROMO_ID.isin(['0', 0]), df.end, df['SHIP_END'])
    df['SHIP_START'] = np.where(df.SHIP_START.isna() & ~df.PROMO_ID.isin(['0', 0]), df.start, df['SHIP_START'])

    del df['end']
    del df['start']
    del gr


@utils.gc_collect
def filter_data(df, filters):
    """
    The function filters data frame by products and/or by customers from filters dictionary.
    
    :param df: pd.DataFrame to be filtered
    :param filters: dict; the structure if as follows: 
                              - keys can take values from ['Products', 'Chains'];
                              - value of key 'Products' is a dictinary itself;
                              - if 'Products' is a key, then filters['Products'] keys can take values from ['Categories', 'Product_names'];
                              - if 'Products' is a key, then the values of its keys are lists of corresponding entities to use;
                              - if 'Chains' is a key, then its value is a list of chains to use
                          example of filters dict:
                              - {'Chains': ['PYATEROCHKA']}
                              - {'Products': {'Product_names': ['TT MEAT & MEAT-VEG PUREE 100', 'AM FUNCTIONAL DRINK 1*100 (6 PACK)']}}
                              - {'Products': {'Categories': ['PASTER MILK WHITE']}}
                              - {'Chains': ['PYATEROCHKA'], 'Products': {'Categories': ['PASTER MILK WHITE']}}
                              - {'Chains': ['PYATEROCHKA'], 'Products': {'Product_names': ['TM CURD DESSERTS 100', 'TM JUICES 200']}}
                          there's no need to use both possible keys for 'Products', since Category > Product_name hierarhically
   
    :return: pd.DataFrame (filtered)
    """
    columns = list(df.columns)
    df = utils.add_cat_cust(df, key_col='KEY')

    if filters.get('Products', False):
        product_filters = filters['Products']
        if product_filters.get('Categories', False):
            df = df[df['PRD_ProdType'].isin(product_filters['Categories'])]
            _ = gc.collect()
        if product_filters.get('Product_names', False):
            df = df[df['Sku/линейка'].isin(product_filters['Product_names'])]
            _ = gc.collect()
    if filters.get('Chains', False):
        df = df[df.Chain.isin(filters['Chains'])]
        _ = gc.collect()

    df = df[columns]
    _ = gc.collect()
    return df


def define_quarter_dates(q, y):
    """
    Defines quarter start date and quarter end date based on quarter number and year
    :param q: int, quarter number 
    :param y: int, year
    :return: timestamp, quarter start date
    :return: timestamp, quarter end date
    """
    y = y
    end_m = q * 3
    start_m = q * 3 - 2
    end_d = calendar.monthrange(y, end_m)[1]
    start_d = 1
    return dt.datetime(y, start_m, start_d, 0, 0), dt.datetime(y, end_m, end_d, 0, 0)


def get_promo_md(promo_cols=None):
    """
    Reads and preprocesses dataframe with promo master data (sttk_md_promo).
    Every column reads as a string.

    :param: promo_cols: list of columns that will be used to read file. Default value is None.
    If None will be used ['PROMO_ID', 'SHIP_START', 'SHIP_END']

    :return: pandas dataframe: mapping between promo ids and promo start/end shipment dates.
    """

    promo_cols = ['PROMO_ID', 'SHIP_START', 'SHIP_END'] if promo_cols is None else promo_cols
    dtype = {col: 'str' for col in promo_cols}

    files = glob.glob('../data/raw/Uplifttool_MDM_Promo_*.csv')
    latest_file = max(files)
    promo = pd.read_csv(latest_file, sep=';', encoding='windows-1251', dtype=dtype, usecols=promo_cols)

    return promo


def get_product_mapping():
    """
    Gets product hierarchy reading it from corresponding file
    :return: pandas dataframe with product hierarchy based on SAP-code (SKU), columns=['SKU', 'PRODUCT_CODE', 'PRD_ProdType', 'Sku/линейка']. 
    """

    cols = ['SKU', 'PRODUCT_CODE', 'PRD_ProdType', 'Sku/линейка']
    tmp = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';',
                      encoding='windows-1251',
                      dtype={'SKU': 'str', 'PRODUCT_CODE': 'str'},
                      usecols=cols)

    tmp = tmp.drop_duplicates()
    tmp = tmp[tmp['PRD_ProdType'].str.find('DUMMY')==-1]
    tmp.rename(columns={'PRD_ProdType': 'CATEGORY'}, inplace=True)

    return tmp


def get_customer_mapping():
    """
    Gets customer hierarchy reading it from corresponding file
    :return: pandas dataframe with product hierarchy based on techbillto code, columns=['Techbillto', 'customer code', 'Chain', 'customer name']. 
    """
    tmp = pd.read_csv('../data/masterdata/Customer_MD.csv',
                      sep=';',
                      encoding='windows-1251', dtype={'Techbillto':'str', 'customer code':'str'},
                      usecols=['Techbillto', 'customer code', 'Chain', 'customer name'])
    tmp = tmp.rename(columns={'Techbillto':'TECH_BILL_TO', 'customer code':'CUSTOMER_CODE'})
    tmp = tmp.dropna()
    return tmp


def add_partition(df, end_freeze_date, end_forecast_date, fintool_date):
    """
    Adds to the input dataframe a column containing the mark of train-freeze-test separation. The separation is based on input boundary dates. 
    :param df: pandas dataframe, required columns = ['Date', 'FT_SHIP_START']
    :param end_freeze_date: string formated like '%Y%m%d', means the last date of freeze period. 
    :param end_forecast_date: string formated like '%Y%m%d', means the last date of forecast period.
    :param fintool_date: string formated like '%Y%m%d', means the last date of train period/actual data available equals to the fintool version date.
    :return: pandas dataframe = input dataframe + partition column
    :notes: 1 - freeze period is a time range between last available actual data and the beginning of the forecast; 2 - we don't cut promo slots which 
    go beyond end_forecast_date.
    """
    start_freeze = utils.td(fintool_date)
    end_freeze = utils.td(end_freeze_date)
    end_forecast = utils.td(end_forecast_date)

    #подрезаем будущее
    lm, ly = end_forecast.month, end_forecast.year
    df['SLOT_MONTH'] = df['FT_SHIP_START'].apply(lambda t: 0 if str(t)=='0' else pd.to_datetime(t, format='%Y-%m-%d').month)
    df['SLOT_YEAR'] = df['FT_SHIP_START'].apply(lambda t: 0 if str(t)=='0' else pd.to_datetime(t, format='%Y-%m-%d').year)
    df = df[(df['Date']<=end_forecast) | ((df['SLOT_MONTH']==lm) & (df['SLOT_YEAR']==ly))]  ## торчащие слоты чтобы не обрезать
    df.drop(columns=['SLOT_MONTH', 'SLOT_YEAR'], inplace=True)

    df['Partition'] = np.where(df.Date<start_freeze, 'train', np.where(df.Date>end_freeze, 'fc', 'freeze'))

    return df


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_missing_zeros(sales, start_date, end_date):
    """
    Adds rows with zero ordered values that were not present in the original table to the dataframe in order to get an equidistant time series
    :param sales: pandas dataframe with date, key and shipment dates (corresponding columns = ['PGI_DATE', 'KEY', 'SHIP_START', 'SHIP_END'])
    :param start_date: string formated like '%Y%m%d', means the first date of history used
    :param end_date: string formated like '%Y%m%d', means the last date of forecast to be built
    :return: pandas dataframe with the same columns and with added rows
    """
    sales['PGI_DATE'] = pd.to_datetime(sales['PGI_DATE'], format='%Y-%m-%d')
    start_date = utils.td(start_date)
    end_date = utils.td(end_date)
    #добавляем пропущенные значения
    results_dict = defaultdict(list)
    for key, mdf in sales.groupby('KEY'):
        real_start_date = min(mdf['PGI_DATE'])
        key_start_date = max(real_start_date, start_date)
        dates = pd.date_range(start = key_start_date, end = end_date)
        results_dict['PGI_DATE'].extend(dates)
        results_dict['KEY'].extend([key] * len(dates))

    results = pd.DataFrame.from_dict(results_dict)
    sales = pd.merge(results, sales, how='left', on=['PGI_DATE', 'KEY'])

    sales['SHIP_START'] = sales['SHIP_START'].fillna('')
    sales['SHIP_END'] = sales['SHIP_END'].fillna('')
    sales.fillna(0, inplace=True)

    return sales


def clip_outs(ex):
    """
    Will be deprecated
    """

    tmp = ex[ex['Partition']=='train']
    tmp_std = tmp.groupby('key', as_index=False).agg({'Ordered':'std'})
    tmp_std = tmp_std.rename(columns={'Ordered':'std'})

    ex = pd.merge(ex, tmp_std, how='left', on='key')

    ex['Ordered'] = np.where((ex['Ordered']>3*ex['std'])&(ex['Partition']=='train'), ex['std'], ex['Ordered'])
    ex = ex.drop(columns=['std'])
    return ex


@utils.gc_collect
@utils.log_n_unique_keys
def exclude_inno(df, days=90):
    """
    Removes from input dataframe keys and associated with them data with short history (aka innovations)
    :param df: pandas dataframe, columns required = ['Date', 'Partition', 'Ordered']
    :param days: numbers of days to detect short history: products with train length < days are removed.
    :return: pandas dataframe with the same columns and with selectively deleted rows
    """

    end_train_date = df[df['Partition'] == 'train'].Date.max()

    inno_df = df[df['Ordered']!=0].groupby(['key'], as_index=False).agg({'Date':'min'})
    inno_df['inno_flag'] = inno_df['Date'] > (end_train_date - dt.timedelta(days))
    df = pd.merge(df, inno_df[['key', 'inno_flag']], how='left', on='key')
    del inno_df
    df = df[df['inno_flag'] == False]
    del df['inno_flag']
    return df


@utils.gc_collect
@utils.log_n_unique_keys
def get_inno_keys(df, days=90):
    """
    get inno keys from dataframe
    :param df: pandas dataframe, columns required = ['Date', 'Partition', 'Ordered']
    :param days: numbers of days to detect short history: products with train length < days are removed.
    :return: set of inno keys
    """
    end_train_date = df[df['Partition'] == 'train'].Date.max()

    inno_df = df[df['Ordered'] != 0].groupby(['key'], as_index=False).agg({'Date': 'min'})
    inno_df['inno_flag'] = inno_df['Date'] > (end_train_date - dt.timedelta(days))
    inno_keys = set(list(inno_df.loc[inno_df['inno_flag'] == True, 'key'].unique()))
    return inno_keys


#TODO если были одни инновации, то после их отсеивания сюда придёт пустой датафрейм

@utils.gc_collect
@utils.log_n_unique_keys
def exclude_delisted_keys(df, days=30):
    """
    Removes from input dataframe keys and associated with them data with recently zero sales (aka delistings)
    :param df: pandas dataframe, columns required = ['Date', 'Partition', 'Ordered']
    :param days: numbers of days to detect delistings: products with more than "days" recent zero sales are removed.
    :return: pandas dataframe with the same columns and with selectively deleted rows
    """
    end_test_date = df[df['Partition'] == 'train'].Date.max()

    delist_df = df[df['Ordered']!=0].groupby(['key'], as_index=False).agg({'Date':'max'})
    delist_df['delist_flag'] = delist_df['Date'] < (end_test_date - dt.timedelta(days))
    df = pd.merge(df, delist_df[['key', 'delist_flag']], how='left', on='key')
    del delist_df
    df = df[df['delist_flag'] == False]
    del df['delist_flag']
    return df


@utils.gc_collect
@utils.log_n_unique_keys
@utils.save_passed_dataframe_if_debug
def exclude_5ka_KiB(ex):
    """
    The function excludes some techbillto values (Chain='KRASNOE&BELOE') from data frame.
    
    :param ex: pd.DataFrame (required columns: 'KEY', 'techbillto')
    
    :return: pd.DataFrame
    """
    #TODO: check if it's still needed
    excl_list = [#'850064086',  #5ka south
#                  '850129619',
#                  '850167098',
#                  '850231995',
#                  '850251526',
#                  '850278195',
#                  '850278196',
#                  '850278197',
#                  '850278198',
#                 '850278199',  
                 '850238681',  #KiB TODO: вынести в get_simple_si
                 '850244903',
                 '850247763',
                 '850247992']

    ex['Techbillto'] = ex['KEY'].str.split("_", n=1, expand=True)[1]
    ex = ex[~ex['Techbillto'].isin(excl_list)]
    del ex['Techbillto']
    return ex


# def get_freeze_end():
#     """
#     Calculates last date of freeze period based on current date: in quarter run it shoud be the last date of the next month.
#     :return: string formated like '%Y%m%d', means freeze end date
#     """
#     current_date = dt.datetime.today()
#     year = current_date.year
#     month = current_date.month
#
#     if month==12:
#         month=1
#         year=year+1
#     else:
#         month+=1
#
#     day = calendar.monthrange(year, month)[1]
#
#     return dt.date(year, month, day).strftime(format='%Y%m%d')


def get_freeze_end(chess):
    """
    Calculates last date of freeze period based on current date: in quarter run it shoud be the last date of the next month.
    :return: string formated like '%Y%m%d', means freeze end date
    """
    if chess.lower() == 'true':
        chess_data = get_chess()
        freeze_end = pd.to_datetime(chess_data['SHIP_START'], format='%Y-%m-%d').min()
        ship_end_min = pd.to_datetime(chess_data['slot_end'], format='%Y-%m-%d').min()
    else:
        cols = ['Index', 'customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift']

        last_fintool = max(glob.glob('../data/raw/Uplifttool_AFTSnapshot_*.xlsx'))
        last_version = pd.to_datetime(last_fintool[-11: -5], format='%y%m%d')
        # TODO: col name for wd, esp (Mechanics) will be changed
        last_columns = cols + ['Off-invoice', 'On-invoice', 'Mechanics']

        last = pd.read_excel(last_fintool, skiprows=6, sheet_name='Data', usecols=last_columns)

        if len(last['Index'].dropna().unique()) != 1:
            logging.info('WARNING! The last fintool in the index column contains more than one quarter')
            raise ValueError('WARNING! The last fintool in the index column contains more than one quarter')

        last['Shipment start date'] = pd.to_datetime(last['Shipment start date'], format='%Y-%m-%d')
        freeze_end = last['Shipment start date'].min()
        ship_end_min = None

    return freeze_end.strftime(format='%Y%m%d'), ship_end_min


# def get_forecast_end(freeze_end):
#     """
#     Calculates last date of forecast period based on last date of freeze period: in quarter run the forecast period is equal to 3 months.
#     :param freeze_end: string formated like '%Y%m%d', means last date of freeze period
#     :return: string formated like '%Y%m%d', means forecast end date
#     """
#     freeze_end = dt.datetime.strptime(freeze_end, '%Y%m%d')
#     year = freeze_end.year
#     month = freeze_end.month
#     if month + 3 > 12:
#         month = (month+3)%12
#         year+=1
#     else:
#         month+=3
#     day = calendar.monthrange(year, month)[1]
#     return dt.date(year, month, day).strftime(format='%Y%m%d')


def get_forecast_end(chess):
    """
    Calculates last date of forecast period based on last date of freeze period: in quarter run the forecast period is equal to 3 months.
    :param freeze_end: string formated like '%Y%m%d', means last date of freeze period
    :return: string formated like '%Y%m%d', means forecast end date
    """
    if chess.lower() == 'true':
        chess_data = get_chess()
        forecast_end = pd.to_datetime(chess_data['slot_end'], format='%Y-%m-%d').max()
        ship_start_max = pd.to_datetime(chess_data['SHIP_START'], format='%Y-%m-%d').max()
    else:
        cols = ['Index', 'customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift']

        last_fintool = max(glob.glob('../data/raw/Uplifttool_AFTSnapshot_*.xlsx'))
        # TODO: col name for wd, esp (Mechanics) will be changed
        last_columns = cols + ['Off-invoice', 'On-invoice', 'Mechanics']

        last = pd.read_excel(last_fintool, skiprows=6, sheet_name='Data', usecols=last_columns)

        if len(last['Index'].dropna().unique()) != 1:
            logging.info('WARNING! The last fintool in the index column contains more than one quarter')
            raise ValueError('WARNING! The last fintool in the index column contains more than one quarter')

        last['Shipment end date'] = pd.to_datetime(last['Shipment end date'], format='%Y-%m-%d')
        forecast_end = last['Shipment end date'].max()
        ship_start_max = None

    return forecast_end.strftime(format='%Y%m%d'), ship_start_max


# def get_freeze_start():
#     """
#     Calculates first date of freeze period based on current date: in quarter run it shoud be the date preceding the current one (yesterday).
#     :return: string formated like '%Y%m%d', means freeze start date
#     """
#     yesterday = dt.datetime.today() - dt.timedelta(days=1)
#     return yesterday.strftime(format='%Y%m%d')


def get_freeze_start():
    new_file_name = max(glob.glob(r'../data/raw/Uplifttool_sellin_[0-9][0-9][0-9][0-9][0-9][0-9].csv'))

    columns = ['PGI_DATE', 'ORDER_QUAN', 'PGI_DATA']
    dtype = {
        'PGI_DATE': 'str',
        'ORDER_QUAN': 'float'
    }

    df = pd.read_csv(new_file_name, sep=';', dtype=dtype, usecols=columns)
    df = df[(df['PGI_DATA'] > 0) & (df.ORDER_QUAN >= 0)]

    df['PGI_DATE'] = pd.to_datetime(df['PGI_DATE'])
    freeze_start = df['PGI_DATE'].max()

    return freeze_start.strftime(format='%Y%m%d')


def get_history_start(freeze_start):
    """
    Calculates first date of train period based on first date of freeze period: by default train contains 547 days (6 full quarters).
    :param freeze_start: string formated like '%Y%m%d', means first date of freeze period
    :return: string formated like '%Y%m%d', means history start date
    """
    freeze_start = dt.datetime.strptime(freeze_start, '%Y%m%d')
    freeze_start = freeze_start - dt.timedelta(days=547)
    return freeze_start.strftime(format='%Y%m%d')


@utils.gc_collect
def add_last_snapshot_to_fintool():
    """
    The function adds last updates the processed file fintool.csv with the information from raw file Uplifttool_AFTSnapshot_yymmdd.xlsx.

    :return: None
    """
    cols = ['customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift', '# of promo']

    last_fintool = max(glob.glob('../data/raw/Uplifttool_AFTSnapshot_*.xlsx'))
    last_version = pd.to_datetime(last_fintool[-11: -5], format='%y%m%d')
    # TODO: col name for wd, esp (Mechanics) will be changed
    last_columns = cols + ['Off-invoice', 'On-invoice', 'Mechanics']

    last = pd.read_excel(last_fintool, skiprows=6, sheet_name='Data', usecols=last_columns)
    last['Ship_start_month'] = pd.to_datetime(last['Shipment start date'], format='%Y-%m-%d').dt.month
    last['Ship_start_year'] = pd.to_datetime(last['Shipment start date'], format='%Y-%m-%d').dt.year
    last['Total Discount'] = last['Off-invoice'] + last['On-invoice']
    del last['Off-invoice']
    del last['On-invoice']
    last['KAM Volume with uplift'] = last['KAM Volume with uplift'].replace(' ', 0)
    last['KAM Volume with uplift'] = last['KAM Volume with uplift'].astype(float)

    utils.add_wd_from_string_col(last, col='Mechanics')
    utils.add_esp_from_string_col(last, col='Mechanics')
    del last['Mechanics']

    last['Versions'] = last_version
    cols += ['Versions', 'Total Discount']

    cum_data = auxiliary.get_processed_fintool(use_columns=cols)
    if 'wd' not in cum_data.columns:
        cum_data['wd'] = 0.0
    if 'esp' not in cum_data.columns:
        cum_data['esp'] = 0.0

    cum_data = cum_data[cum_data.Versions != last_version]
    cum_data = pd.concat([cum_data, last])

    cum_data.to_csv('../data/processed/fintool.csv', index=False, encoding='windows-1251')


def update_pinc():
    """
    Function scan ../data/raw folder to get price change files by mask Uplifttool_PriceChanges_*.csv'
    Data from these files swap data in preprocessed pinc file by the window.
    If the preprocessed file does not contain the 'window' column will be created automatically.
    If price changes faced a few times last will be used.

    :return: None
    """
    pinc = features.get_processed_pinc()
    pinc.PINC.fillna(0, inplace=True)

    product_mapping = get_product_mapping()[['SKU', 'PRODUCT_CODE']].drop_duplicates()

    files = glob.glob('../data/raw/Uplifttool_PriceChanges_*.csv')
    last_windows = []
    last_pinc_dfs = []
    for file in files:
        window = file[-9:-4]  # 'W4_20
        window = window[-2:] + '_' + window[:2]  # 'W4_20 -> 20_W4

        last_windows.append(window)

        df = pd.read_csv(file, usecols=['Month', 'SAP SKU', 'Price Change'],
                         sep=';', decimal=',', dtype={'SAP SKU': str})#, 'Price Change': float})
        df['Price Change'] = df['Price Change'].astype('float')

        df[['Y', 'M']] = df.Month.str.split('-', expand=True)
        del df['Month']

        df.rename(columns={'SAP SKU': 'SKU', 'Price Change': 'PINC'}, inplace=True)

        df = df.merge(product_mapping, how='left')
        df.dropna(inplace=True)  # what we should do if missed product codes there are
        df.drop_duplicates(inplace=True)
        del df['SKU']

        df['PINC'] = df['PINC'].round(decimals=4)
        df['window'] = window

        print('df.columns', df.columns)

        last_pinc_dfs.append(df)

    if 'window' not in pinc.columns:
        pinc['window'] = 'default'

    pinc = pinc[~pinc.window.isin(last_windows)]
    pinc = pd.concat([pinc] + last_pinc_dfs)

    # на float ожидаемо не работает groupby
    pinc['Y'] = pinc.Y.astype(int)
    pinc['M'] = pinc.M.astype(int)
    pinc = pinc.groupby(['PRODUCT_CODE', 'Y', 'M'], as_index=False).agg({'PINC': 'last', 'window': 'last'})

    pinc.to_csv('../data/processed/pinc.csv', index=False)


@utils.gc_collect
def update_shelf_prices():
    """
    Function updates processed shelf price data
    Function scans ../data/raw folder by mask 'Uplifttool_ShelfPrice_*.txt'
    The function doesn't modify the preprocessed file if there are not any new files in the raw folder
    Function updates existed data if it is existing in the preprocessed file.
    (old data for this date will be removed and new data will be added)

    :return: None
    """

    processed = features.get_processed_prices()

    tmp = []
    last_dates = []
    for file in glob.glob(r'../data/raw/Uplifttool_ShelfPrice_*.txt'):
        df = pd.read_csv(file, sep=';', dtype={'Ship To': str})
        df['Data'] = pd.to_datetime(df['Data'], format='%Y%m%d')
        df['Data'] = df['Data'].astype(str)
        df.rename(columns={'Ship To': 'ShipToID', 'Data': 'Date'}, inplace=True)

        last_dates.extend(df['Date'].unique())
        tmp.append(df)

    if tmp:
        processed = pd.concat([processed[~processed.Date.isin(last_dates)], ] + tmp)
        processed.to_csv(r'../data/processed/shelf_price.csv', index=False)


@utils.gc_collect
def update_wd():
    """
    The function updates preprocessed wd file.
    The function doesn't modify the preprocessed file if features.get_latest_wd() returned empty dataframe.
    The function updates existed data if it is existing in the preprocessed file.
    (old data for these dates will be removed and new data will be added)

    :return: None
    """

    latest = features.get_latest_wd()
    if latest.empty:
        return

    processed = features.get_processed_wd()

    first_y_w = latest[['Y', 'W']].drop_duplicates()
    first_y_w = first_y_w.sort_values(['Y', 'W'], ignore_index=True)
    first_y, first_w = first_y_w.loc[0].values

    indexer = ~(
            (processed.Y > first_y)
            | ((processed.Y == first_y) & (processed.W >= first_w))
    )

    processed = processed[indexer]

    wd = pd.concat([processed, latest], ignore_index=True)
    wd.sort_values(['key', 'Y', 'W'], inplace=True)
    wd = wd.groupby(['key', 'Y', 'W'], as_index=False).mean()

    wd.to_csv(r'../data/processed/wd.csv', index=False)


def update_customer_plans():
    file = '../data/processed/customer_plans.csv'
    customer_plans = pd.read_csv(file,
                                 encoding='windows-1251',
                                 parse_dates=['Shipment Start', 'Shipment End', 'Store Start', 'Store End'])
    latest_plans = utils.get_latest_customer_plans()
    latest_periods = latest_plans.groupby('Chain', as_index=False).agg({'Shipment Start': min, 'Shipment End': max})

    for _, chain, start, end in latest_periods.itertuples():
        indexer = (customer_plans.Chain == chain) & (customer_plans['Shipment Start'] >= start) & (
                    customer_plans['Shipment End'] <= end)
        customer_plans = customer_plans[~indexer]

    customer_plans = pd.concat([customer_plans, latest_plans])

    customer_plans.to_csv('../data/processed/customer_plans.csv', index=False, encoding='windows-1251')


def update_processed_files(upd_customer_plans=True):
    """
    Function updates preprocessed files
    :param: upd_customer_plans: if True customer plans will be updated. Default: True
    :return: None
    """

    update_pinc()
    update_shelf_prices()
    update_wd()
    if upd_customer_plans:
        update_customer_plans()


def update_product_MD():
    """
    Function updates the file Product_MD from masterdata with the latest Upliftool_MDM_Products_yymmdd.csv from raw files
    :return: None
    """

    d6_mask = r'[0-9]' * 6
    mask = fr'../data/raw/Uplifttool_MDM_Products_{d6_mask}.csv'
    if len(glob.glob(mask)):
        updating_path = max(glob.glob(mask))
    else:
        msg = f"update_product_MD : there are no files in data/row/ to update product_MD.csv, updating is skipped."
        logging.warning(msg)
        return

    path = r'../data/masterdata/Product_MD.csv'
    md = pd.read_csv(path, sep=';', encoding='windows-1251', dtype={'SKU': str, })
    md.SKU = md.SKU.str.zfill(6)

    latest_md = pd.read_csv(updating_path, sep=';', encoding='windows-1251', dtype={'SKU': str, })
    latest_md.SKU = latest_md.SKU.str.zfill(6)

    # считаем, что Product code не может измениться у скю
    # поэтому берем только новые скю
    # считаем, что sku не может перетекать из одной линейки в другую

    used_sku = md.SKU.unique()
    latest_md = latest_md[~latest_md.SKU.isin(used_sku)]
    if latest_md.empty:
        logging.info('update_product_MD: nothing to update.')
        return

    md = pd.concat([md, latest_md])
    md.drop_duplicates(subset=['SKU', 'PRODUCT_CODE', 'Sku/линейка'], inplace=True)

    md['PRODUCT_CODE'] = md.groupby('Sku/линейка')['PRODUCT_CODE'].transform(max)

    nan_sku_lin = md[md.PRODUCT_CODE.isna() & md['Sku/линейка'].isna()]
    if len(nan_sku_lin):
        logging.warning('update_product_MD: found string where PRODUCT_CODE and Sku/линейка missed')
        md = md[~(md.PRODUCT_CODE.isna() & md['Sku/линейка'].isna())]

    # generate new product codes for new 'Sku/линейка'
    last_code = int(md['PRODUCT_CODE'].max())
    new_sku_lins = md[md.PRODUCT_CODE.isna()]['Sku/линейка']
    new_sku_lins = new_sku_lins.dropna().unique()
    for idx, sku_lin in enumerate(new_sku_lins, last_code + 1):
        md.loc[md['Sku/линейка'] == sku_lin, 'PRODUCT_CODE'] = idx

    md['PRODUCT_CODE'] = md['PRODUCT_CODE'].astype(int).astype(str).str.zfill(4)
    md.to_csv(r'../data/masterdata/Product_MD.csv', encoding='windows-1251', sep=';', index=False)


def update_version_dict():
    """
    The function read version dict file for aft and updates|create record for the following quarter.

    :return: pandas.DataFrame that contains updated version dict
    """
    versions_dict = pd.read_csv(danone.VERSIONS_DICT_PATH, sep=":", header=None)
    versions_dict = dict(zip(versions_dict[0], versions_dict[1]))

    current_month = dt.datetime.today().month
    current_year = dt.datetime.today().year
    q_by_month = {m + 1: (m + 3) // 3 for m in range(0, 12)}
    current_quarter = q_by_month[current_month]

    next_q = f'{current_year}Q{current_quarter + 1}' if current_quarter < 4 else f'{current_year + 1}Q1'

    aft_files = glob.glob('../data/raw/Uplifttool_AFTSnapshot_*.xlsx')
    aft_files = sorted(aft_files)

    latest_aft = max(aft_files)
    s_date = latest_aft[-11:-5]
    s_date = dt.datetime.strptime(s_date, '%y%m%d').strftime('%d.%m.%Y')

    versions_dict[next_q] = s_date

    df = pd.DataFrame.from_dict(data=versions_dict, orient='index', columns=[1]).reset_index()
    df.rename(columns={'index': 0}, inplace=True)
    df.to_csv(danone.VERSIONS_DICT_PATH, index=False, sep=':', header=False)

    return df


def update_masterdata():
    """
    Function updates masterdata files
    :return: None
    """

    update_product_MD()
    update_version_dict()


@utils.gc_collect
def run_preprocessing(fc_type='PROD', promo_source='Fintool', save_preprocessed=True,
                      freeze_start=None, freeze_end=None, forecast_end=None,
                      use_prev_fintool=False, use_prev_customer_plans=False, use_prev_simple_si=False,
                      chess=True,
                      filters=None,
                      auto_aft_versions=False):
    """
    Calls a sequence of functions to assemble incoming data into a single dataframe.
    Writes the execution reports to a log file.
    Writes the collected dataframe to a separate file.

    :param fc_type: string, one of ['PROD', 'CUSTOM']. PROD is used for quarterly run in the very beginning of planning cycle.
    When PROD, automaticaly calculated boundary dates are used (see get_freeze_start, get_freeze_end functions).
    :param promo_source: string, one of ['Fintool', 'Chess']. Defines the source for promo plan data, whether it is AutoFinTool upload or Chess excel files.
    :param save_preprocessed: boolean, True if it is need to write the preprocessed dataframe to the SI_full.csv.
    :param freeze_start: string formated like '%Y%m%d', custom freeze start date.
    :param freeze_end: string formated like '%Y%m%d', custom freeze end date.
    :param forecast_end: string formated like '%Y%m%d', custom forecast end date.
    :param use_prev_fintool: boolean, defines if it is neeed to update fintool (promo plan) data. True if existing file should be used.
    :param use_prev_customer_plans: boolean, defines if it is neeed to update customer plan data. True if existing file should be used.
    :param use_prev_simple_si: boolean, defines if it is neeed to update sell in data. True if existing file should be used.
    :param filters:
    :param chess: True or False. If True chess data will be used/ If False AFT data will be used
    :param auto_aft_versions: Boolean. passed to auxiliary.write_fintool_by_dates as 'auto_version' parameter
    :return: pandas dataframe to be passed to the forecasting model.
    :notes:
    """

    logging.info('update_masterdata')
    update_masterdata()
    
    logging.info('update_processed_files')
    upd_customer_plans = not use_prev_customer_plans
    update_processed_files(upd_customer_plans=upd_customer_plans)

    freeze_start = get_freeze_start() if freeze_start is None else freeze_start
    freeze_end, ship_end_min = get_freeze_end(chess) if freeze_end is None else (freeze_end, None)
    forecast_end, ship_start_max = get_forecast_end(chess) if forecast_end is None else (forecast_end, None)
    print(f'freeze_start = {freeze_start}')

    if ship_end_min is not None:
        if ship_start_max - ship_end_min > pd.Timedelta('100 days'):
            logging.info(f"min ship end in chess = {ship_end_min} and max ship start in chess = {ship_start_max}")
            raise ValueError("Chess are filled in incorrectly for autodetecting freeze end and forecast end.")


    if pd.to_datetime(freeze_start, format='%Y%m%d') >= pd.to_datetime(freeze_end, format='%Y%m%d'):
        logging.info(f'freeze_start={freeze_start} and freeze_end={freeze_end}')
        if fc_type == 'HIST':
            freeze_start = pd.to_datetime(freeze_end, format='%Y%m%d') - dt.timedelta(days=1)
            freeze_start = freeze_start.strftime(format='%Y%m%d')
            logging.info(f'new freeze_start={freeze_start}')
        elif fc_type == 'PROD':
            freeze_end = pd.to_datetime(freeze_start, format='%Y%m%d') + dt.timedelta(days=1)
            freeze_end = freeze_end.strftime(format='%Y%m%d')
            logging.info(f'new freeze_end={freeze_end}')

    history_start = get_history_start(freeze_start)

    # TODO: We dont't need here parameters fc_type and promo_source. Need to discuss.
    if not use_prev_fintool:
        # logging.info('auxiliary.fintool_to_csv')
        # auxiliary.fintool_to_csv()
        add_last_snapshot_to_fintool()

        logging.info('write_fintool_by_dates(versions_dict)')
        versions_dict = get_version_dict()
        if fc_type == 'PROD':
            auxiliary.write_fintool_by_dates(versions_dict, auto_version=auto_aft_versions)
        else:
            auxiliary.write_fintool_by_dates(versions_dict, auto_version=auto_aft_versions, freeze_start=freeze_start)


    # TODO: Ask @Nat: can we move exclude_5 into get_simple_SI: so-so
    ex = get_si(use_prev_simple_si, history_start, filters)

    logging.info(f'getting future promo: chess={chess}')
    future_promo = features.get_future_promo(freeze_end, forecast_end, option='Quarter', chess=chess)
    last_promo_date = future_promo.PGI_DATE.max().strftime('%Y%m%d')
    
    logging.info('exclude_5ka_KiB(ex)')
    ex = exclude_5ka_KiB(ex)

    logging.info('add_missing_zeros(ex, history_start, forecast_end)')
    ex = add_missing_zeros(ex, history_start, max(forecast_end, last_promo_date))
   
    logging.info('features.add_act_promo(ex)')
    ex = features.add_act_promo(ex)
    
    logging.info('features.add_fintool')
    ex = features.add_fintool(ex, future_promo)
    
    logging.info('rename + drop')
    ex.rename(columns={'PGI_DATE': 'Date', 'KEY': 'key', 'GI_QUAN': 'Delivered', 'ORDER_QUAN': 'Ordered'}, inplace=True)
    del ex['Delivered']

    logging.info('features.add_calendar(ex)')
    business_features = ex.columns
    ex = features.add_calendar(ex, ymw_only=True)
    calendar_features = list(set(ex.columns) - set(business_features))
    
    ex, _ = utils.reduce_mem_usage(ex)

    logging.info('features.add_PINC(ex)')
    ex = features.add_PINC(ex)

    logging.info('features.add_inno(ex)')
    ex = features.add_inno(ex)

    logging.info('features.add_delisting_mark_for_product(ex)')
    ex = features.add_delisting_mark_for_product(ex)

    logging.info('add_partition')
    ex = add_partition(ex, freeze_end, forecast_end, freeze_start)

    logging.info('add_stock_features')
    # TODO: ask Nat why dixy so special - потому что для них работает
    ex = features.add_stock_features(ex, apply_to = [('DIXY', 'DRINKING YOGHURTS'),
                                                    ('DIXY', 'KEFIR')],
                                    cutoff_date=freeze_start, filters=filters)

    logging.info('add_WD')
    ex = features.add_WD(ex)

    # add_wd_from_chess()

    # TODO: create function combine_history_future(df, historical_columns, future_columns)
    # распихать по функциям
    logging.info('align  "Discount on date", "Promo_day_num", "Promo_day_num_backward", "WD"')
    ex['Discount on date'] = np.where(ex['Partition']=='train', ex['Discount on date'], ex['FT_DISCOUNT'])
    ex['Promo_day_num'] = np.where(ex['Partition']=='train', ex['Promo_day_num'], ex['FT_Promo_day_num'])
    ex['Promo_day_num_backward'] = np.where(ex['Partition']=='train', ex['Promo_day_num_backward'], ex['FT_Promo_day_num_backward'])
    ex['WD'] = np.where(ex['Partition'] == 'train', ex['WD'], ex['FT_WD'])
    
    logging.info("del ['FT_DISCOUNT', 'FT_Promo_day_num', 'FT_Promo_day_num_backward']")
    for col in ['FT_DISCOUNT', 'FT_Promo_day_num', 'FT_Promo_day_num_backward']:
        if col in ex.columns:
            del ex[col]
    _ = gc.collect()
    
    logging.info('add_profile')
    ex = features.add_profile(ex)

    logging.info('exclude_wo_promo')
    ex = features.exclude_wo_promo(ex)

    # logging.info('exclude_inno')
    # TODO: ask Nat: can we exclude_inno earlier?
    # ex = exclude_inno(ex)
    logging.info('exclude_delisted_keys')
    ex = exclude_delisted_keys(ex)


    logging.info('get_inno_keys')
    inno_keys = get_inno_keys(ex)


    logging.info('add_customer_plans')
    # TODO: optimize call below: if apply_to and filters does not have intersections: skip this step
    ex = features.add_customer_plans(ex, apply_to=[], cutoff_date=freeze_start)

    logging.info('add_seasonal_promo')
    ex = features.add_seasonal_promo(ex)

    # TODO: add from chess data
    logging.info('add_prices')
    ex = features.add_prices(ex, filters)

    logging.info('add_coinvest')
    ex = features.add_coinvest(ex)
    
    ex['ESP'] = np.where(ex['Partition'] == 'train', ex['Promo_price'], ex['FT_ESP'])

    # ft_price = pd.read_csv('../data/processed/test_ft_11_23_for_Q1.csv')

    # ft_price['FT_SHIP_START'] = ft_price['FT_SHIP_START'].astype('str')
    # ft_price['FT_SHIP_START'] = ft_price['FT_SHIP_START'].str[:10]

    # ex['FT_SHIP_START'] = ex['FT_SHIP_START'].astype('str')
    # ex['FT_SHIP_START'] = ex['FT_SHIP_START'].str[:10]

    # ex = ex.merge(ft_price, how='left', on=['key', 'FT_SHIP_START'])
    # ex['Min Shelf Price'] = ex['Min Shelf Price'].fillna(0)

    # ex['ESP'] = np.where(ex['Partition'] == 'train', ex['Promo_price'], ex['Min Shelf Price'])

    if save_preprocessed:
        logging.info('saving preprocessed')
        ex.to_csv(f'../data/processed/SI_full.csv', index=False)

    # TODO: найти причину
    ex['FT_SHIP_START'] = ex['FT_SHIP_START'].astype(str)

    logging.info('preprocessing done')
    return ex, calendar_features, inno_keys


def get_version_dict():
    versions_dict = pd.read_csv(danone.VERSIONS_DICT_PATH, sep=":", header=None)
    versions_dict = dict(zip(versions_dict[0], versions_dict[1]))
    return versions_dict


@utils.log_n_unique_keys
@utils.save_passed_dataframe_if_debug
def get_si(use_prev_simple_si, history_start, filters=None):
    """
    The function preprocesses all available sell-in data if not asked to use previous; otherwise reads the previous preprocessed data.

    :param use_prev_simple_si: if True, then the reading of existing simple_SI file occurs; otherwise the file is collected again
    :param history_start: str (in format yymmdd); the date of first non-zero sale
    :param filters: dict; see the documentation string of function filter_data from preprocessing.py for the details

    :return: pd.DataFrame with all available/requested sell-in in corresponding format
    """
    if not use_prev_simple_si:
        logging.info('get_simple_SI(history_start)')
        ex = get_simple_SI(history_start=history_start, filters=filters)
    else:
        logging.info('reading simple_SI')
        ex = pd.read_csv('../data/processed/SI_simple.csv',
                         dtype={'SHIP_START': 'str', 'SHIP_END': 'str', 'PROMO_ID': 'str'})
        if filters is not None:
            ex = filter_data(ex, filters)
        ex, _ = utils.reduce_mem_usage(ex)
    return ex


def add_features_in_sample(mdf):
    """
    Adds to input dataframe columns with calendar, holiday and big discount features
    :param mdf: pandas dataframe
    :return: pandas dataframe with added columns
    :notes: usually applied to dataframe containing one key.
    """
    mdf = features.add_calendar(mdf)
    mdf = features.add_holiday_features(mdf)
    mdf = features.add_big_disc(mdf)

    return mdf


def extract_data_from_chess_sheet(file, xls, sheet):
    """
    The function collects the data with promo plans from the specified sheet of specified .xlsm file with specified path.
    
    :param file: str; the path to the .xlsm file
    :param xls: pandas.io.excel._base.ExcelFile; read .xlsm file
    :param sheet: str; the name of desired sheet of .xlsm file
    
    :return: pd.DataFrame storing the data about promo plans from specified sheet of specified input file
    """
    df = xls.parse(sheet)
    header = utils.get_chess_header(df)
    if header == -1:
        msg = f"get_chess func:  can't define header position for file {file} - sheet {sheet}. Sheep skipped."
        logging.warning(msg)
        return pd.DataFrame()

    table = xls.parse(sheet, header=header)
    table.dropna(subset=['Product name'], inplace=True)

    # fix space in columns
    for col in table.columns:
        table.loc[table[col] == ' ', col] = np.nan

    ship_starts_from_idx = table.columns.get_loc("Product name") + 1
    ship_starts = table.columns[ship_starts_from_idx:].values

    ship_ends = df.iloc[header - 2].values
    ship_ends = ship_ends[ship_starts_from_idx:]

    periods = ['{}|{}'.format(*items) for items in zip(ship_starts, ship_ends)]
    table.columns = table.columns[:ship_starts_from_idx].values.tolist() + periods
    table.rename(columns={'Сеть': 'Chain'}, inplace=True)
    cols_to_del = set([col for col in table.columns if col.startswith('Unnamed')] + \
                      [col for col in table.columns if col not in {'Chain', 'Product name'} and '|' not in col] + \
                      [col for col in table.columns if col == ' ' or 'nan' in col])

    for col in cols_to_del:
        del table[col]

    df = table.melt(id_vars=['Chain', 'Product name'], value_name='data', var_name='period')
    df.dropna(inplace=True)

    if df.empty:
        return df

    # flag for metron traiders slots
    df['Metro_traiders'] = False

    def metro_fix(data_str):
        if 'traders' in str(data_str):
            return True
        else:
            return False

    df.loc[df['data'].apply(lambda x: metro_fix(x)), 'Metro_traiders'] = True

    clean_chess_data_col_4_parsing(df)

    df[['start', 'end']] = df.period.str.split('|', expand=True)

    secs_list = utils.combine_promo_slots_in_chess(df)
    utils.add_ship_start_in_chess(df, secs_list)

    df['data_str'] = df.data.apply(lambda x: x if isinstance(x, str) else '')
    df['data_str'] = df['data_str'].str.strip()

    if df.data_str.max() == '':
        df.rename(columns={'data': 'discount'}, inplace=True)
        df['wd'] = 0
        df['esp'] = 0
        del df['data_str']
    else:
        df[['discount', 'wd', 'esp']] = df.data_str.str.split(',', expand=True)
        df['discount'] = df['discount'].str.strip(' %').replace('', '0').astype('float')
        df['discount'] = np.where(df['data_str'] == '', df.data, df.discount)

        df['wd'] = np.where(df.wd.isna() | (df.wd == ''), 'wd=0', df.wd)
        df['esp'] = np.where(df.esp.isna() | (df.esp == ''), 'esp=0', df.wd)

        df.wd = df.wd.str.split('=', expand=True)[1].str.strip(' %').astype('float') / 100
        df.esp = df.esp.str.split('=', expand=True)[1].str.strip(' %').astype('float')

        for col in ['data', 'data_str']:
            del df[col]

    for col in ['discount', 'wd', 'esp']:
        df[col] = np.where(df[col].isin(list(string.whitespace)), '0', df[col])
        df[col] = df[col].astype(float)

    # TODO: fix this hack more accurate. Find and fix the reason.
    for col in ['discount', 'wd']:
        df[col] = np.where(df[col] < 1, df[col] * 100, df[col])

    return df


def clean_chess_data_col_4_parsing(df: pd.DataFrame):
    """
    The function clean 'data' col in the passed data frame.
    Cleaning happens only for string values: all symbols except string.digits and '=,.' will be removed
    The function also add commas in the end if commas count less 2.

    :param df:
    :return:
    """

    allowed_simbols = string.digits + '=,.'
    indexer = df.data.apply(lambda x: isinstance(x, str))
    filter_lambda = lambda x: ''.join(filter(lambda y: y in allowed_simbols, x))
    df.loc[indexer, 'data'] = df[indexer].data.apply(filter_lambda)

    def add_commas(s, expected_count=2):
        comma_count = s.count(',')
        return s + ',' * (expected_count - comma_count)

    df.loc[indexer, 'data'] = df[indexer].data.apply(lambda x: add_commas(x))


def get_chess(freeze_end=None, forecast_end=None):
    """
    The function create tall dataframe from chess files.
    The function scanning '../data/raw/' folder to get files list by mask 'Chess*.xlsm'.
    In every file will be processed sheets that names start with 'I - '.

    To define where table starts function use utils.get_chess_header(df) function.
    If get_chess_header couldn't get header position for sheet sheet will be skipped.

    Assumptions:
        - Ship start dates placed on the same row as 'Product name' label
        - Ship ends dates placed on previous row
        - The 'Product name' columns is the last column in the table: only dates of promo placed later

    :param freeze_end: string that contains in '%Y%m%d' format
    :param forecast_end: string that contains in '%Y%m%d' format
    :return: pd.DataFrame(columns=['Chain', 'product_name', 'date', 'discount', 'wd', 'esp'])
    """

    if freeze_end is not None:
        freeze_end = datetime.strptime(freeze_end, '%Y%m%d').strftime('%Y-%m-%d')
        forecast_end = datetime.strptime(forecast_end, '%Y%m%d').strftime('%Y-%m-%d')

    # TODO: объединенные ячейки лежат только на первой
    files = glob.glob(r'../data/raw/Chess*.xlsm')

    tmp = []
    for file in files:

        xls = pd.ExcelFile(file)
        sheets = [sheet for sheet in xls.sheet_names if sheet.startswith('I - ')]

        for sheet in sheets:

            try:
                df = extract_data_from_chess_sheet(file, xls, sheet,)
                if df.empty:
                    continue
                df['slot_end'] = df.end

                # на шахматке карусели в Q4 2021 возник артефакт, дата прочиталась как 2021-09-27 00:00:00.100 из-за
                # чего не мержились слоты
                df.start = df.start.str[:10]
                df.end = df.end.str[:10]

                df.start = pd.to_datetime(df.start)
                df.end = pd.to_datetime(df.end)
                #del df['period']

                df.rename(columns={'Product name': 'product_name'}, inplace=True)

                flat_df = utils.expand_df_period(df, start='start', end='end')
                # # TODO: flat_df.SHIP_START >= freeze_end ? slot on start of fc horizon

                if freeze_end is not None:
                    indexer = (pd.to_datetime(flat_df.slot_end) > freeze_end) & (pd.to_datetime(flat_df.SHIP_START) <= forecast_end)

                    flat_df = flat_df[indexer]
                
                # condition real period for metro_traiders
                if not flat_df.empty:
                    
                    agg_list = ['Chain', 'product_name', 'date']
                    flat_df = metro_traiders(flat_df, agg_list) 


                del flat_df['period']

                #if not flat_df.empty:
                #    flat_df = flat_df.groupby(['Chain', 'product_name', 'date'], as_index=False).max()
                # del flat_df['slot_end']
                
                if not flat_df.empty:
                    if flat_df['Chain'].values[0].upper() == 'PEREKRESTOK':
                        temp_df = flat_df.copy()
                        temp_df['Chain'] = 'PEREKRESTOK ONLINE'
                        flat_df = flat_df.append(temp_df)

                tmp.append(flat_df)
            except (ValueError, AssertionError):

                msg = f"get_chess func:  can't parse file {file} - sheet {sheet}. Sheep skipped."
                logging.error(msg)
                logging.error(traceback.format_exc())
                continue

    return pd.concat(tmp, ignore_index=True)

def metro_traiders(flat_df, agg_list, metro_traiders=True):
    '''Function for metro traiding mapping
       :param
        df
        agg_list - list of aggregation slots
        metro_traiders (default=True) - mapping metro traiding slots or not
       
       :return 
        df'''
    

    if metro_traiders:
        
        # mapping traiders period
        traiders = flat_df.groupby(['Chain', 'product_name', 'SHIP_START'],
                                   as_index=False).agg({'Metro_traiders': lambda x: True if True in x.unique() else False})
        traiders = traiders.rename(columns={'Metro_traiders': 'mapping'})
        flat_df = flat_df.merge(traiders, on=['Chain', 'product_name', 'SHIP_START'], how='left')
        flat_df['start']=flat_df['period'].str.split('|', expand=True)[0]
        flat_df['end']=flat_df['period'].str.split('|', expand=True)[1]
        flat_df['start'] = pd.to_datetime(flat_df['start'], format='%Y-%m-%d', errors='coerce')
        flat_df['end'] = pd.to_datetime(flat_df['end'], format='%Y-%m-%d', errors='coerce')
        flat_df['date'] = pd.to_datetime(flat_df['date'], format='%Y-%m-%d', errors='coerce')
        # delete slots with metro traiders not in real slot period
        flat_df = flat_df[~((flat_df['mapping']==True) & ((flat_df['date']<flat_df['start']) | (flat_df['date']>flat_df['end'])))]
        
        del flat_df['mapping']
        del flat_df['start']
        del flat_df['end']
        
        flat_df['cross_slots'] = flat_df['Metro_traiders']
        #flat_df['cross_slots'] = (flat_df['date'] <= pd.to_datetime(flat_df['period'].str.split('|', expand=True)[1])) \
        #                    & (flat_df['date'] >= pd.to_datetime(flat_df['period'].str.split('|', expand=True)[0]))
        flat_df['traiders_discount'] = flat_df['discount']
    
        # conditions for metro traiders

        traiders_condition = lambda x: False if False in x.unique() else True
        traiders_discount = lambda x: x.mean() if x.nunique() > 1 else x.max()
        traiders_cross_slots = lambda x: True if x.nunique() > 1 else False
        col = flat_df.columns.to_list()

        cond_dict = {'Metro_traiders': traiders_condition, 'traiders_discount': traiders_discount, 'cross_slots': traiders_cross_slots}
        col = [value for value in col if (value not in agg_list) & (value not in cond_dict.keys())]
    
        agg_dict = defaultdict(list)
        agg_dict = dict(list({i: 'max' for i in col}.items()) + list(cond_dict.items()))
    
        flat_df = flat_df.groupby(agg_list, as_index=False).agg(agg_dict)
                                        
        # cross slots
        #flat_df['cross_slots'] = flat_df['Metro_traiders'] & flat_df['cross_slots']
                   
        # mean discount for traiders slots
        is_traiders_slots = lambda x: True if True in x.unique() else False
        metro_traiders_mean = flat_df.groupby(['Chain', 'product_name', 'SHIP_START'], as_index=False).agg({'Metro_traiders': is_traiders_slots,
                                            'discount': 'mean'}).rename(columns={'Metro_traiders': 'is_traiders', 'discount': 'mean'})
        flat_df = flat_df.merge(metro_traiders_mean, on=['Chain', 'product_name', 'SHIP_START'], how='left')
        flat_df.loc[flat_df['is_traiders']==True, 'discount'] = flat_df[flat_df['is_traiders']==True]['mean']
                 
        del flat_df['is_traiders']
        del flat_df['mean']
    
    else:
        col = flat_df.columns.to_list()
        col = [value for value in col if (value not in agg_list)]
        agg_dict = {i: 'max' for i in col}
        flat_df = flat_df.groupby(agg_list, as_index=False).agg(agg_dict)
        
    return flat_df