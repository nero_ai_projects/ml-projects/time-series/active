def check_filters(filters: dict):
    """
    Function checks filters and return list of errors
    :param filters: dict with filters
    :return: list of errors
    """
    errors = []

    top_levels = filters.keys()
    if not top_levels:
        errors.append('filters do not contain chains and products. At least one of them needed')

    if 'Product' in top_levels:
        product_levels = filters['Product'].keys()
        if len(product_levels) > 1:
            errors.append('Got 2 product filters, but need only one')

    return errors


