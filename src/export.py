import os
import pyodbc
import pandas as pd
import win32com.client

# === default parameters
default_access_file = '../results/outputs/ForecastDB.accdb'

customer_md_table = 'CustomerMD'
customer_md_fields = ['Techbillto', 'Chain']

product_md_table = 'ProductMD'
product_md_fields = ['PRODUCT_CODE', 'PRD_ProdType', 'PRD_UmbrellaBrand', 'Sku/линейка']

chain_field = 'Chain'
chain_to_db_replacement = { # in case name of table does not match that in data (e.g. no exclamation marks are possible)
    'DC DA!': 'DA',
    'DC VICTORIA': 'VICTORIA'
}
data_fields = ['Date', 'Ordered', 'Partition', 'SHIP_START', 'FT_SHIP_START', 'Forecast', 'Techbillto', 'PRODUCT_CODE', 'Discount on date']
datetime_fields = ['Date', 'SHIP_START', 'FT_SHIP_START']
datetime_format = '%Y-%m-%d'
default_date = '2000-01-01'
# === end of default parameters


def to_access(df, 
              access_file=default_access_file, 
              customer_md_table=customer_md_table, 
              customer_md_fields=customer_md_fields, 
              product_md_table=product_md_table,
              product_md_fields=product_md_fields, 
              chain_field=chain_field,
              chain_to_db_replacement=chain_to_db_replacement,
              data_fields=data_fields, 
              datetime_fields=datetime_fields, 
              datetime_format=datetime_format, 
              default_date=default_date, 
              access_compact_threshold=1.5e9,
              verbose=False):
    '''
    Will be deprecated
    
    Exports a Dataframe to Access database for more user-friendly review.
    The operation is split into following steps:
    - check that passed DataFrame contains all columns which are necessary for the update
    - if new Customer Master Data is present in the DataFrame - append it to Access
    - if new Product Master Data is present in the DataFrame - append it to Access
    - for each Chain in the DataFrame:
        - remove ALL existing data from that chain's table in Access database
        - insert the new data for that chain from DataFrame to Access
    
    :PARAMETERS:
    df: pandas.DataFrame - DataFrame with data to export
    access_file: str - link to the MS Access database
    customer_md_table: str - name of table with Customer Master Data in MS Access
    customer_md_fields: list(str) - fields to upload from DataFrame to Customer Master Data table
    product_md_table: str - name of table with Product Master Data in MS Access
    product_md_fields: list(str) - fields to upload from DataFrame to Product Master Data table
    chain_field: str - name of DataFrame column with customer Chain name
    chain_to_db_replacement: dict{str:str} - mapping of Chain name in DataFrame to its name in Access, if they are not the same (e.g. no exclamation marks)
    data_fields: list(str) - list of columns to load from DataFrame to a Chain's table in Access
    datetime_fields: list(str) - list of columns which have DateTime format in Chain's table in Access - those cannot be null / 0 and are filled in by dummy value
    datetime_format: str - the DateTime format of DateTime columns for type conversion
    default_date: str - dummy DateTime value to fill the datetime_fields by, in order to avoid type conversion errors
    access_compact_threshold: int - threshold of Access file's volume (in bytes), after which the 'Compact & Repair' process will be run (Access has file limit of 2Gb, append will stop working if we exceed it)
    verbose: bool - flag whether to print debug messages of process steps to stdout
    
    :OUTPUT:
    None, the Access update operation is silent (unless verbose=True)
    
    :NOTES:
    1) Works only on Windows platform, as it utilizes MS Access drivers to interact with the database.
    2) As the loop by Chain uses only the ones present in the new DataFrame, data for other Chains will be kept intact in Access
    3) The Chain update loop generates temporary csv files for each one, using Access's insert * from that file afterwards.
    The reason for that is purely performance-based, as appending line-by-line from DataFrame to Access takes an unreasonable amount of time (~5h for all Chains)
    4) Most of the parameters (except the DataFrame) have default values, defined in the export.py module
    '''
    # check that we have a non-buggy version of pyodbc
    assert pyodbc.version[:6] != '4.0.25', 'pyodbc v4.0.25 has an issue with Access ODBC, please update your environment'
    # check that all necessary columns are in received data
    assert all(col in df.columns for col in customer_md_fields), 'Some of these columns are not in the received data'+str(customer_md_fields)
    assert all(col in df.columns for col in product_md_fields), 'Some of these columns are not in the received data'+str(product_md_fields)
    assert all(col in df.columns for col in data_fields), 'Some of these columns are not in the received data'+str(data_fields)
    
    # Open connection to Access DB
    connection_string = (
        r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};' 
        r'DBQ=%s;' % access_file
    )
    conn = pyodbc.connect(connection_string, autocommit=True)
    cursor = conn.cursor()
    base_path = os.path.dirname(os.path.realpath(access_file))
    temp_access_file = os.sep.join([base_path, 'compact_backup.accdb'])
    
    # Append missing Customer Master Data
    new_customer_data = df.groupby(customer_md_fields).count().reset_index()[customer_md_fields]
    append_missing_md(new_customer_data, cursor, customer_md_table, customer_md_fields, verbose=verbose)
    
    # Append missing Product Master Data
    new_product_data = df.groupby(product_md_fields).count().reset_index()[product_md_fields]
    append_missing_md(new_product_data, cursor, product_md_table, product_md_fields, verbose=verbose)
    conn.commit()
    conn.close()
    
    # For each Chain in df:
    # - delete all entries in DB
    # - append new entries from df to DB
    temp_path = os.sep.join([base_path, 'temp.csv'])
    data_fields_sql = ", ".join(["["+field+"]" for field in data_fields])
    for chain in df[chain_field].unique():
        chain_table = chain_to_db_replacement[chain] if (chain in chain_to_db_replacement.keys()) else chain
        entries_to_append = df[df[chain_field]==chain][data_fields]
        if verbose:
            print(f'Updating %s data - %d entries' % (chain, len(entries_to_append)))
        for field in datetime_fields:
            entries_to_append[field].replace('0', default_date, inplace=True)
            entries_to_append[field] = pd.to_datetime(entries_to_append[field], format=datetime_format)
        conn = pyodbc.connect(connection_string, autocommit=True)
        cursor = conn.cursor()
        cursor.execute(f'DELETE * FROM [{chain_table}]')
        # Export a temporary csv with this chain's data
        entries_to_append.to_csv(temp_path, index=False, sep=',', decimal='.', encoding='windows-1251')
        # Import this part to Access
        insert_query = f'INSERT INTO [%s] (%s) SELECT %s FROM [Text;HDR=Yes;FMT=Delimited(,);Database=%s].temp.csv' % (chain_table, data_fields_sql, data_fields_sql, base_path)
        cursor.execute(insert_query)
        # cleanup and close connection to DB
        os.remove(temp_path)
        conn.commit()
        conn.close()
        # Compact And Repair database - so that it doesn't overflow 2Gb limit (set threshold to 1.5Gb to avoid making calls every step)
        if (os.name =='nt') & (os.path.getsize(access_file) > access_compact_threshold) : # works in Windows only - we'd have to call win32com
            oApp = win32com.client.Dispatch('Access.Application')
            oApp.compactRepair(os.path.realpath(access_file), temp_access_file)
            os.remove(temp_access_file)
            oApp = None
    # Compact the database in the end of the loop
    if (os.name =='nt'): # works in Windows only - we'd have to call win32com
        oApp = win32com.client.Dispatch('Access.Application')
        oApp.compactRepair(os.path.realpath(access_file), temp_access_file)
        os.remove(temp_access_file)
        oApp = None
            

def append_missing_md(md, cursor, md_table, md_fields, verbose=False):
    '''
    Will be deprecated
    
    Appends Master Data, if it's not yet present in the Database:
    - gets existing data from MD table in Access
    - performs a left merge with the master data that we need to have to check which entries are missing
    - appends the missing entries to Access
    
    :PARAMETERS:
    md: pandas.DataFrame - DataFrame with master data that we need to have present in Access
    cursor: Cursor - cursor to the connection with Access Database (has to be open outside this function)
    md_table: str - name of Master Data table within Access to check and update
    md_fields: list(str) - column names to update in Master Data table within Access
    verbose: bool - flag whether to print debug messages of process steps to stdout
    
    :OUTPUT:
    None, the Access update operation is silent (unless verbose=True)
    
    :NOTES:
    1) Works only on Windows platform, as it utilizes MS Access drivers to interact with the database.
    2) As the function does not delete old entries in any way, previously filled data will stay in the database
    '''
    if verbose:
        print(f'Updating Master Data in %s...' % md_table)
        print(f'SELECT {", ".join(["["+field+"]" for field in md_fields])} FROM {md_table}')
    # Get already existing data from DB
    existing = cursor.execute(f'SELECT {", ".join(["["+field+"]" for field in md_fields])} FROM {md_table}').fetchall()
    existing = pd.DataFrame.from_records(existing, columns=md_fields)
    
    # Check which entries from md are not yet present in DB
    entries_to_append = md.merge(existing, how='left', indicator=True).query('_merge == "left_only"')[md_fields]
    if verbose:
        print(f'%d new entries to append to %s table' % (len(entries_to_append), md_table))
    
    # Append new entries
    if len(entries_to_append) > 0:
        cursor.executemany(
            f'INSERT INTO [{md_table}] ({", ".join(["["+field+"]" for field in md_fields])}) VALUES ({", ".join(["?" for i in range(entries_to_append.shape[1])])})', 
            entries_to_append.itertuples(index=False))
        if verbose:
            print(f'New entries appended to %s' % md_table)
            
            
def export_to_access_new_vs_old(new_file, old_file, access_file=default_access_file):
    '''
    Will be deprecated
    
    Exports the new Forecast to Access database, appending previous forecast version to it for comparison.
    
    :PARAMETERS:
    new_file: str - link to the new forecast file (csv/zip)
    old_file: str - ling to the previous forecast file (csv/zip)
    access_file: str - link to the MS Access database
    
    :OUTPUT:
    None, the Access update operation is silent
    
    :NOTES:
    - Works only on Windows platform, as the called to_access function utilizes MS Access drivers to interact with the database.
    - Default Access file location is defined in export.py module
    '''
    # Create the merged new/old forecast dataframe
    forecast_new = pd.read_csv(new_file)
    forecast_old = pd.read_csv(old_file)
    forecast_joined = pd.merge(forecast_new, forecast_old[['Date', 'key', 'Forecast']], how='left', on=['Date', 'key'], suffixes=('', '_PREV'))
    del forecast_new
    del forecast_old
    forecast_joined['Forecast_PREV'] = forecast_joined['Forecast_PREV'].fillna(0)
    # A rename to make things work
    forecast_joined.rename(columns={'PRD_UbrellaBrand': 'PRD_UmbrellaBrand'}, inplace=True)

    # Export the data to Access
    data_fields_upd = data_fields+['Forecast_PREV']
    to_access(forecast_joined, data_fields=data_fields_upd, access_file=access_file)