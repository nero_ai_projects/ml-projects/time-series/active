import pandas as pd
import hashlib
import os


def md5(name):
    hash_md5 = hashlib.md5()
    with open(name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def main():
    project_root = os.path.dirname(os.getcwd())

    files = []
    names = []
    dir_names = []
    hashes = []
    for parent, childs, _files in os.walk(project_root):
        for f in _files:

            _f = os.path.join(parent, f)

            files.append(_f)
            names.append(os.path.basename(_f))
            dir_names.append(os.path.dirname(_f).replace(project_root, ''))
            hashes.append(md5(_f))
            print(_f)

    data_dict = {
        'hash': hashes,
        'file': files,
        'name': names,
        'dir_name': dir_names,

    }

    df = pd.DataFrame(data_dict)
    df.to_csv('hashes.csv', index=False)


if __name__ == '__main__':
    main()
