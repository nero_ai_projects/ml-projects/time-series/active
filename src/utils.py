import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

from collections import defaultdict
import logging
import glob
import shutil


from pandas.core.tools.datetimes import to_datetime

def td(s, format='%Y%m%d'):
    """
    Converts string to pandas date_time format
    :param s: string to be converted
    :param format: format applied
    :return: timestamp object
    """
    return pd.to_datetime(s, format=format)


def add_cat_cust(df, key_col='key', chess='false'):
    """
    Adds product and customer hierarchy based on key
    :param df: pandas dataframe
    :param key_col: columns containing key
    :return: input dataframe with additional columns: ['PRD_ProdType', 'PRD_UbrellaBrand', 'PRODUCT_CODE', 'Sku/линейка', 'Techbillto', 'Chain']
    :notes: 1 - uses two masterdata filses. 2 - it is assumed that key has the following structure: xxxx_yyyyyyyyy, where xxxx is product code, yyyyyyyyy is techbillto code.
    """
    prod = pd.read_csv('../data/masterdata/Product_MD.csv', encoding='windows-1251', sep=';', dtype={'PRODUCT_CODE':'str'}) # /home/nata/Projects/Active
    prod = prod[['PRD_ProdType', 'PRD_UbrellaBrand', 'PRODUCT_CODE', 'Sku/линейка']]
    prod = prod.drop_duplicates(subset=['PRODUCT_CODE'])

    # убираем ProductType экспериментальных линеек
    prod = prod[~prod['PRD_ProdType'].apply(lambda x: 'dummy' in x.lower())]
    
    cust = pd.read_csv('../data/masterdata/Customer_MD.csv', encoding='windows-1251', sep=';', dtype={'Techbillto':'str'})
    cust = cust[['Techbillto', 'Chain']]
    
    # lenta change Chain for chess (task24)
    if chess.lower() == 'true':
        dict_lenta = get_lenta_chain_mapping()
        cust.loc[cust['Techbillto'].isin(dict_lenta.keys()), 'Chain'] = cust[cust['Techbillto'].isin(dict_lenta.keys())]['Techbillto'].map(dict_lenta)

    # TODO: optimize it
    df['Techbillto'] = df[key_col].str.split("_", n=1, expand=True)[1]
    df['PRODUCT_CODE'] = df[key_col].str.split("_", n=1, expand=True)[0]
    df = pd.merge(df, cust, how='left', on='Techbillto')
    df = pd.merge(df, prod, how='left', on='PRODUCT_CODE')

    return df


def metrics_weeks(df, target_column, predict_columns, date_column, key_column, start_date, end_date, keys=None,
                        category=False, customer=False):
    """The function calculates the metrics on weeks on the forecast

    Args:
        df ([DataFrame]): DataFrame with forecasting
        target_column (string): forecasting column
        predict_columns (list): columns with forecasting
        date_column (string): column with date
        key_column (string): column with key
        start_date (string): start date of metric calculation
        end_date (string): end date of metric calculation
        keys (list, optional): list of keys for calculating the metric. Defaults to None.
        category (bool, optional): calculate metrics on category. Defaults to False.
        customer (bool, optional): calculate metrics on customer. Defaults to False.

    Returns:
        [type]: [description]
    """
    if keys is None:
        keys = df[key_column].unique()
        data = df.copy()
    else:
        data = df[df[key_column].isin(keys)].copy()

    agg_columns = [key_column]
    data['gr'] = 'gr'
    hierarchy_columns = ['gr']
    if customer:
        hierarchy_columns.append('Chain')
    if category:
        hierarchy_columns.append('BP_ProdGroup')

    data[date_column] = pd.to_datetime(data[date_column])

    FA_columns = [f'FA_{col}' for col in predict_columns]
    BIAS_columns = [f'BIAS_{col}' for col in predict_columns]
    AE_columns = [f'AE_{col}' for col in predict_columns]

    FA_list = []
    for day in pd.date_range(start=start_date, end=end_date, freq='7D'):
        week = data[(data[date_column] >= day) & (data[date_column] <= day + pd.to_timedelta('6D'))]
        week = week.groupby(agg_columns + hierarchy_columns, as_index=False).sum()

        for col in predict_columns:
            week[f'AE_{col}'] = pd.Series(np.abs(week[f'{col}'] - week[target_column]))

        week = week.groupby(hierarchy_columns, as_index=False).sum()

        for col in predict_columns:
            week[f'FA_{col}'] = pd.Series(1 - week[f'AE_{col}'] / week[target_column])

            week.loc[(week[f'{col}'] == 0.) & (week[target_column] == 0.), f'FA_{col}'] = 1.
            week.loc[(week[f'{col}'] != 0.) & (week[target_column] <= 0.), f'FA_{col}'] = 0.

            week[f'BIAS_{col}'] = pd.Series((week[target_column] - week[f'{col}'])/ week[f'{col}']) 

        week = week[hierarchy_columns + FA_columns + BIAS_columns]# + predict_columns + [target_column]
        week['start_date'] = day
        week['end_date'] = day + pd.to_timedelta('6D')
        FA_list.append(week)

    FA_weeks = pd.concat(FA_list)
    
    FA_weeks = FA_weeks.groupby(hierarchy_columns, as_index=False).mean()

    return FA_weeks


def metrics_promo(df, category=False, product=False, customer=False, lvl = 'chain'):
    """
    Calculates forecast accuracy, forecast bias and absolute error by category, by customer or in total either on chain or on techbillto level.
    :param df: pandas dataframe with actuals and forecast, should contain: ['Partition', 'SHIP_START', 
    'FT_SHIP_START', 'Date', 'Chain', 'Techbillto', 'PRODUCT_CODE', 'PRD_ProdType', 'Ordered', 'Forecast']
    :param category: boolean, True if category detail is requested
    :param customer: boolean, True if customer detail is requested
    :param lvl: ['chain', 'techbillto'] - level of metrics calculation
    :return: pandas dataframe, contains actuals, forecast, AE, FA, BIAS in the specified detail 
    """
    df = df[(df['Partition']=='fc') & ((df['SHIP_START'].astype(str)!='0') | (df['FT_SHIP_START'].astype(str)!='0'))].copy()
    df = add_cat_cust(df)
    
    fc_period_start = min(df['Date'])
    
    gr_list = ['PRODUCT_CODE', 'PRD_ProdType']
    if lvl=='chain':
        gr_list.append('Chain')
    else:
        gr_list.extend(['Techbillto', 'Chain'])
    act_gr_list = gr_list + ['SHIP_START']
    fc_gr_list = gr_list + ['FT_SHIP_START']
    
    df_act = df[df['SHIP_START'].astype(str)!='0'].groupby(act_gr_list, as_index=False).agg({'Ordered':'sum'})
    df_fc = df[df['FT_SHIP_START'].astype(str)!='0'].groupby(fc_gr_list, as_index=False).agg({'Forecast':'sum'})
    df = pd.merge(df_fc, df_act, how='left', left_on=fc_gr_list, right_on=act_gr_list)
    df = df.dropna()

    df['AE'] = abs(df['Ordered'] - df['Forecast'])
    df = df[df['FT_SHIP_START']>=fc_period_start]
    df['gr'] = 'gr'
    
    gr_list = ['gr']
    if product:
        gr_list.append('PRODUCT_CODE')
    if category:
        gr_list.append('PRD_ProdType')
    if customer:
        gr_list.append('Chain')
        
    
    df = df.groupby(gr_list, as_index=False).sum()
    df = df.drop(columns=['gr'])
    
    df['Acc'] = np.where((df['Ordered'] != 0), (1 - df['AE']/df['Ordered']), 0)  #вынести в calc_acc со всеми обертками
    df['BIAS'] = (df['Ordered'] - df['Forecast'])/df['Forecast']
    return df

def metrics_hist(df):
    """plot a histogram with the distribution of precision by keys

    Args:
        df (DataFrame): DataFrame with forecasting
    """
    df = df[(df['Partition']=='fc')& (df['FT_SHIP_START'].astype(str)!='0')].copy()

    df = df.groupby(['key', 'FT_SHIP_START'], as_index=False).agg({'Ordered':'sum', 'Forecast':'sum'})
    df['AE'] = abs(df['Ordered'] - df['Forecast'])
    df = df.groupby(['key'], as_index=False).agg({'Ordered':'sum', 'Forecast':'sum', 'AE':'sum'})
    df['Acc, %'] = np.where((df['Ordered'] != 0), (1 - df['AE']/df['Ordered']), 0) * 100  #вынести в calc_acc со всеми обертками
    df['Acc, %'] = np.minimum(np.maximum(df['Acc, %'], -100), 100)
    df['BIAS, %'] = (df['Ordered'] - df['Forecast'])/df['Forecast']*100
    print('NB: Accuracy is cliped (-100, 100)')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 5))
    fig.suptitle('Metrics histogram')
    ax1.hist(df['Acc, %'])
    ax1.set_title('FA')
    ax2.hist(df['BIAS, %'])
    ax2.set_title('FB')
#     return df
    
    
def plot_func(df, target_column, predict_columns, date_column, key_column, start_date, end_date, keys=None,
                        category=False, customer=False, figsize = (10, 6)): 
    """
    Will be deprecated
    """

    if keys is not None:
        data = df[df[key_column].isin(keys)].copy()
    else:
        data = df.copy()

    del df 

    data = add_cat_cust(data)

    data['gr'] = 'gr'
    hierarchy_columns = ['gr']
    if customer:
        hierarchy_columns.append('Chain')
    if category:
        hierarchy_columns.append('BP_ProdGroup')
    
    FA_weeks = metrics_weeks(data, target_column, predict_columns, date_column, key_column, start_date, end_date, keys=keys,
                                      category=category, customer=customer) ## metrics_weeks, set index agg columns without gr

    data = data.groupby(hierarchy_columns + [date_column], as_index=False).sum()

    FA_weeks = FA_weeks.groupby(hierarchy_columns).sum()

    for col in predict_columns: #if-then-else заменить на: 1 - ключ = ag columns concat, for key in unique keys. метрики брать по ключу из FA_metrics
        for index, df in data.groupby(hierarchy_columns):
            # print(index)
            # print(FA_weeks.index)
            FA = FA_weeks.loc[index, f'FA_{col}']
            BIAS = FA_weeks.loc[index, f'BIAS_{col}']
            df.plot(x=date_column,
                    y=[target_column, col],
                    figsize=figsize,
                    title=f'{index} FA == {FA}   BIAS == {BIAS}')

            
            
def get_n_keys(sales, n):
    """
    Will be deprecated
    """
    sales = sales.rename(columns={'KEY':'key'})  ##ужасно и отвратительно, придумать, что с этим сделать
    sales = add_cat_cust(sales)
    n_cust = sales['Chain'].nunique()
    n_cat = sales['BP_ProdGroup'].nunique()
    m = max(1, (n // (n_cust * n_cat)))  #сколько представителей каждой группы
    keys = []
    for _, mdf in sales.groupby(['Chain', 'BP_ProdGroup']):  #попробовать пересадить с групбая #as an idea: groupby.transform + cumcount
        mdf = mdf[['key', 'ORDER_QUAN']].groupby(['key'], as_index=False).sum()
        part_keys = mdf.nlargest(m, 'ORDER_QUAN')['key'].tolist()
        keys += part_keys
    return keys

def get_str_idx(series):  ##возвращает индексы элементов, которые имеют тип str
    """
    Will be deprecated
    """
    b = [isinstance(x, str) for x in series]
    df = pd.DataFrame(columns = ['tmp'])
    df['tmp'] = b
    idx = df[df['tmp']==True].index
    return idx

def get_lenta_mapping():
    """
    Returns the mapping of Lenta customer codes which should be replaced due to change
    :return: dict of codes: keys are old, values - new ones.
    """
    keys = ['850165720',
            '850176143',
            '850176279',
            '850176305',
            '850246914',
            '850247359',
            '850247443',
            '850247444',
            '850247484',
            '850247525',
            '850247526',
            '850247527',
            '850247528',
            '850247529',
            '850247530',
            '850247531',
            '850247532',
            '850247533',
            '850247534',
            '850247535',
            '850247536',
            '850247537',
            '850247538',
            '850247540',
            '850247541',
            '850247542',
            '850247543',
            '850247544',
            '850247545',
            '850247546',
            '850247547',
            '850247548',
            '850247549',
            '850247550',
            '850247551',
            '850247552',
            '850247553',
            '850247555',
            '850247556',
            '850247557',
            '850247558',
            '850247559',
            '850247560',
            '850247561',
            '850247562',
            '850247563',
            '850247564',
            '850247565',
            '850247566',
            '850247567',
            '850247620',
            '850247621',
            '850247622',
            '850247623',
            '850247624',
            '850247630',
            '850247631',
            '850247632',
            '850247633',
            '850247634',
            '850247635',
            '850247636',
            '850247637',
            '850264240',
            '850247638',
            '850252074',
            '850265677',
            '850265678',
            '850265679']
    values = ['850265677',
            '850265678',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265679',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265678',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265679',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265678',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265679',
            '850265678',
            '850265678',
            '850265677',
            '850265678',
            '850265677',
            '850265679',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265678',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265677',
            '850265679',
            '850265678',
            '850265677',
            '850265678',
            '850265679']
    return dict(zip(keys, values))

def get_lenta_chain_mapping():
    """Function separate LENTA to LENTA_SM and LENTA_HM for chess"""

    keys = ['850265677',
            '850265678',
            '850289765',
            '850265679']
    values = ['LENTA_HM',
              'LENTA_HM',
              'LENTA_SM',
              'LENTA_SM']
              
    return dict(zip(keys, values))

def fa(tuples):
    """
    Calculates forecast accuracy
    :param tuples: list of tuples with actual and forecast values corresponding in each of them. 
    :return: float number, forecast accuracy without inf values in case of deviding by zero
    """
    actuals = np.array([x[0] for x in tuples])
    forecast = np.array([x[1] for x in tuples])
    ae = np.sum(np.abs(actuals - forecast))
    if ae == 0:
        return 1
    
    act_sum  = np.sum(actuals)
    if act_sum == 0:
        return 0
    
    return 1 - ae/act_sum

def neg_mae(tuples):
    """
    Calculates mean absolute forecast error multiplied by -1.
    :param tuples: list of tuples with actual and forecast values corresponding in each of them. 
    :return: float number, mean absolute forecast error multiplied by -1
    """
    actuals = np.array([x[0] for x in tuples])
    forecast = np.array([x[1] for x in tuples])
    mae =  - np.mean(np.sum(np.abs(actuals - forecast)))
    
    return mae

def fa_promo_slots(tuples):
    """
    Calculates forecast acuracy by promo slots
    :param tuples: list of tuples with actual, forecast and slot shipment start date values corresponding in each of them.
    :return: float number, forecast accuracy calculated by promo slots
    :notes: assumes a certain order of elements in the tuple. Non-promo dates have SHIP_START == '0'.
    """
    gt, pred, gt_sum, pred_sum = defaultdict(list), defaultdict(list), defaultdict(float), defaultdict(float)
    for x in tuples:
        if str(x[2])!='0':  #завязано на порядок следования столбцов. TODO: написать нормально
            gt[x[2]].append(x[0])
        if str(x[3])!='0':
            pred[x[3]].append(x[1])

    for k, v in gt.items():
        gt_sum[k] = sum(v) 
    for k, v in pred.items():
        pred_sum[k] = sum(v)

    res = []
    for k, _ in gt_sum.items():
        res.append((gt_sum[k], pred_sum[k]))

    return fa(res)


def get_cases_keys():
    """Returns cases for testing in the first quarter (deprecated)

    Returns:
        [tuple]: tuple DataFrames with cases and keys
    """
    cases = pd.read_csv('../data/temp/cases.csv')
    skus = cases['SKU'].unique()
    chain = cases['Chain'].unique()

    prod = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251', dtype={'SKU':'str', 
                                                                                                    'PRODUCT_CODE':'str'})
    prod = prod[['Sku/линейка', 'PRODUCT_CODE']]
    prod = prod.drop_duplicates()

    prod = prod[prod['Sku/линейка'].isin(skus)]

    cust = pd.read_csv('../data/masterdata/Customer_MD.csv', 
                        sep=';', 
                        encoding='windows-1251', dtype={'Techbillto':'str', 'customer code':'str'}, 
                        usecols=['Techbillto', 'customer code', 'Chain', 'customer name'])
    cust = cust.rename(columns={'Techbillto':'TECH_BILL_TO', 'customer code':'CUSTOMER_CODE'})
    cust = cust.dropna()
    cust = cust[['Chain', 'TECH_BILL_TO']]
    cust = cust.drop_duplicates()

    cust = cust[cust['Chain'].isin(chain)]

    prod['merge'] = 'merge'
    cust['merge'] = 'merge'
    keys = prod.merge(cust)
    keys = keys.merge(cases, how='inner', left_on=['Sku/линейка', 'Chain'],
                      right_on=['SKU', 'Chain'])[['PRODUCT_CODE', 'TECH_BILL_TO', 'Sku/линейка', 'Chain']]

    # keys = keys.drop(columns=['merge'])
    keys['key'] = keys['PRODUCT_CODE'] + '_' + keys['TECH_BILL_TO']
    keys = keys.drop_duplicates()

    return cases, keys


def metric_on_cases(df, Q='20 Q1'):
    """calculates metrics for Q1 cases (deprecated)

    Args:
        df (DataFrame): DataFrame with forecasting 
        Q (str, optional): quarter for cases. Defaults to '20 Q1'.

    Returns:
        [DataFrame]: DataFrame filtered by cases
    """
    df = df[df['Partition'] == 'fc'].copy()

    cases, keys = get_cases_keys()
    keys['key'] = keys['PRODUCT_CODE'] + '_' + keys['Chain']
    keys = keys.drop(columns=['TECH_BILL_TO'])
    keys = keys.drop_duplicates()

    df = df[df['key'].isin(keys['key'].unique())]

    df = df.merge(keys[['key', 'Sku/линейка', 'Chain']], how='inner')
    df['FT_SHIP_START'] = df['FT_SHIP_START'].astype(str)
    df = df[df['FT_SHIP_START'] != '0']
    df = df.groupby(['Sku/линейка', 'Chain', 'FT_SHIP_START'], as_index=False).agg({'Ordered': 'sum', 'Forecast': 'sum'})

    if Q == '20 Q1':
        df['FT_SHIP_START'] = pd.to_datetime(df['FT_SHIP_START'])
        cases['Shipment start date'] = pd.to_datetime(cases['Shipment start date'])

        df = df.merge(cases[['SKU', 'Chain', 'Shipment start date', 'Sum of Sum of fact ']], how='inner',
                      left_on=['Sku/линейка', 'Chain', 'FT_SHIP_START'], right_on=['SKU', 'Chain', 'Shipment start date'])
        df = df[['Sku/линейка', 'Chain', 'FT_SHIP_START', 'Sum of Sum of fact ',
                 'Forecast']].rename(columns={'Sum of Sum of fact ': 'Ordered'})
    else:
        df = df[['Sku/линейка', 'Chain', 'FT_SHIP_START', 'Ordered', 'Forecast']]

    df['AE'] = np.abs(df['Ordered'] - df['Forecast'])

    df['FA'] = 1 - df['AE'] / df['Ordered']
    print(df['AE'].sum())
    print(df['Ordered'].sum())
    print(1 - df['AE'].sum() / df['Ordered'].sum())
    return df

def get_kpis_in_all_slices(model_results):
    '''
    Calculates KPIs for selected files (using metrics_promo()) in different slices and returns them as DataFrame:
    - TOTAL
    - by Chain (without split by ProdType)
    - by ProdType (without split by Chain)
    - by Chain and ProdType
    
    :PARAMETERS:
    model_results: list(str) - list of file links to saved DataFrames with Forecast (with 'Ordered' and 'Forecast' columns)
    
    :OUTPUT:
    all_kpis: pandas.DataFrame - dataframe with KPIs with columns ['File', 'PRD_ProdType', 'Chain', 'Forecast', 'Ordered', 'AE', 'Acc', 'BIAS']
    '''
    all_kpis = []
    for file in model_results:
        print(f'Loading %s' % (file))
        result = pd.read_csv(file)
        print('Calculating KPIs...')
        total = metrics_promo(result, category=False, customer=False) # TOTAL
        total['PRD_ProdType'] = 'TOTAL'
        total['Chain'] = 'TOTAL'
        prodtype = metrics_promo(result, category=True, customer=False) # by product type
        prodtype['Chain'] = 'TOTAL'
        by_chain = metrics_promo(result, category=False, customer=True) # Chain
        by_chain['PRD_ProdType'] = 'TOTAL'
        by_prod_and_chain = metrics_promo(result, category=True, customer=True) # by prod type and chain
        # rearrange columns to be universal
        cols_sorted = ['PRD_ProdType', 'Chain', 'Forecast', 'Ordered', 'AE', 'Acc', 'BIAS']
        total = total[cols_sorted]
        prodtype = prodtype[cols_sorted]
        by_chain = by_chain[cols_sorted]
        by_prod_and_chain = by_prod_and_chain[cols_sorted]
        file_kpis = pd.concat([total, prodtype, by_chain, by_prod_and_chain], ignore_index=True)
        file_kpis['File'] = file
        all_kpis.append(file_kpis)

    print('Concatenating all results...')
    all_kpis = pd.concat(all_kpis, ignore_index=True)
    return all_kpis


def reduce_mem_usage(props, verbose=False):
    """
    The function reduces the memory, that is used by input data frame by changing the types of columns.

    :param props: pd.DataFrame
    :verbose: if True, the function prints all the previous and final types and the results of memory usage reduction

    :return: (pd.DataFrame, list); tuple of resulting data frame and list with columns having any NaN values
    """
    start_mem_usg = round(props.memory_usage().sum() / 1024 ** 2)
    if verbose:
        print("Memory usage of properties dataframe is :", start_mem_usg, " MB")
    nan_list = []  # Keeps track of columns that have missing values filled in.
    for col in props.columns:
        if props[col].dtype == object or props[col].dtype == np.dtype('<M8[ns]'):
            # Exclude strings and time
            continue

        # Make float datatypes 32 bit
        if props[col].dtype == np.dtype('float64'):
            props[col] = props[col].astype(np.float32)
            continue

        if verbose:
            # Print current column type
            print("******************************")
            print("Column: ", col)
            print("dtype before: ", props[col].dtype)

        # make variables for Int, max and min
        is_int = False
        mx = props[col].max()
        mn = props[col].min()

        # Integer does not support NA, therefore, NA needs to be filled
        if not np.isfinite(props[col]).all():
            nan_list.append(col)
            props[col].fillna(mn - 1, inplace=True)

        # test if column can be converted to an integer
        as_int = props[col].fillna(0).astype(np.int64)
        result = (props[col] - as_int)
        result = result.sum()
        if -0.01 < result < 0.01:
            is_int = True

        # Make Integer/unsigned Integer datatypes
        if is_int:
            if mn >= 0:
                if mx < 255:
                    props[col] = props[col].astype(np.uint8)
                elif mx < 65535:
                    props[col] = props[col].astype(np.uint16)
                elif mx < 4294967295:
                    props[col] = props[col].astype(np.uint32)
                else:
                    props[col] = props[col].astype(np.uint64)
            else:
                if mn > np.iinfo(np.int8).min and mx < np.iinfo(np.int8).max:
                    props[col] = props[col].astype(np.int8)
                elif mn > np.iinfo(np.int16).min and mx < np.iinfo(np.int16).max:
                    props[col] = props[col].astype(np.int16)
                elif mn > np.iinfo(np.int32).min and mx < np.iinfo(np.int32).max:
                    props[col] = props[col].astype(np.int32)
                elif mn > np.iinfo(np.int64).min and mx < np.iinfo(np.int64).max:
                    props[col] = props[col].astype(np.int64)

        # Print new column type
        if verbose:
            print("dtype after: ", props[col].dtype)
            print("******************************")

    if verbose:
        # Print final result
        print("___MEMORY USAGE AFTER COMPLETION:___")
        mem_usg = round(props.memory_usage().sum() / 1024 ** 2)
        print("Memory usage is: ", mem_usg, " MB")
        changes = round(100 * mem_usg / start_mem_usg, 2)
        print(f"This is {changes}% of the initial size")
    return props, nan_list


def gc_collect(func):
    """
    Wrapper that call gc.collect after function calls
    """

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        import gc
        _ = gc.collect()

        return result

    return wrapper


def save_passed_dataframe_if_debug(func):
    """
    The wrapper that save outcome dataframe if danone.DEBUG=True

    :param func: function
    :return: wrapped function
    """

    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        from danone import DEBUG
        if isinstance(result, pd.DataFrame) and DEBUG:
            folder = r'../data/temp'
            func_name = func.__name__
            result.to_csv(f'{folder}/{func_name}.csv', index=False)

        return result

    return wrapper


def log_n_unique_keys(func):
    """
    Wrapper that log amount of keys in columns 'key' ar 'KEY' if first argument is pd.Dataframe.
    Calculus executes after function call.

    """

    def wrapper(*args, **kwargs):

        result = func(*args, **kwargs)

        if isinstance(result, pd.DataFrame):

            key_cols = ['key', 'KEY']
            for col in key_cols:
                if col in result.columns:
                    amount = result[col].nunique()
                    logging.info(f'after {func.__name__} we have {amount} keys in "{col}" column')

        return result

    return wrapper


def get_float_col_list(df):
    """
    Return columns that contain float data
    :param df: pandas DataFrame
    :return: columns that contain float data
    """
    types = df.dtypes
    cols = types[types == 'float64'].index

    return cols


def round_float_cols(df, decimals=4):
    """
    Round every float column in the dataframe
    :param df: pandas dataframe
    :param decimals: number of decimal places
    :return: None
    """
    cols = get_float_col_list(df)
    for col in cols:
        df[col] = df[col].round(decimals=decimals)


def get_adaptive_keys(data: pd.DataFrame):
    """
    Return set of keys that need to use adaptive fold length for tournament
    (use_adaptive_fold_length parameter in tournament.evaluate_models function)
    Note: to define adaptive keys function uses master data file 'Filter for CV unlimited.xlsx'
    :param data: pandas DataFrame that contain 'key' column
    :return: set of keys
    """
    keys = pd.DataFrame(data=data.key.unique(), columns=['key'])
    adaptive_cv_groups = pd.read_excel(r'../data/masterdata/Filter for CV unlimited.xlsx')
    adaptive_keys = adaptive_cv_groups.merge(add_cat_cust(keys), on=['Chain', 'PRD_ProdType'])
    adaptive_keys = set(adaptive_keys.key)

    return adaptive_keys


def check_len_cust_plans(data, keys):
    """check len history customers plans for keys

    Args:
        data (pd.Dataframe): dataset for tournament
        keys (set): set of keys for using customers plans

    Returns:
        set: keys with order plans from the client for more than 14 days of the forecast
    """
    data = data[data['key'].isin(list(keys))]
    data['Date'] = pd.to_datetime(data['Date'], format='%Y-%m-%d')

    fc_start = data.loc[data['Partition'] == 'fc', 'Date'].min()

    data_with_cust_plans = data[data['cust_plan'] > 0]

    del data

    keys_data_with_cast_plans = data_with_cust_plans.groupby(['key'], as_index=False).agg({'Date': 'max'})

    del data_with_cust_plans

    keys_data_with_cast_plans = keys_data_with_cast_plans[keys_data_with_cast_plans['Date'] \
                                                          >= (fc_start + pd.DateOffset(days=14))]

    keys = set(keys_data_with_cast_plans['key'].unique())

    return keys


def get_use_cust_plan_as_feature(data: pd.DataFrame):
    """
    Return set of keys that need to use 'cust_plan' feature in tournament
    (pass the dataframe with this column in tournament.evaluate_models function)

    Note: to define those keys uses master data file 'Filter for cust_plan as feature.xlsx'

    :param data: pandas DataFrame that contain 'key' column
    :return: set of keys
    """
    keys = pd.DataFrame(data=data.key.unique(), columns=['key'])
    cust_plan_feature_groups = pd.read_excel(r'../data/masterdata/Filter for cust_plan as feature.xlsx')
    cust_plan_feature_keys = cust_plan_feature_groups.merge(add_cat_cust(keys), on=['Chain', 'PRD_ProdType'])
    
    # Filter OUT the Magnit's Hyper Techbillto
    magnit_exclude_tbt = pd.read_excel(r'../data/masterdata/Cust_plan include-exclude list.xlsx', sheet_name='exclude')
    magnit_exclude_tbt['Techbillto'] = magnit_exclude_tbt['Techbillto'].astype(str)
    cust_plan_feature_keys = pd.merge(cust_plan_feature_keys, magnit_exclude_tbt, how='outer', on='Techbillto', indicator=True)
    cust_plan_feature_keys = cust_plan_feature_keys[cust_plan_feature_keys['_merge']=='left_only'].drop(columns=['_merge']).reset_index(drop=True)
    # For Magnit - select only Hammers
    magnit_hammers = pd.read_excel(r'../data/masterdata/Cust_plan include-exclude list.xlsx', sheet_name='include-magnit')
    non_magnit_keys = cust_plan_feature_keys[cust_plan_feature_keys['Chain']!='MAGNIT']
    magnit_keys = cust_plan_feature_keys[cust_plan_feature_keys['Chain']=='MAGNIT']
    magnit_keys = pd.merge(magnit_keys, magnit_hammers, how='left', on='Sku/линейка', indicator=True)
    magnit_keys = magnit_keys[magnit_keys['_merge']=='both'].drop(columns=['_merge']).reset_index(drop=True)
    cust_plan_feature_keys = non_magnit_keys.append(magnit_keys).reset_index(drop=True)
    
    # Return set of keys to use
    cust_plan_feature_keys = set(cust_plan_feature_keys.key)

    cust_plan_feature_keys = check_len_cust_plans(data, cust_plan_feature_keys)
    
    return cust_plan_feature_keys


def get_use_cust_plan_as_forecast(data: pd.DataFrame):
    '''
    Return set of keys for which to replace Forecast by 'cust_plan' if that feature is non-zero
    (used in postprocessing after tournament run)
    
    Note: to define those keys uses master data file 'Filter for cust_plan as forecast.xlsx'
    
    :param data: pandas DataFrame that contain 'key' column
    :return: set of keys
    '''
    keys = pd.DataFrame(data=data.key.unique(), columns=['key'])
    cust_plan_forecast_groups = pd.read_excel(r'../data/masterdata/Filter for cust_plan as forecast.xlsx')
    cust_plan_forecast_keys = cust_plan_forecast_groups.merge(add_cat_cust(keys), on=['Chain', 'PRD_ProdType'])
    
    # Filter OUT the Magnit's Hyper Techbillto
    magnit_exclude_tbt = pd.read_excel(r'../data/masterdata/Cust_plan include-exclude list.xlsx', sheet_name='exclude')
    magnit_exclude_tbt['Techbillto'] = magnit_exclude_tbt['Techbillto'].astype(str)
    cust_plan_forecast_keys = pd.merge(cust_plan_forecast_keys, magnit_exclude_tbt, how='outer', on='Techbillto', indicator=True)
    cust_plan_forecast_keys = cust_plan_forecast_keys[cust_plan_forecast_keys['_merge']=='left_only'].drop(columns=['_merge']).reset_index(drop=True)
    # For Magnit - select only Hammers
    magnit_hammers = pd.read_excel(r'../data/masterdata/Cust_plan include-exclude list.xlsx', sheet_name='include-magnit')
    non_magnit_keys = cust_plan_forecast_keys[cust_plan_forecast_keys['Chain']!='MAGNIT']
    magnit_keys = cust_plan_forecast_keys[cust_plan_forecast_keys['Chain']=='MAGNIT']
    magnit_keys = pd.merge(magnit_keys, magnit_hammers, how='left', on='Sku/линейка', indicator=True)
    magnit_keys = magnit_keys[magnit_keys['_merge']=='both'].drop(columns=['_merge']).reset_index(drop=True)
    cust_plan_forecast_keys = non_magnit_keys.append(magnit_keys).reset_index(drop=True)
    
    # Return set of keys to use
    cust_plan_forecast_keys = set(cust_plan_forecast_keys.key)
    
    return cust_plan_forecast_keys


def expand_df_period(df, start='start', end='end'):
    """
    The function creates a flat dataframe from the passed dataframe.
    Each row in the dataframe will be expanded to the range of dates(by day).
    The output dataframe doesn't contain columns that passed in start and end arguments.

    Limitation: Space in the column name does not allow. ValueError will be raised in this case.

    :param df: pd.DataFrame
    :param start: column that contains start of the period
    :param end:  column that contains end of the period
    :return: pd.DataFrame
    """
    # TODO: add_opportunity to keep start, end columns

    tmp = defaultdict(list)
    columns = [col for col in df.columns if col not in [start, end]]

    cols_with_space = [col for col in columns if ' ' in col]
    if cols_with_space:
        raise ValueError(f'Using columns that contains space in the name impossible. Columns to rename: {cols_with_space}')

    for row in df.itertuples(index=False):
        dates = pd.date_range(start=getattr(row, start), end=getattr(row, end))
        tmp['date'].extend(dates)
        length = len(dates)
        for col in columns:
            tmp[col].extend([getattr(row, col)] * length)

    return pd.DataFrame.from_dict(tmp)


def get_chess_header(df, limit_rows=10, limit_cols=10):
    """
    The function finds the index of the row where the placed header of a chess table.
    The function scans the first 10 columns to find the row where 'Product name' placed.

    :param df: pd.DataFrame
    :param limit_rows: int that limit how many rows will be scanned. Default value is 10
    :param limit_cols: int that limit how many columns will be scanned. Default value is 10
    :return: index of header or -1
    """

    for idx, vals in enumerate(df.itertuples()):
        for idx_col, label in enumerate(vals):
            if idx_col > limit_cols: break
            if label == 'Product name':
                return idx + 1
        if idx >= limit_rows:
            return -1


def combine_promo_slots_in_chess(df):
    """
    The function define sequences of promo windows that have overlays and and combine it in one window.
    The function sort values in the dataframe by Chain, Product_name, period.

    :param df: pandas.DataFrame
    :return: secs_list -  list of sequences.

    """

    df.sort_values(['Chain', 'Product name', 'period'], ascending=False, inplace=True, ignore_index=True)
    secs_list = []
    for key, sample in df.groupby(['Chain', 'Product name']):

        sample = sample.copy()
        sample['next_start'] = sample.start.shift(1)
        sample['delta'] = pd.to_datetime(sample.end) - pd.to_datetime(sample.next_start)
        sample['add_prev_row'] = np.where(sample.delta >= pd.Timedelta(1), True, False)

        sec = False
        start_index = None
        prev_row_index = None
        for row in sample.itertuples():
            if row.add_prev_row:
                if not sec:
                    start_index = row.Index - 1
                    sec = True
            else:
                if sec:
                    df.loc[start_index, 'start'] = df.loc[prev_row_index, 'start']
                    secs_list.append((start_index, prev_row_index))
                    sec = False

            prev_row_index = row.Index

        if start_index is not None and sec:
            df.loc[start_index, 'start'] = row.start
            secs_list.append((start_index, row.Index))

    return secs_list


def add_ship_start_in_chess(df, secs_list=None):
    """
    The function add 'SHIP_START' column in the dataframe.
    The default value is value from 'start' column. For index passed in the

    :param df: pd.DataFrame
    :param secs_list: list of sequences. Sequence is tuple of start_index and end_index.
    :return: None
    """

    secs_list = [] if secs_list is None else secs_list

    df['SHIP_START'] = df.start.str.slice(0, 10)
    for start_ind, end_ind in secs_list:
        df.loc[start_ind: end_ind, 'SHIP_START'] = df.loc[start_ind, 'SHIP_START']


def align_chess_format_with_aft(df, add_wd=False, add_esp=False):
    """
    The function create dataframe that contains same columns as AFT dataframe (ref. features.add_fintool function)

    :param df: chess pd.DataFrame
    :param add_wd: Bolean, if True 'WD' column will be added
    :return: pd.DataFrame with columns PGI_DATE, FT_Promo_day_num, FT_Promo_day_num_backward, Total Discount, KEY, SHIP_START
    """

    aft_columns = ['PGI_DATE', 'FT_Promo_day_num', 'FT_Promo_day_num_backward', 'Total Discount', 'KEY', 'SHIP_START']
    if add_wd:
        aft_columns.append('WD')
    if add_esp:
        aft_columns.append('ESP')

    df['one'] = 1
    df['FT_Promo_day_num'] = df.groupby(['Chain', 'product_name', 'SHIP_START'])['one'].transform('cumsum')
    df['FT_Promo_day_num_backward'] = df.groupby(['Chain', 'product_name', 'SHIP_START'])['FT_Promo_day_num'].transform(
        'max')
    df['FT_Promo_day_num_backward'] = df['FT_Promo_day_num_backward'] - df['FT_Promo_day_num'] + 1
    del df['one']

    cols = ['PRODUCT_CODE', 'Sku/линейка']
    product_hier = pd.read_csv('../data/masterdata/Product_MD.csv', encoding='windows-1251', sep=';',
                               dtype={'PRODUCT_CODE': 'str'}, usecols=cols)  # /home/nata/Projects/Active
    product_hier.drop_duplicates(inplace=True)
    product_hier.rename(columns={'Sku/линейка': 'product_name'}, inplace=True)

    cols = ['Techbillto', 'Chain']
    customer_hier = pd.read_csv('../data/masterdata/Customer_MD.csv', encoding='windows-1251', sep=';',
                                dtype={'Techbillto': 'str'}, usecols=cols)
    customer_hier.dropna(inplace=True)

    # lenta change Chain for chess (task24)
    dict_lenta = get_lenta_chain_mapping()
    customer_hier.loc[customer_hier['Techbillto'].isin(dict_lenta.keys()), 'Chain'] = customer_hier[customer_hier['Techbillto'].isin(dict_lenta.keys())]['Techbillto'].map(dict_lenta)

    df = pd.merge(df, customer_hier, how='left', on='Chain')

    # change lenta to one chain
    df.loc[df['Chain'].isin(list(dict_lenta.values())), 'Chain'] = 'LENTA'

    df = pd.merge(df, product_hier, how='left', on='product_name')

    name_mapping = {
        'date': 'PGI_DATE',
        'discount': 'Total Discount',
        'wd': 'WD',
        'esp': 'ESP',
    }
    df.rename(columns=name_mapping, inplace=True)

    if 'Metro_traiders' in df.columns:
        aft_columns = aft_columns + ['Metro_traiders', 'traiders_discount', 'cross_slots']

    df['KEY'] = df['PRODUCT_CODE'] + '_' + df['Techbillto']

    return df[aft_columns]


def add_wd_from_string_col(df, col='Mechanics+'):
    """
    The function parse string column in dataframe to extract WD.
    The function adds "wd" column.
    The wd data extracts by mask "WD=\d*%".

    Example: WD=10%

    :param df: pd.DataFrame
    :param col: column name that will be parsed
    :return: None
    """
    df['wd'] = df[col].str.upper()
    df['wd'] = df['wd'].str.extract(r"(WD=\d*%)", expand=False)
    df['wd'].fillna('WD=0%', inplace=True)
    df['wd'] = df.wd.str.slice(3, -1).astype(float)
    if df.wd.nunique() == 1:
        logging.warning(f"add_wd_from_string_col - passed column ({col}) produce only one value for wd")


def add_esp_from_string_col(df, col='Mechanics+'):
    """
    The function parse string column in dataframe to extract WD.
    The function adds "esp" column.
    The wd data extracts by mask "ESP=\d+(\.\d*)?".

    Example: ESP=39.50

    :param df: pd.DataFrame
    :param col: column name that will be parsed
    :return: None
    """
    df['esp'] = df[col].str.upper()
    df['esp'] = df['esp'].str.extract(r"(ESP=\d+(\.\d*)?)", expand=False)
    df['esp'].fillna('esp=0.0', inplace=True)
    df['esp'] = df.esp.str.slice(4, ).astype(float)
    if df.esp.nunique() == 1:
        logging.warning(f"add_esp_from_string_col - passed column ({col}) produce only one value for esp")


def get_last_order_by_chain():
    order_files = glob.glob(r'../data/raw/Uplifttool_CustOrd_*.xlsx')

    files_by_chain = defaultdict(list)
    for file in order_files:
        name = os.path.basename(file)
        _, _, chain, tail = name.split('_')
        date, _ = tail.split('.')
        files_by_chain[chain].append(file)

    order_by_chain = dict()
    for chain in files_by_chain.keys():
        order_by_chain[chain] = max(files_by_chain[chain])

    return order_by_chain


def get_latest_customer_plans():
    from auxiliary import get_customer_plan_chain_x5, get_customer_plan_dixy, get_customer_plan_tander
    order_by_chain = get_last_order_by_chain()

    product_md_file = '../data/masterdata/Product_MD.csv'
    product_md = pd.read_csv(product_md_file, sep=';', decimal=',', encoding='windows-1251', usecols=[0, 13, 34])

    func_by_chain = {
        'Perekrestok': get_customer_plan_chain_x5,
        'Pyaterochka': get_customer_plan_chain_x5,
        'Dixy': get_customer_plan_dixy,
        'Tander': get_customer_plan_tander,
    }

    sheet_by_chain = {
        'Perekrestok': 'Перекресток',
        'Pyaterochka': 'Пятерочка',
    }

    buffer = []
    for chain, file in order_by_chain.items():

        if chain not in func_by_chain.keys():
            msg = f'get_latest_customer_plans: Cant define function-parser for chain {chain}. File {file} skipped'
            logging.warning(msg)
            continue

        sheet = sheet_by_chain.get(chain, None)
        plan = func_by_chain[chain](sheet, file, chain, product_md)
        buffer.append(plan)

    return pd.concat(buffer)


def recover_promo_shipments(df):
    """Temporary solution to a line shipments in the promo slots. 
    The function define BIAS and multiply promo values on this BIAS
    Assamption: there is only one key in passed dataframe
    Args:
        df (DataFrame): sample dataframe 

    Return:
        df (DataFrame): modified dataframe
    """

    past_plane_promo = df.loc[(df['Partition'] == 'train') & (df['FT_SHIP_START'] != '0'), 'FT_SHIP_START'].unique()
    
    for ft_promo in past_plane_promo:
        planed = df[df['FT_SHIP_START'] == ft_promo].copy()
        fact = df[df['SHIP_START'] == ft_promo].copy()

        if planed.empty or fact.empty:
            continue

        all_fact = fact['Ordered'].sum()
        pp_fc = planed['Ordered'].sum()

        if pp_fc != 0:
            planed['Ordered'] = planed['Ordered'] / pp_fc * all_fact
            planed['SHIP_START'] = ft_promo

        df.loc[df['FT_SHIP_START'] == ft_promo, 'Ordered'] = planed['Ordered']
        
    return df


def copy_res_in_nq_folder(res, filters, now_str, result_file_path, OUTPUT_MQ_FOLDER):
    name = f'MLForecast_{now_str}.csv'
    if filters is None:
        shutil.copy(result_file_path, os.path.join(OUTPUT_MQ_FOLDER, 'Results_AFT', name))
    else:
        last_res = max(glob.glob(os.path.join(OUTPUT_MQ_FOLDER, 'Results_AFT', '*')))
        df = pd.read_csv(last_res)
        df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
        columns = list(df.columns)
        df = add_cat_cust(df, key_col='key')

        if filters.get('Products', False):
            product_filters = filters['Products']
            if product_filters.get('Categories', False):
                df = df[~df['PRD_ProdType'].isin(product_filters['Categories'])]
            if product_filters.get('Product_names', False):
                df = df[~df['Sku/линейка'].isin(product_filters['Product_names'])]
        if filters.get('Chains', False):
            df = df[~df.Chain.isin(filters['Chains'])]

        df = df[columns]
        res = res.copy()
        res = res.append(df)
        res.to_csv(os.path.join(OUTPUT_MQ_FOLDER, 'Results_AFT', name), index=False)


def transform_feautures_selector(X, columns=['M', 'W', 'Q', 'S']):
    """Find columns in dataset for ohe transformation"""

    list_columns = list(set(columns) - set(columns).difference(X.columns))
    return pd.get_dummies(X, columns=list_columns)


def sin_cos_transform(data):
    """ Sin cos transform features
    input:
        data: pd.DataFrame() - dataframe column for sin cos transformation,
    output:
        df.column, df.column: pd.Series() - Series with sin, cos tranformation column """
    
    circle_features =['M', 'W', 'DoM', 'DoW', 'DoY', 'Q', 'S']
    for col in data.columns:
        if col in circle_features:
            cyclical_count = data[col].max() - data[col].min() + 1
            data[col + '_sin'] = np.sin(2*np.pi*data[col]/cyclical_count)
            data[col + '_cos'] = np.cos(2*np.pi*data[col]/cyclical_count)
            data.drop(col, axis=1)
    return data
