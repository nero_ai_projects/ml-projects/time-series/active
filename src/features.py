import logging

import pandas as pd
import numpy as np
import datetime as dt

from collections import defaultdict

import utils
import preprocessing
import glob

from sklearn.linear_model import LinearRegression

import gc


@utils.gc_collect
def add_calendar(df, ymw_only=False, date_col='Date'):
    """
    The function adds calendar columns to the dataframe: 'Y' (year), 'M' (month; from 1), 'W' (week; from 1), 'DoM' (day of month; from 1), 
                                                         'DoW' (day of week; 0 is for Monday), 'DoY' (day of year; from 1),
                                                         'HY' (half of year; 1 or 2)', 'Q' (quarter; 1 to 4), 
                                                         'S' (season; 1 for Winter, 2 for Spring, 3 for Summer, 4 for Autumn)
                                                         
    :param df: pd.DataFrame
    :param ywm_only: if True, only year, month and week features are added; otherwise other calendar features added
    :param date_col: name of df's date column
    
    :return: pd.DataFrame with added columns
    """
    
    if df[date_col].dtype == np.dtype('O'):
        df[date_col] = pd.to_datetime(df[date_col], format='%Y-%m-%d')

    df['Y'] = df[date_col].dt.year
    df['M'] = df[date_col].dt.month
    df['W'] = df[date_col].dt.week

    if ymw_only:
        return df

    df['DoM'] = df[date_col].dt.day
    df['DoW'] = df[date_col].dt.weekday
    df['DoY'] = df[date_col].dt.dayofyear
    df['HY'] = (df['M']-1)//6 + 1
    df['Q'] = (df['M']-1)//3 + 1
    df['S'] = df['M'].replace(12, 0)//3 + 1
    return df


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_PINC(sales: pd.DataFrame):
    """The function adds a "PINC" column to the dataframe. 
    Uses the PINC.csv file compiled from raw pinc data

    Args:
        sales (DataFrame): DataFrame to append columns to. Need to have 'key', 'Y', 'M' columns

    Returns:
        DataFrame: DataFrame from input with appended column
    """
    pinc = get_processed_pinc(window=False)

    sales['PRODUCT_CODE'] = sales['key'].str[:4]
    df = sales.merge(pinc, how='left', on=['PRODUCT_CODE', 'Y', 'M'])
    df.drop(columns='PRODUCT_CODE', inplace=True)
    df.fillna(0, inplace=True)

    return df


def get_processed_pinc(window=True):
    """
    Function read and prepare preprocessed pinc data.
    :return:
    """
    pinc = pd.read_csv('../data/processed/pinc.csv',
                       dtype={'Y': 'float', 'M': 'float', 'PRODUCT_CODE': 'str', 'PINC': 'float'})
    pinc['PINC'] = pinc['PINC'].round(decimals=4)

    if not window and 'window' in pinc.columns:
        del pinc['window']

    return pinc


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_inno(sales: pd.DataFrame):
    """The function adds inno's columns ("ON_TOP" and "ROTATION") to the dataframe.
    To define inno skus uses latest file by mask '../data/raw/Uplifttool_SKUIn_*.csv'

    Args:
        sales (DataFrame): DataFrame to append columns to. Need to have 'key', 'Y', 'M' columns

    Returns:
        DataFrame: DataFrame from input with appended column
    """
    files = glob.glob(r'../data/raw/Uplifttool_SKUIn_*.csv')
    file = max(files)

    inno = pd.read_csv(file, sep=';', encoding='windows-1251')
    dtype = {'PRODUCT_CODE': str}
    hierarchy = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251', dtype=dtype)
    hierarchy = hierarchy[['ProductName', 'PRODUCT_CODE']].drop_duplicates()

    inno = pd.concat([inno, pd.get_dummies(inno.Inno)], axis=1)
    inno = inno.groupby(['Product', 'Window'], as_index=False).agg({'ON TOP': 'sum', 'ROTATION': 'sum'})
    inno['Y'] = inno.Window.str[:4].astype(int)
    inno['M'] = inno.Window.str[5:]
    inno['M'] = inno['M'].map({'Q1': 2, 'Q2': 4, 'Q3': 7, 'Q4': 10, 'Q5': 12})

    inno = inno.merge(hierarchy, left_on='Product', right_on='ProductName')
    if inno.empty:
        logging.warning('add_inno: got empty inno dataset')

    inno.rename(columns={'ON TOP': 'ON_TOP'}, inplace=True)
    # inno.drop(columns=['Product', 'Window', 'Sku/линейка'], inplace=True)
    inno = inno.groupby(['PRODUCT_CODE', 'Y', 'M'], as_index=False).agg({'ON_TOP': 'sum', 'ROTATION': 'sum'})

    sales['PRODUCT_CODE'] = sales['key'].str[:4]

    sales = sales.merge(inno, how='left', left_on=['PRODUCT_CODE', 'M', 'Y'], right_on=['PRODUCT_CODE', 'M', 'Y'])

    sales.drop(columns='PRODUCT_CODE', inplace=True)
    sales.fillna(0, inplace=True)

    return sales


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_delisting_mark_for_product(sales: pd.DataFrame):
    """
    The function add count of delisted skus in the product.
    To define delisted skus uses latest file by mask '../data/raw/Uplifttool_SKUOUT_*.csv'

    :param sales: pd.DataFrame
    :return: sales with column 'count_delist_sku'
    """ 
    files = glob.glob(r'../data/raw/Uplifttool_SKUOUT_*.csv')
    file = max(files)

    outs = pd.read_csv(file, sep=';', encoding='windows-1251')
    dtype = {'PRODUCT_CODE': str}
    hierarchy = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251', dtype=dtype)
    hierarchy = hierarchy[['ProductName', 'PRODUCT_CODE']].drop_duplicates()

    outs.rename(columns={'Amount of out skus': 'count_delist_sku'}, inplace=True)

    outs['Y'] = outs.Window.str[:4].astype(int)
    outs['M'] = outs.Window.str[5:]
    outs['M'] = outs['M'].map({'Q1': 2, 'Q2': 4, 'Q3': 7, 'Q4': 10, 'Q5': 12})

    outs = outs.merge(hierarchy, left_on='Product', right_on='ProductName', how='inner')
    if outs.empty:
        logging.warning('add_delisting_mark_for_product: got empty dataset')

    # outs.drop(columns=['Product', 'Window', 'Sku/линейка'], inplace=True)
    outs = outs.groupby(['PRODUCT_CODE', 'Y', 'M'], as_index=False).agg({'count_delist_sku': 'sum'})

    sales['PRODUCT_CODE'] = sales['key'].str[:4]

    sales = sales.merge(outs, how='left', left_on=['PRODUCT_CODE', 'M', 'Y'], right_on=['PRODUCT_CODE', 'M', 'Y'])

    sales.drop(columns='PRODUCT_CODE', inplace=True)
    sales.fillna(0, inplace=True)

    return sales


@utils.gc_collect
def add_seasonal_promo(sales):
    """The function adds a "seasonal" column to the dataframe. 
    Marks seasonal catalogs for the PYATEROCHKA and MAGNIT if the promo is longer than 20 days.

    Args:
        sales (DataFrame): DataFrame to append columns to. Need to have 'key', 'Partition', 'SHIP_START', 'FT_SHIP_START', 'Promo_day_num' columns

    Returns:
        [DataFrame]: DataFrame from input with appended column
    """
    sales = sales.copy()

    cust = pd.read_csv('../data/masterdata/Customer_MD.csv', encoding='windows-1251', sep=';', dtype={'Techbillto':'str'})
    cust = cust[['Techbillto', 'Chain']].drop_duplicates()

    season_cust = ['PYATEROCHKA', 'MAGNIT']
    cust = cust[cust['Chain'].isin(season_cust)]

    sales['SHIP_PROMO_START'] = np.where(sales['Partition'] == 'train', sales['SHIP_START'], sales['FT_SHIP_START'])

    sales['SHIP_PROMO_START'] = sales['SHIP_PROMO_START'].astype(str)

    seasonal_promo = sales[sales['SHIP_PROMO_START'] != '0'].groupby(['key', 'SHIP_PROMO_START'],
                                                                as_index=False).agg({'Promo_day_num': 'max'})

    seasonal_promo = seasonal_promo[seasonal_promo['Promo_day_num'] > 20]

    seasonal_promo['Promo_day_num'] = seasonal_promo['Promo_day_num'].fillna(0)
    seasonal_promo = seasonal_promo.fillna('0')


    if len(seasonal_promo) == 0:
        sales['seasonal'] = 0
        return sales


    seasonal_promo['Techbillto'] = seasonal_promo['key'].str.split("_", n = 1, expand = True)[1]
    seasonal_promo = cust.merge(seasonal_promo, how='inner', left_on=['Techbillto'], right_on=['Techbillto'])
    seasonal_promo = seasonal_promo[['key', 'SHIP_PROMO_START']]
    seasonal_promo['seasonal'] = 1

    df = sales.merge(seasonal_promo, how='left', left_on=['key', 'SHIP_PROMO_START'], right_on=['key', 'SHIP_PROMO_START'])
    df = df.drop(columns=['SHIP_PROMO_START'])
    df = df.fillna(0)

    return df


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_profile(ex):
    """
    The function adds the profile (historical behaviour during promo periods) using weighted average (latter promos with greater weights).
    Added columns: 'Profile', 'max_value_day'
    
    :param ex: pd.DataFrame (required columns: 'key', 'Ordered', 'Promo_day_num', 'Partition')
    
    :return: pd.DataFrame with added columns (
    """

    pp = ex[(ex['Promo_day_num']!=0)&(ex['Partition']=='train')]
    if pp.empty:
        ex['Profile'] = 0
        ex['max_value_day'] = 0

    else:
        pp = pp[['key', 'Promo_day_num', 'Ordered']].groupby(['key', 'Promo_day_num'],
                                    as_index=False).agg(lambda x: np.average(x, weights=[i for i in np.linspace(1, 10, len(x))]))

        pp.rename(columns={'Ordered': 'Profile'}, inplace=True)
        ex = pd.merge(ex, pp, how='left', on=['key', 'Promo_day_num'])
        ex.fillna(0, inplace=True)
        max_value = pp.groupby(['key'], as_index=False)['Profile'].max()
        max_value.rename(columns={'Profile': 'max_value'}, inplace=True)

        pp = pp.merge(max_value, on='key', how='left')
        max_value_day = pp[pp['Profile'] == pp['max_value']][['key', 'Promo_day_num']]
        max_value_day.rename(columns={'Promo_day_num': 'max_value_day'}, inplace=True)
        ex = ex.merge(max_value_day[['key', 'max_value_day']], on=['key'], how='left' )
        
        ex['max_value_day'] = np.where(ex['Promo_day_num'] == ex['max_value_day'], 1, 0)
        ex['max_value_day'] = ex['max_value_day'].astype(int)
    
    return ex


def add_promo_lvl(ex):
    """
    Will be deprecated
    """
    results = []
    for _, mdf in ex.groupby('key'):
        mdf_base = mdf[(mdf['Partition']=='train')&(mdf['Discount on date']==0)].tail(90)['Ordered'].mean()  ##,дневной безлайн для обрезки снизу
        mdf['TMP_SHIP_START'] = np.where(mdf['Partition']=='train', mdf['SHIP_START'], mdf['FT_SHIP_START'])
        mdf_promo = mdf[mdf['TMP_SHIP_START'].astype(str)!='0'].groupby(['key', 'TMP_SHIP_START'], as_index=False).agg({'Ordered':'sum',
                                                                                                                  'Promo_day_num':'max',
                                                                                                               'Partition':'last'})

#         mdf_promo = mdf_promo.rename(columns={'Ordered':'max_vol'})
        mdf_promo['Daily_vol'] = mdf_promo['Ordered']/mdf_promo['Promo_day_num']
#         mdf_promo['MA'] = mdf_promo['Daily_vol'].rolling(window=5, win_type='hamming', min_periods=1, center=True).mean()
        model = LinearRegression()
        try:
            X = mdf_promo[mdf_promo['Partition']=='train'].index[-3:].values.reshape(-1, 1)
            y = mdf_promo[mdf_promo['Partition']=='train']['Daily_vol'][-3:]
            model.fit(X, y)
            res = model.predict(mdf_promo[mdf_promo['Partition']!='train'].index.values.reshape(-1, 1))
            mdf_promo['promo_lvl'] = mdf_promo[mdf_promo['Partition']=='train']['Daily_vol'].tolist() + res.tolist()
        except:
            mdf_promo['promo_lvl'] = 0

        
        mdf_promo['TMP_SHIP_START'] = mdf_promo['TMP_SHIP_START'].astype(str)
        mdf = pd.merge(mdf, mdf_promo[['key', 'TMP_SHIP_START', 'promo_lvl']], how='left', on=['key', 'TMP_SHIP_START']).fillna(0)
        mdf['base'] = mdf_base
        mdf['promo_lvl'] = np.maximum(mdf['promo_lvl'], mdf['base'])
        mdf = mdf.drop(columns=['TMP_SHIP_START', 'base'])
        results.append(mdf)
    return pd.concat(results)


@utils.gc_collect
def add_big_disc(ex):
    """
    The function adds a column with max discount on each date.
    Added columns: 'big_disc'
    
    :param ex: pd.DataFrame (required columns: 'key', 'Partition', 'Discount on date')
    
    :return: pd.DataFrame with added column
    """
    max_discs = ex[(ex['Partition']=='train')].groupby(['key'], as_index=False).agg({'Discount on date':'max'})
    max_discs = max_discs.rename(columns={'Discount on date':'big_disc'})
    ex = pd.merge(ex, max_discs, how='left', on=['key']).fillna(0)
    ex['big_disc'] = np.where(ex['big_disc'] - ex['Discount on date'] <=5, 1, 0)
    return ex


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_act_promo(ex):
    """
    The function transforms the information about promo in data frame.
    Added columns: 'Promo_day_num', 'Promo_day_num_backward', 'Discount on date'
    Removed columns: 'DISCOUNT', 'SHIP_END', 'PROMO_ID', 'Chain', 'PRD_ProdType', 'PRD_UbrellaBrand', 'Sku/линейка'
    
    :param ex: pd.DataFrame (required columns: 'PGI_DATE', 'KEY', 'SHIP_START', 'SHIP_END', 'DISCOUNT', 'Chain', 'ORDER_QUAN', 'GI_QUAN')
    
    :return: pd.DataFrame with added columns
    """
    import datetime
    # заполняем пропущенными скидками добавленные в add_zeros строки
    ex_promo = ex[ex['SHIP_START'] != '']
    ex_promo['SLOT_START'] = utils.td(ex_promo['SHIP_START']) #запоминаем мастердатную дату начала слота
    ex_promo['SLOT_END'] = utils.td(ex_promo['SHIP_END'])
    promo_df = ex_promo.groupby(['KEY', 'SLOT_START'], as_index=False).agg({'DISCOUNT': 'max', 'PGI_DATE': ['min', 'max'],#})
                                                                            'SLOT_END': 'first'})
    promo_df.columns = ['KEY', 'SLOT_START', 'DISCOUNT', 'SHIP_START', 'SHIP_END', 'SLOT_END']
    del ex_promo
    _ = gc.collect()
    
    promo_df['SHIP_len'] = promo_df['SHIP_END'] - promo_df['SHIP_START']
    promo_df['SLOT_len'] = promo_df['SLOT_END'] - promo_df['SLOT_START']
    promo_df = utils.add_cat_cust(promo_df, key_col='KEY')
    
#     # костыль (по мотивам работы с некорректным прогнозом по ключам METRO): фиксим промо-акции с малым кол-вом продаж
    idx = (promo_df['SHIP_END']==promo_df['SHIP_START']) & (promo_df['Chain'].isin(['METRO', 'AUCHAN']))
    promo_df.loc[idx, 'SHIP_END'] = promo_df.loc[idx, 'SLOT_END']

    idx = (promo_df['SHIP_len'] < promo_df['SLOT_len']) & (promo_df['Chain'].isin(['METRO', 'AUCHAN']))
    promo_df.loc[idx, 'SHIP_END'] = promo_df.loc[idx, 'SLOT_END']
    promo_df.loc[idx, 'SHIP_START'] = promo_df.loc[idx, 'SLOT_START']

    promo_df.drop(columns=['Techbillto', 'PRODUCT_CODE', 'Chain', 'PRD_ProdType', 'PRD_UbrellaBrand', 'Sku/линейка'], inplace=True)
    
    promo_df = promo_df[promo_df['SHIP_len'] < pd.Timedelta(50, 'D')]  # TODO: костыль. убрать, когда почистим факт. Ask @Nat
    promo_df.drop(columns=['SHIP_len', 'SLOT_len'], inplace=True)

    promo_df_dict = defaultdict(list)
    for row in promo_df.itertuples(index=False):
        dates = pd.date_range(start=row.SHIP_START, end=row.SHIP_END)
        n = len(dates)
        promo_df_dict['PGI_DATE'].extend(dates)
        promo_df_dict['Promo_day_num'].extend(np.arange(1, n+1))
        promo_df_dict['Promo_day_num_backward'].extend(np.arange(1, n+1)[::-1])
        promo_df_dict['Discount on date'].extend([row.DISCOUNT] * n)
        promo_df_dict['KEY'].extend([row.KEY] * n)
        promo_df_dict['SHIP_START'].extend([row.SLOT_START] * n)

    full_promo_df = pd.DataFrame.from_dict(promo_df_dict)
    full_promo_df.drop_duplicates(subset=['KEY', 'PGI_DATE'], inplace=True) # в одну дату - одно промо (см. скрн, необходимо расследование)

    ex = pd.merge(ex[['PGI_DATE', 'KEY', 'ORDER_QUAN', 'GI_QUAN']], full_promo_df, how='left', on=['PGI_DATE', 'KEY'])

    del full_promo_df
    _ = gc.collect()

    return ex


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_fintool(ex, future_promo):
    """
    The function adds the information from fintool to data frame.
    Added columns: 'FT_Promo_day_num', 'FT_Promo_day_num_backward', 'FT_DISCOUNT', 'FT_SHIP_START', 'FT_WD', 'FT_ESP'
    
    :param ex: pd.DataFrame (required columns: 'PGI_DATE', 'KEY')
    
    :return: pd.DataFrame with added columns
    """
    ex = pd.merge(ex, future_promo, how='left', on=['KEY', 'PGI_DATE'])
    ex.fillna(0, inplace=True)
    return ex


def get_future_promo(freeze_end, forecast_end, option='M-1', chess=None):
    """
    The function collects the information about promo from fintool.
    
    :param freeze_end: the end of freeze period (in format yyyymmdd)
    :param forecast_end: the end of forecast horizon (in format yyyymmdd)
    :param option: if 'Quarter', the information is collected from fintool_by_dates.csv file;
                   if 'M-1', the information is collected from fintool_by_dates_M-1.csv file (monthly option)
    :param chess: if False, chess files are not used for dataset construction; otherwise the future promo plans are taken from chess files
    
    :return: pd.DataFrame with the information about promo actions (both historical and future)
    """
    # добавим прогнозные значения промо из финтула
    # TODO: align type of parameter (it could be string 'False' or True as bool)
    if str(chess).lower() == 'false':
        if option == 'M-1':
            file_path = '../data/processed/fintool_by_dates_M-1.csv'
        elif option == 'Quarter':
            file_path = '../data/processed/fintool_by_dates.csv'
        else:
            raise ValueError(f'Got unknown option {option}')

        full_ft = pd.read_csv(file_path)
        full_ft['SHIP_START'] = pd.to_datetime(full_ft['SHIP_START'])
        full_ft['Total Discount'] = full_ft['Total Discount'] * 100
        full_ft['PGI_DATE'] = pd.to_datetime(full_ft['PGI_DATE'])

        full_ft.rename(columns={'wd': 'WD', 'esp': 'ESP'}, inplace=True)

        if 'WD' not in full_ft.columns:
            logging.warning('get_future_promo: WD missed in the processed aft file. set up zero')
            full_ft['WD'] = 0

        if 'ESP' not in full_ft.columns:
            logging.warning('get_future_promo: ESP missed in the processed aft file. set up zero')
            full_ft['ESP'] = 0
            
    else:
        if option == 'M-1':
            raise ValueError('Monthly option does not support chess using')
        chess_df_future = preprocessing.get_chess(freeze_end, forecast_end)
        # chess_df_future = preprocessing.get_chess()
        chess_df_future = utils.align_chess_format_with_aft(chess_df_future, add_wd=True, add_esp=True)

        min_chess_date = chess_df_future.PGI_DATE.min()
        file_path = '../data/processed/fintool_by_dates.csv'
        ft_df = pd.read_csv(file_path)
        ft_df['SHIP_START'] = pd.to_datetime(ft_df['SHIP_START'])
        ft_df['Total Discount'] = ft_df['Total Discount'] * 100
        ft_df['PGI_DATE'] = pd.to_datetime(ft_df['PGI_DATE'])
        ft_df_past = ft_df[ft_df['PGI_DATE'] < min_chess_date]
        full_ft = pd.concat([chess_df_future, ft_df_past])

    rename_mapping = {
        'SHIP_START': 'FT_SHIP_START',
        # 'SHIP_END': 'FT_SHIP_END',
        'Total Discount': 'FT_DISCOUNT',
        'WD': 'FT_WD',
        'ESP': 'FT_ESP',
    }
    full_ft.rename(columns=rename_mapping, inplace=True)
    full_ft.drop_duplicates(subset=['KEY', 'PGI_DATE'],
                            inplace=True)  # в одну дату - одно промо (необходимо расследование, бывает, что не одно)
    return full_ft


@utils.gc_collect
@utils.log_n_unique_keys
def exclude_wo_promo(ex):
    """
    The function excludes all the keys without any promo periods in forecasting period.
    
    :param ex: pd.DataFrame (requeired columns: 'key', 'Partition', 'Discount on date'
    
    :return: filtered pd.DataFrame
    """
    tmp = ex[ex['Partition']=='fc'].groupby(['key'], as_index=False).agg({'Discount on date':'max'})
    non_zero_list = tmp[tmp['Discount on date']>0]['key'].tolist()
    return ex[ex['key'].isin(non_zero_list)]


def add_promo(df):
    """
    Will be deprecated
    """
    df = add_act_promo(df)
    df = add_fintool(df)


    #также соберем данные в колонки из правильных источников
    df['Discount on date'] = np.where(df['Partition']=='train', df['Discount on date'], df['FT_DISCOUNT'])
    df['Promo_day_num'] = np.where(df['Partition']=='train', df['Promo_day_num'], df['FT_Promo_day_num'])
    df = df.drop(columns=['FT_DISCOUNT', 'FT_Promo_day_num'])
    
    df = exclude_wo_promo(df)

    return df


@utils.gc_collect
def add_holiday_features(ex):
    '''
    Adds Boolean columns for Bank Holiday periods in Russia.
    Following columns are added in returned DataFrame: ['new_year_holidays', 'lent', 'may_holidays', 'back_to_school', other_holidays', 'covid_lockdown_dates']
    
    :PARAMETERS:
    ex: pandas.DataFrame - DataFrame to append columns to. Need to have 'Date' column
    
    :OUTPUT:
    ex: pandas.DataFrame - input DataFrame with appended columns
    
    :NOTES:
    1) Assumes there is 'Date' column to create the flags
    2) Fills in dates from 2018 till 2025 inclusive only
    '''
    ex = ex.copy()
    # TODO: generalize to more generic timeframe
    ex['DateAsDT'] = ex['Date'].astype('datetime64[ns]') # temporary column
    # New Year - Jan 1st to Jan 10th
    ny_dates = []
    for year in range(2018, 2025):
        ny_dates += pd.date_range(dt.datetime(year, 1, 1), periods=10, freq='D').tolist()
    ex['new_year_holidays'] = np.where(ex['DateAsDT'].isin(ny_dates), True, False)
    # Lent - this one changes every year, TODO: generalize
    lent_dates = []
    lent_dates += pd.date_range(dt.datetime(2018, 2, 19), dt.datetime(2018, 4, 7), freq='D').tolist() #2018
    lent_dates += pd.date_range(dt.datetime(2019, 3, 11), dt.datetime(2019, 4, 27), freq='D').tolist() #2019
    lent_dates += pd.date_range(dt.datetime(2020, 3, 2), dt.datetime(2020, 4, 18), freq='D').tolist() #2020
    lent_dates += pd.date_range(dt.datetime(2021, 3, 15), dt.datetime(2021, 5, 1), freq='D').tolist() #2021
    lent_dates += pd.date_range(dt.datetime(2022, 3, 7), dt.datetime(2022, 4, 23), freq='D').tolist() #2022
    lent_dates += pd.date_range(dt.datetime(2023, 2, 27), dt.datetime(2023, 4, 15), freq='D').tolist() #2023
    lent_dates += pd.date_range(dt.datetime(2024, 3, 18), dt.datetime(2024, 5, 4), freq='D').tolist() #2024
    lent_dates += pd.date_range(dt.datetime(2025, 3, 3), dt.datetime(2025, 4, 19), freq='D').tolist() #2025
    ex['lent'] = np.where(ex['DateAsDT'].isin(lent_dates), True, False)
    # May Holidays - May 1st to May 11th, as generally people fill in the gaps by taking vacation
    may_holidays = []
    for year in range(2018, 2025):
        may_holidays += pd.date_range(dt.datetime(year, 5, 1), dt.datetime(year, 5, 11),freq='D').tolist()
    ex['may_holidays'] = np.where(ex['DateAsDT'].isin(may_holidays), True, False)
    # back to school
    back_to_school = []
    for year in range(2018, 2025):
        back_to_school += pd.date_range(dt.datetime(year, 8, 15), dt.datetime(year, 9, 1), freq='D').tolist()
    ex['back_to_school'] = np.where(ex['DateAsDT'].isin(back_to_school), True, False)
    # other holidays
    other_holidays = []
    for year in range(2018, 2025):
        other_holidays += pd.date_range(dt.datetime(year, 2, 22), dt.datetime(year, 2, 24),freq='D').tolist() # Motherland Defender's day
        other_holidays += pd.date_range(dt.datetime(year, 3, 7), dt.datetime(year, 3, 9),freq='D').tolist() # Women's day
        other_holidays += pd.date_range(dt.datetime(year, 6, 12), dt.datetime(year, 6, 14),freq='D').tolist() # Russia day
        other_holidays += pd.date_range(dt.datetime(year, 11, 4), dt.datetime(year, 11, 5),freq='D').tolist() # National Solidarity Day
    ex['other_holidays'] = np.where(ex['DateAsDT'].isin(other_holidays), True, False)
    # COVID lockdown
    covid_lockdown_dates = pd.date_range(dt.datetime(2020, 3, 23), dt.datetime(2020, 4, 30),freq='D').tolist()
    ex['covid_lockdown_dates'] = np.where(ex['DateAsDT'].isin(covid_lockdown_dates), True, False)
    ex.drop(columns=['DateAsDT'], inplace=True)
    return ex


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_stock_features(ex, apply_to=[], cutoff_date=None, filters=None):
    '''
    Adds stock features (inventory on customer DCs and in Sales Points) to the dataframe and returns it
    Following columns are added in returned DataFrame: ['ship_to_stock', 'DC_stock']
    
    :PARAMETERS:
    ex: pandas.DataFrame - DataFrame to append columns to
    apply_to: list of ['Chain', 'PRD_ProdType'] pairs, for which to add the features. If list is empthy, add them to everything
    cutoff_date: str, optional - date, after which make the feature zero, simulating lack of future knowledge. If None - won't cut the available stock data
    filters: list of ['Chains'] - if to apply to DIXY
    
    :OUTPUT:
    ex: pandas.DataFrame - input DataFrame with appended columns
    
    :NOTES:
    1) Fit to preprocessed Magnit stock file, Master Data files (Product_MD, MDM_CUSTOMERS, CUSTOMER_MD) and DIXY files (dixy jan jun milk.zip, SalesOUT_KA_Stoks200915_1454.zip) and their format. Links are hardcoded, format should stay the same.
    2) Assumes the ex DataFrame doesn't have master data mapped yet, just the key
    3) In case stock data cannot be merged (e.g. non-existent Techbillto or Product) - those values will be lost due to left join
    '''
    # TODO: How to align stock format?

    # -- read preprocessed Magnit stock - file was created from Excel ones (st1, st2...) --
    preprocessed_stock_file = '../data/processed/stocks/stocks.csv'
    stock = pd.read_csv(preprocessed_stock_file, parse_dates=[2])

    utils.round_float_cols(stock, decimals=4)
    stock, _ = utils.reduce_mem_usage(stock)

    # -- filter cutoff_date
    if cutoff_date is not None:
        stock = stock[stock['Date']<cutoff_date]
    # -- the keys are different from main dataframe, use Product and Customer Master Data to match --
    # Product code
    prod = pd.read_csv('../data/masterdata/Product_MD.csv', 
                       sep=';', 
                       encoding='windows-1251', 
                       dtype={'SKU':'int64', 'PRODUCT_CODE':'str'})
    prod = prod[['SKU', 'PRODUCT_CODE']]
    prod.rename(columns={'SKU': 'Product'}, inplace=True)
    stock = stock.merge(prod, how='left', on='Product')
    stock['PRODUCT_CODE'].fillna('', inplace=True)
    
    # Ship-To to TechBillTo mapping
    mdm_customers = pd.read_csv('../data/masterdata/MDM_CUSTOMERS.csv', 
                                encoding='windows-1251', 
                                sep=';', 
                                usecols=['ShipToID', 'TechToID'])
    
    # DC mapping
    cust = pd.read_csv('../data/masterdata/Customer_MD.csv', 
                       encoding='windows-1251', 
                       sep=';', 
                       dtype={'Techbillto':'str'})
    cust = cust[['Techbillto', 'customer name.2']] # merge on 'customer name.2', but 4 out of 40 DCs are not in Master Data
    cust.rename(columns={'customer name.2': 'DC_name'}, inplace=True)
    stock = stock.merge(cust, how='left', on='DC_name')
    stock['Techbillto'].fillna('', inplace=True)
    
    # group by key (Techbillto / DC_name / Date, as specific Products are irrelevant)
    stock['key'] = stock['PRODUCT_CODE'] + '_' + stock['Techbillto']
    stock = stock.groupby(['Date', 'key'])['ship_to_stock', 'DC_stock'].sum().reset_index()

    if filters is not None and 'DIXI' in filters.get('Chains', []):
        # DIXY part (v1, small one)
        dixy_stock = pd.read_csv('../data/processed/stocks/dixy jan jun milk.zip', compression='zip', header=1, delimiter=';')
        dixy_stock.drop(columns=['Week', 'Unnamed: 3', 'Ship to ID', '(SO-OSA) OSA', 'Delivered Units', '(SO-OSA) CAF'], inplace=True)
        dixy_stock['Date'] = dixy_stock['Date'].astype('datetime64[ns]')

        utils.round_float_cols(dixy_stock, decimals=4)
        dixy_stock, _ = utils.reduce_mem_usage(dixy_stock)

        # -- filter cutoff_date
        if cutoff_date is not None:
            dixy_stock = dixy_stock[dixy_stock['Date']<cutoff_date]
        # lookup TBT by ShipTo
        dixy_stock = dixy_stock[dixy_stock['Ship To SAP']!='##']
        dixy_stock[['Ship To SAP']] = dixy_stock[['Ship To SAP']].astype('int64')
        dixy_stock = dixy_stock.merge(mdm_customers, how='left', left_on='Ship To SAP', right_on='ShipToID')
        dixy_stock = dixy_stock[dixy_stock['TechToID'].isna()==False]
        dixy_stock[['TechToID']] = dixy_stock[['TechToID']].astype('int64')
        dixy_stock.rename(columns={'TechToID': 'Techbillto', 'Stock Units': 'ship_to_stock'}, inplace=True)
        dixy_stock.drop(columns=['Ship To SAP', 'ShipToID'], inplace=True)
        dixy_stock = dixy_stock.merge(prod, how='left', on='Product')
        _ = gc.collect()
        dixy_stock['PRODUCT_CODE'].fillna('', inplace=True)
        dixy_stock[['Techbillto']] = dixy_stock[['Techbillto']].astype('str')
        dixy_stock['key'] = dixy_stock['PRODUCT_CODE'] + '_' + dixy_stock['Techbillto']
        dixy_stock.drop(columns=['Product', 'PRODUCT_CODE', 'Techbillto'], inplace=True)
        dixy_stock['DC_stock'] = 0
        dixy_stock = dixy_stock.groupby(['Date', 'key'], as_index=False)['ship_to_stock', 'DC_stock'].sum()
        # combine in a single dataframe
        stock = stock.append(dixy_stock)
        del dixy_stock
        _ = gc.collect()

        # DIXY part (v2, big one - 77M entries)
        dixy_data = pd.read_csv('../data/processed/stocks/SalesOUT_KA_Stoks200915_1454.zip', compression='zip', encoding='windows-1251', delimiter=';', decimal=',')
        dixy_data['Date'] = pd.to_datetime(dixy_data['TR_DATE'], dayfirst=True)
        # ADD SHIPTO_DATA
        dixy_data = dixy_data[dixy_data['SHPTO_SAP']!='##']
        dixy_data[['SHPTO_SAP']] = dixy_data[['SHPTO_SAP']].astype('int64')
        dixy_data = dixy_data.merge(mdm_customers, how='left', left_on='SHPTO_SAP', right_on='ShipToID')
        dixy_data = dixy_data[dixy_data['TechToID'].isna()==False] # NOTE: LOTS of lines get filtered out here...
        dixy_data.drop(columns=['TR_DATE', 'WEEK_ID', 'SKU_DESC', 'SHPTO_SAP', 'SHIP_TO', 'SOOSA_OSA', 'DELIVEREDUNITS', 'SOOSA_CAF', 'ShipToID'], inplace=True)
        dixy_data[['TechToID']] = dixy_data[['TechToID']].astype('int64')
        dixy_data[['TechToID']] = dixy_data[['TechToID']].astype('str')

        utils.round_float_cols(dixy_data, decimals=4)
        dixy_data, _ = utils.reduce_mem_usage(dixy_data)

        # -- filter cutoff_date
        if cutoff_date is not None:
            dixy_data = dixy_data[dixy_data['Date']<cutoff_date]
        dixy_data.rename(columns={'SKU': 'Product', 'TechToID': 'Techbillto', 'STOCKUNITS': 'ship_to_stock'}, inplace=True)
        dixy_data = dixy_data[dixy_data['Techbillto']!='##']
        dixy_data = dixy_data.merge(prod, how='left', on='Product')
        dixy_data['PRODUCT_CODE'].fillna('', inplace=True)
        dixy_data['key'] = dixy_data['PRODUCT_CODE'] + '_' + dixy_data['Techbillto']
        dixy_data.drop(columns=['Product', 'PRODUCT_CODE', 'Techbillto'], inplace=True)
        dixy_data['DC_stock'] = 0
        dixy_data['ship_to_stock'] = dixy_data['ship_to_stock'].astype('int64')
        dixy_data = dixy_data.groupby(['Date', 'key'])['ship_to_stock', 'DC_stock'].sum().reset_index()
        # combine in a single dataframe
        stock = stock.append(dixy_data)
        del dixy_data
    
    del prod
    del cust
    del mdm_customers
    
    # -- extend ex DataFrame --
    # merge DFs
#     stock['Date'] = stock['Date'].dt.strftime('%Y-%m-%d')
    stock['Date'] = pd.to_datetime(stock['Date'], format='%Y-%m-%d')
    # only apply to selected customers\categoties
    if len(apply_to)>0:
        st_cols=stock.columns.tolist()
        selected = pd.DataFrame(apply_to, columns=['Chain', 'PRD_ProdType'])
        stock = utils.add_cat_cust(stock)
        # todo: can we optimize it? drop for example
        stock = pd.merge(stock, selected, how='inner', on=['Chain', 'PRD_ProdType'])
        stock = stock[st_cols]
    ex = ex.merge(stock, how='left', on=['Date', 'key'])
    # fill NAs with zeroes (there's no data - and it's unclear how to approximate. Current approach will split them apart anyway)
    ex['ship_to_stock'].fillna(0, inplace=True)
    ex['DC_stock'].fillna(0, inplace=True)
    del stock
    return ex

def add_stock_days_of_sales(ex, window=7):
    '''
    Appends stock features in Days Of Sales for a particular combination to the dataframe and returns it
    Input DataFrame must have 'ship_to_stock' and 'DC_stock' columns (i.e. require add_stock_features(ex) run)
    Implementation takes a 7-day lookback average as "average sales per day" approximation by default
    Columns added: ['ship_to_stock_DOS', 'DC_stock_DOS']
    
    :PARAMETERS:
    ex: pandas.DataFrame - DataFrame to append columns to
    window: int, default=7 - length of moving average window to evaluate 'average sales per day'
    
    :OUTPUT:
    ex: pandas.DataFrame - input DataFrame with appended columns ['ship_to_stock_DOS', 'DC_stock_DOS']
    
    :NOTES:
    Input DataFrame must have 'ship_to_stock' and 'DC_stock' columns (i.e. require add_stock_features(ex) run) and 'Date' column
    '''
    ex = ex.copy()
    # check existence of necessary columns and add them if necessary
    if not all(item in ex.columns.tolist() for item in ['ship_to_stock', 'DC_stock']):
        ex = add_stock_features(ex)
        
    # parse DateTime from Date column for slicing
    ex['dt'] = ex['Date'].astype('datetime64[ns]')
    # rolling average sales over 'window' past days
    ex['avg_daily_sales'] = ex.groupby('key').rolling(window=window, on='dt')['Ordered'].mean().reset_index(drop=True)
    # calculate inventory in Days Of Sales
    ex['ship_to_stock_DOS'] = ex['ship_to_stock'] / ex['avg_daily_sales']
    ex['DC_stock_DOS'] = ex['DC_stock'] / ex['avg_daily_sales']
    # drop unused columns
    ex.drop(columns=['dt', 'avg_daily_sales'], inplace=True)
    # fill NAs/Inf with 365 (arbitrary - year of supply)
    ex.replace([np.inf, -np.inf], np.nan, inplace=True)
    ex['ship_to_stock_DOS'].fillna(365, inplace=True)
    ex['DC_stock_DOS'].fillna(365, inplace=True)
    ex['ship_to_stock_DOS'] = np.where(ex['ship_to_stock'] == 0, 0, ex['ship_to_stock_DOS'])
    ex['DC_stock_DOS'] = np.where(ex['DC_stock'] == 0, 0, ex['DC_stock_DOS'])
    return ex

def add_over_understock_flags(ex, oos_thresh=1, overstock_thresh=30, fc_cutoff=None):
    '''
    Appends Out Of Stock and Overstock boolean flags based on stock in Days Of Sales
    Input DataFrame should have ['ship_to_stock_DOS', 'DC_stock_DOS'] columns (i.e. require add_stock_features(ex) and add_stock_days_of_sales(ex) run), otherwise they will be added as step 1
    Default thresholds for flagging: Out Of Stock - stock less than 1 day of sales, Overstock - stock more than 30 days of sales
    Columns added: ['DC_oos', 'DC_overstock', 'ship_to_oos', 'ship_to_overstock']
    
    :PARAMETERS:
    ex: pandas.DataFrame - DataFrame to append columns to
    oos_thresh: int, default=1 - stock in Days Of Sales, below which the data point will be marked as Out Of Stock
    overstock_thresh: int, default=30 - stock in Days Of Sales, above which the data point will be marked as Overstock
    fc_cutoff: str (convertable to DateTime) - date, after which all output flags will be marked as False to simulate lack of knowledge about future
    
    :OUTPUT:
    ex: pandas.DataFrame - input DataFrame with appended columns ['DC_oos', 'DC_overstock', 'ship_to_oos', 'ship_to_overstock']
    
    :NOTES:
    Input DataFrame should have ['ship_to_stock_DOS', 'DC_stock_DOS'] columns (i.e. require add_stock_features(ex) and add_stock_days_of_sales(ex) run), otherwise they will be added as step 1
    '''
    ex = ex.copy()
    # check existence of necessary columns and add them if necessary
    if not all(item in ex.columns.tolist() for item in ['ship_to_stock_DOS', 'DC_stock_DOS']):
        ex = add_stock_days_of_sales(ex)
    
    # initialize all flags as False
    ex['DC_oos'] = 0
    ex['DC_overstock'] = 0
    ex['ship_to_oos'] = 0
    ex['ship_to_overstock'] = 0
    # filter later, for now those will be uniform
    ex[ex['DC_stock_DOS']<oos_thresh]['DC_oos'] = 1
    ex[ex['DC_stock_DOS']>overstock_thresh]['DC_overstock'] = 1
    ex[ex['ship_to_stock_DOS']<oos_thresh]['ship_to_oos'] = 1
    ex[ex['ship_to_stock_DOS']>overstock_thresh]['ship_to_overstock'] = 1
    if fc_cutoff is not None:
        ex['Date'] = ex['Date'].astype('datetime64[ns]')
        ex[ex['Date']>=fc_cutoff]['DC_oos'] = 0
        ex[ex['Date']>=fc_cutoff]['DC_overstock'] = 0
        ex[ex['Date']>=fc_cutoff]['ship_to_oos'] = 0
        ex[ex['Date']>=fc_cutoff]['ship_to_overstock'] = 0
        ex['Date'] = ex['Date'].astype('object')
    # filter only the points where we had actual data
    ex['DC_oos'] = np.where(ex['DC_stock'] == 0, 0, ex['DC_oos'])
    ex['DC_overstock'] = np.where(ex['DC_stock'] == 0, 0, ex['DC_overstock'])
    ex['ship_to_oos'] = np.where(ex['ship_to_stock'] == 0, 0, ex['ship_to_oos'])
    ex['ship_to_overstock'] = np.where(ex['ship_to_stock'] == 0, 0, ex['ship_to_overstock'])
    return ex


@utils.gc_collect
@utils.save_passed_dataframe_if_debug
def add_WD(df):
    """The function adds a "WD" column to the dataframe. 
    Uses raw wd data, customer and product hierarchy

    Args:
        sales (DataFrame): DataFrame to append columns to. Need to have 'key', 'Y', 'W' columns

    Returns:
        DataFrame: DataFrame from input with appended column
    """

    wd = get_processed_wd()

    df = df.merge(wd, how='left', on=['key', 'Y', 'W'])
    df['WD'].fillna(0, inplace=True)
    df['WD'] = np.where(df['Partition'] == 'train', df['WD'], 0)

    return df


@utils.gc_collect
def get_wd():
    """
    The function read and combine data from historical wd files
    :return: pd.Dataframe(columns=['key', 'Y', 'W', 'WD'])
    """

    wd_1_20 = pd.read_csv('../data/raw/WD/WD 1-20.csv', encoding='windows-1251', sep=';', skiprows=12,
                          dtype={'Week ': 'str'})
    wd_1_20 = wd_1_20[~wd_1_20['Unified Name'].isna()]
    wd_1_20['WD, %'] = wd_1_20['WD, %'].apply(lambda x: float(str(x).replace('%', '').replace(',', '.')) / 100)
    wd_21_40 = pd.read_csv('../data/raw/WD/WD20-42.csv', encoding='windows-1251', sep=';', skiprows=13,
                           dtype={'Week ': 'str'})
    wd_21_40 = wd_21_40[~wd_21_40['Unified Name'].isna()]
    wd_21_40['WD, %'] = wd_21_40['WD, %'].apply(lambda x: float(str(x).replace('%', '').replace(',', '.')) / 100)
    wd_21_40['Week '] = wd_21_40['Week '].str[:6]

    wd = pd.read_excel('../data/raw/WD/WD.xlsx', dtype={'PGI Week Of Year': 'str'})
    wd_2020 = wd_1_20.append(wd_21_40)
    wd_2020.columns = wd.columns
    wd = wd.append(wd_2020)
    cust = pd.read_csv('../data/masterdata/Customer_MD.csv',
                       encoding='windows-1251',
                       sep=';',
                       dtype={'Techbillto': 'str'}, usecols=['Techbillto', 'Chain']).drop_duplicates()
    prod = pd.read_csv('../data/masterdata/Product_MD.csv',
                       encoding='windows-1251',
                       sep=';',
                       dtype={'PRODUCT_CODE': 'str'}, usecols=['PRODUCT_CODE', 'BP_ProdNature']).drop_duplicates()
    wd = wd.merge(prod, how='left', left_on=['Unified Name'], right_on=['BP_ProdNature'])
    wd = wd.groupby(['Proxy Category', 'PRODUCT_CODE', 'PGI Week Of Year'], as_index=False).agg({'Итог': 'mean'})
    wd['Y'] = wd['PGI Week Of Year'].str[:4]
    wd['W'] = wd['PGI Week Of Year'].str[4:]
    wd = wd.astype({'Y': 'int', 'W': 'int'}, copy=False)
    wd = wd.merge(cust, how='left', left_on=['Proxy Category'], right_on=['Chain'])
    wd.rename(columns={'Итог': 'WD'}, inplace=True)
    wd['key'] = wd['PRODUCT_CODE'] + '_' + wd['Techbillto']
    wd = wd[['key', 'Y', 'W', 'WD']]

    return wd


def get_latest_wd():
    """
    The function return pd.Dataframe(columns=['key', 'Y', 'W', 'WD']) with wd data.
    The function extract data from the last file that was found by mask '../data/raw/Uplifttool_WD_*.xlsx'
    If the function can't find any file or doesn't find needed columns the empty df will be returned.

    :return: pd.Dataframe(columns=['key', 'Y', 'W', 'WD']) with wd data.
    """

    outcome_columns = ['key', 'Y', 'W', 'WD']

    files = glob.glob(r'../data/raw/Uplifttool_WD_*.xlsx')
    if files:
        file = max(files)
    else:
        logging.warning("get_latest_wd: Can't file any file for wd. Return empty df")
        return pd.DataFrame(columns=outcome_columns)

    # expected_columns = ['Week', 'Chain', 'Product name', 'WD, %'] приводим к согласованному фомату
    expected_columns = ['Date', 'Chain', 'Product name', 'WD']

    wd = pd.read_excel(file)
    wd.columns = [col.strip() for col in wd.columns]
    missed_cols = set(expected_columns).difference(set(wd.columns))
    if missed_cols:
        logging.warning(f"get_latest_wd: Return empty df because can't find columns: {missed_cols}")
        return pd.DataFrame(columns=outcome_columns)

    wd.dropna(inplace=True)
    # wd.rename(columns={'WD, %': 'WD'}, inplace=True)

    cust = pd.read_csv('../data/masterdata/Customer_MD.csv',
                       encoding='windows-1251',
                       sep=';',
                       dtype={'Techbillto': 'str'}, usecols=['Techbillto', 'Chain']).drop_duplicates()
    prod = pd.read_csv('../data/masterdata/Product_MD.csv',
                       encoding='windows-1251',
                       sep=';',
                       dtype={'PRODUCT_CODE': 'str'}, usecols=['PRODUCT_CODE', 'BP_ProdNature']).drop_duplicates()

    wd = wd.merge(prod, how='left', left_on=['Product name'], right_on=['BP_ProdNature'])
    wd.dropna(inplace=True)
    # wd = wd.groupby(['PRODUCT_CODE', 'Chain', 'Week'], as_index=False).agg({'WD': 'mean'})
    # wd['Week'] = wd['Week'].astype(str) приводим к согласованному фомату
    # wd['Y'] = wd['Week'].str[:4]
    # wd['W'] = wd['Week'].str[4:]

    wd = wd.groupby(['PRODUCT_CODE', 'Chain', 'Date'], as_index=False).agg({'WD': 'mean'})
    wd['Date'] = wd['Date'].astype(str)
    wd['Y'] = wd['Date'].str[:4]
    wd['W'] = wd['Date'].str[4:]

    wd = wd.astype({'Y': 'int', 'W': 'int'}, copy=False)
    wd = wd.merge(cust, how='left', on=['Chain'])
    wd.dropna(inplace=True)

    wd['key'] = wd['PRODUCT_CODE'] + '_' + wd['Techbillto']

    for col in wd.columns:
        if col not in outcome_columns:
            del wd[col]

    return wd


def get_processed_wd():
    """
    The function read and return processed wd data from file

    :return: pd.DataFrama
    """
    wd = pd.read_csv(r'../data/processed/wd.csv')
    wd.dropna(inplace=True)
    return wd


@utils.gc_collect
def add_customer_plans(ex, apply_to=[], cutoff_date=None, plan_visibility_days=30):
    '''
    Adds customer plans in kg as 'cust_plan' feature to the input ex DataFrame
    Feature visibility in time is limited into the future by 'cutoff_date + plan_visibility_days' to make realistic in training loop
    Uses preprocessed file (by auxiliary.load_customer_plans()) to get the input data, link is hardcoded in first few lines
    Runtime ~4 minutes
    
    :PARAMETERS:
    ex: pandas.DataFrame - DataFrame with history/future data, to which we'll append the feature
    apply_to: list of pairs ['Chain', 'PRD_ProdType'], for which to append the cust_plan feature (will be 0 for others)
    cutoff_date: None or str - if string, should be convertable to DateTime, start of the forecast period for customers (rather than fact)
    plan_visibility_days: int (default=30) - number of calendar days, by which limit visibility of customer plans (to avoid leakage in training)
    
    :OUTPUT:
    ex: pandas.DataFrame - ex DataFrame from input parameters with added ['cust_plan'] column
    
    :NOTES:
    1) Uses preprocessed customer plans' file and Master Data files, links are hardcoded in first few lines
    2) Product Master Data is used to convert customer plans from Units to Kg
    3) As X5 Chains make forecast on Region level, the forecasted volume is split proportionally to sum of 'Ordered' (past) or 'Profile' (future) in the relevant Techbillto's:
        - the Techbillto's which belong to the region based on Customer Master Data
        - the Techbillto's which actually do the sales (i.e. not closed)
    4) As all customers provide their plans for total period of promo, that volume is split proportionally to Techbillto's 'Ordered' (past) or 'Profile' (future) over that period, REGARDLESS of 'SHIP_START'/'FT_SHIP_START' identifiers
    '''

    ex['Date'] = pd.to_datetime(ex['Date'])
    # Load preprocessed customer plans and Master Data files
    product_md_file = '../data/masterdata/Product_MD.csv'
    cust_md_file = '../data/masterdata/Customer_MD.csv'
    customer_plans_combined_file = '../data/processed/customer_plans.csv'
    customer_plans = pd.read_csv(customer_plans_combined_file, 
                                 encoding='windows-1251',
                                 parse_dates = ['Shipment Start', 'Shipment End', 'Store Start', 'Store End'])

    # only aply to selected customers\categories
    if len(apply_to)>0:
        selected = pd.DataFrame(apply_to, columns=['Chain', 'PRD_ProdType'])
        product_md = pd.read_csv(product_md_file, sep=';', decimal=',', encoding='windows-1251', usecols=[15, 34])
        customer_plans = pd.merge(customer_plans, product_md, how='left', on='PRODUCT_CODE')
        del product_md
        customer_plans = pd.merge(customer_plans, selected, how='inner', on=['Chain', 'PRD_ProdType'])
        customer_plans = customer_plans[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 
                                         'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # cutoff future dates 30 days beyond cutoff
    cutoff_date = pd.to_datetime(cutoff_date)
    if cutoff_date is not None:
        customer_plans = customer_plans[customer_plans['Shipment End']<(cutoff_date+dt.timedelta(days=plan_visibility_days))]
    # fix leaking non-numeric input
    if customer_plans.dtypes['Объем, шт'] == 'O':
        customer_plans.drop(customer_plans['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    customer_plans['Объем, шт'] = pd.to_numeric(customer_plans['Объем, шт'])
    # convert from units to kg
    customer_plans['PRODUCT_CODE'] = customer_plans['PRODUCT_CODE'].astype(str).str.zfill(4)
    product_md = pd.read_csv(product_md_file, sep=';', decimal=',', encoding='windows-1251', usecols=[13, 34])
    product_md['BP_WnitWeight'] = product_md['BP_WnitWeight'].astype('float64')
    product_md.dropna(inplace=True)
    product_md = product_md.groupby('PRODUCT_CODE', as_index=False)['BP_WnitWeight'].min()
    product_md['PRODUCT_CODE'] = product_md['PRODUCT_CODE'].astype(str).str.zfill(4)
    customer_plans = customer_plans.merge(product_md, left_on='PRODUCT_CODE', right_on='PRODUCT_CODE', how='left')
    customer_plans['Объем, кг'] = (customer_plans['Объем, шт'] * customer_plans['BP_WnitWeight']) / 1000
    del product_md
    # Iterate over the promo offers and add them to the dataframe
    customer_plans.rename(columns={'Shipment Start': 'Shipment_Start', 'Shipment End': 'Shipment_End', 'Объем, кг': 'Q'}, inplace=True)
    # customer_plans.groupby('Chain')['Q'].sum()  # ds: this line

    _ = gc.collect()

    # Make 2 loops: for non-X5 accounts (direct TechBillTo mapping) and for X5 (split by active Techbillto's AND days)
    # -- Direct TechBillTo loop -- (~1m:20s, ~80% of KG data actually mapped)
    # create a DataFrame with columns
    # CUST_PLANNED_DATE | key | cust_plan_raw | SLOT_START_PLANNED
    # then merge it to main df and in there split daily proportionally to the Actuals (past) / Profile (future)
    promo_df_dict_non_x5 = defaultdict(list)
    for row in customer_plans.itertuples(index=False):
        if (row.Chain in ['KARUSEL', 'PEREKRESTOK', 'PYATEROCHKA']):
            continue # not relevant for X5
        dates = pd.date_range(start=row.Shipment_Start, end=row.Shipment_End)
        key =  row.PRODUCT_CODE + '_' + str(int(row.Techbillto))
        plan = row.Q
        slot_start = row.Shipment_Start
        n = len(dates)
        promo_df_dict_non_x5['CUST_PLANNED_DATE'].extend(dates) # one for each day of planned promo slot
        promo_df_dict_non_x5['KEY'].extend([key] * n) # same inside the same slot
        promo_df_dict_non_x5['cust_plan_raw'].extend([plan] * n) # same inside the same slot (FULL plan on that slot - will be scaled after merge)
        promo_df_dict_non_x5['SLOT_START_PLANNED'].extend([slot_start] * n) # same inside the same slot

    promo_df_non_x5 = pd.DataFrame.from_dict(promo_df_dict_non_x5)
    del promo_df_dict_non_x5
    promo_df_non_x5 = promo_df_non_x5.drop_duplicates(subset=['KEY', 'CUST_PLANNED_DATE'])
    # merge with main dataframe
    ex = pd.merge(ex, promo_df_non_x5, how='left', left_on=['key', 'Date'], right_on=['KEY', 'CUST_PLANNED_DATE'])
    del promo_df_non_x5
    _ = gc.collect()

    ex[['CUST_PLANNED_DATE', 'KEY', 'cust_plan_raw']] = ex[['CUST_PLANNED_DATE', 'KEY', 'cust_plan_raw']].fillna(0)
    ex[['SLOT_START_PLANNED']] = ex[['SLOT_START_PLANNED']].fillna(dt.datetime(2000, 1, 1))
    # get total Actuals / Profile for the SLOT_START_PLANNED

    # TODO: rewrite next two lines
    slot_subtotals = ex.groupby(['KEY', 'SLOT_START_PLANNED'], as_index=False)[['Ordered', 'Profile']].sum()
    ex = pd.merge(ex, slot_subtotals, how='left', on=['KEY', 'SLOT_START_PLANNED'], suffixes=('', '_subtotal'))
    del slot_subtotals
    _ = gc.collect()

    # and calculate PROPORTION as % of line's Ordered / Profile from total promo slot (vectorized for performance)
    ex['Proportion_Ordered'] = ex['Ordered'] / ex['Ordered_subtotal']
    ex['Proportion_Profile'] = ex['Profile'] / ex['Profile_subtotal']
    ex[['Proportion_Ordered', 'Proportion_Profile']] = ex[['Proportion_Ordered', 'Proportion_Profile']].fillna(0)
    ex['Proportion_flag'] = 0
    ex.loc[ex['Ordered_subtotal']!=0, 'Proportion_flag'] = 1
    ex.loc[ex['Partition']=='fc', 'Proportion_flag'] = 0 # avoid leakage from the future
    ex['Proportion'] = ex['Proportion_flag']*ex['Proportion_Ordered'] + ex['Proportion_Profile']*(1-ex['Proportion_flag'])
    # get what we need (proportion * customer_plan_raw) and delete temporary columns
    ex['cust_plan_non_x5'] = ex['cust_plan_raw'] * ex['Proportion']
    ex['cust_plan_non_x5'] = ex['cust_plan_non_x5'].fillna(0)

    cols_to_del = [
        'CUST_PLANNED_DATE', 'KEY', 'cust_plan_raw', 'SLOT_START_PLANNED',
        'Ordered_subtotal', 'Profile_subtotal', 'Proportion_Ordered', 'Proportion_Profile',
        'Proportion_flag', 'Proportion'
    ]
    for col in cols_to_del:
        del ex[col]
    
    # -- X5: loop by Region -- (~2m:40s, ~70% of KG data actually mapped)
    # First, add 'Region' column to the main dataframe based on key - will use it for mapping
    ex['Techbillto'] = ex['key'].str.split('_', n=1, expand=True)[1]
    cust_md = pd.read_csv(cust_md_file, encoding='windows-1251', sep=';', dtype={'Techbillto':'str'}, usecols=[0, 4])
    cust_md.rename(columns={'customer name.1': 'Region'}, inplace=True)
    cust_md['Region'] = cust_md['Region'].str.strip()
    ex = pd.merge(ex, cust_md, how='left', on='Techbillto')
    del cust_md
    ex.drop(columns=['Techbillto'], inplace=True)
    _ = gc.collect()

    ex['Region'] = ex['Region'].fillna('')
    ex['PRODUCT_CODE'] = ex['key'].str.split('_', n=1, expand=True)[0]
    # create a DataFrame with columns
    # CUST_PLANNED_DATE | PRODUCT_CODE | Region | cust_plan_raw | SLOT_START_PLANNED
    # then merge it to main df and in there split daily proportionally to the Actuals (past) / Profile (future)
    promo_df_dict_x5 = defaultdict(list)
    for row in customer_plans.itertuples(index=False):
        if (row.Chain not in ['KARUSEL', 'PEREKRESTOK', 'PYATEROCHKA']):
            continue # not relevant - not X5
        dates = pd.date_range(start=row.Shipment_Start, end=row.Shipment_End)
        prd_code = row.PRODUCT_CODE
        region = row.Region.strip()
        plan = row.Q
        slot_start = row.Shipment_Start
        n = len(dates)
        promo_df_dict_x5['CUST_PLANNED_DATE'].extend(dates) # one for each day of planned promo slot
        promo_df_dict_x5['PRODUCT_CODE'].extend([prd_code] * n) # same inside the same slot
        promo_df_dict_x5['Region'].extend([region] * n) # same inside the same slot
        promo_df_dict_x5['cust_plan_raw'].extend([plan] * n) # same inside the same slot (FULL plan on that slot - will be scaled after merge)
        promo_df_dict_x5['SLOT_START_PLANNED'].extend([slot_start] * n) # same inside the same slot
    promo_df_x5 = pd.DataFrame.from_dict(promo_df_dict_x5)
    del promo_df_dict_x5
    promo_df_x5 = promo_df_x5.drop_duplicates(subset=['PRODUCT_CODE', 'Region', 'CUST_PLANNED_DATE'])
    # merge with main dataframe
    ex = pd.merge(ex, promo_df_x5, how='left', left_on=['Date', 'PRODUCT_CODE', 'Region'], right_on=['CUST_PLANNED_DATE', 'PRODUCT_CODE', 'Region'])
    del promo_df_x5
    _ = gc.collect()

    ex[['CUST_PLANNED_DATE', 'cust_plan_raw']] = ex[['CUST_PLANNED_DATE', 'cust_plan_raw']].fillna(0)
    ex[['SLOT_START_PLANNED']] = ex[['SLOT_START_PLANNED']].fillna(dt.datetime(2000, 1, 1))
    # get total Actuals / Profile for the SLOT_START_PLANNED
    slot_subtotals = ex.groupby(['Region', 'PRODUCT_CODE', 'SLOT_START_PLANNED'], as_index=False)[['Ordered', 'Profile']].sum()
    ex = pd.merge(ex, slot_subtotals, how='left', on=['Region', 'PRODUCT_CODE', 'SLOT_START_PLANNED'], suffixes=('', '_subtotal'))
    del slot_subtotals
    _ = gc.collect()

    # and calculate PROPORTION as % of line's Ordered / Profile from total promo slot (vectorized for performance)
    ex['Proportion_Ordered'] = ex['Ordered'] / ex['Ordered_subtotal']
    ex['Proportion_Profile'] = ex['Profile'] / ex['Profile_subtotal']
    ex[['Proportion_Ordered', 'Proportion_Profile']] = ex[['Proportion_Ordered', 'Proportion_Profile']].fillna(0)
    ex['Proportion_flag'] = 0
    ex.loc[ex['Ordered_subtotal']!=0, 'Proportion_flag'] = 1
    ex.loc[ex['Partition']=='fc', 'Proportion_flag'] = 0 # avoid leakage from the future
    ex['Proportion'] = ex['Proportion_flag']*ex['Proportion_Ordered'] + ex['Proportion_Profile']*(1-ex['Proportion_flag'])
    # get what we need (proportion * customer_plan_raw) and delete temporary columns
    ex['cust_plan_x5'] = ex['cust_plan_raw'] * ex['Proportion']
    ex['cust_plan_x5'] = ex['cust_plan_x5'].fillna(0)

    cols_to_del = [
        'Region', 'PRODUCT_CODE', 'CUST_PLANNED_DATE', 'cust_plan_raw', 'SLOT_START_PLANNED',
        'Ordered_subtotal', 'Profile_subtotal', 'Proportion_Ordered', 'Proportion_Profile',
        'Proportion_flag', 'Proportion'
    ]
    for col in cols_to_del:
        del ex[col]
    _ = gc.collect()

    # Get total splitted customer plan
    ex['cust_plan'] = ex['cust_plan_non_x5'] + ex['cust_plan_x5']
    ex.drop(columns=['cust_plan_non_x5', 'cust_plan_x5'], inplace=True)
    return ex


# TODO: to check
@utils.gc_collect
def add_prices(data, filters=None):
    """The function adds the availability of co-invest, regular and discount prices, shelf discount ("CoInvest", 'Reg_price', 'Promo_price', 'price_discount').
    The function requires SI_simple.csv, sttk_md_promo.csv

    Args:
        data (DataFrame): DataFrame to append columns to. Need to have 'key', 'SHIP_START' columns
        filters: filters that could be applied for SI_simple.csv

    Returns:
        [DataFrame]: DataFrame from input with appended column
    """

    avg_price = get_avg_price()  # -> 'Aver Price', 'key', 'Date', 'Price_Type'

    columns = ['PGI_DATE', 'TECH_BILL_TO', 'ORDER_QUAN',
               'GI_QUAN', 'SKU', 'DISCOUNT', 'PGI_DATA', 'PROMO_ID']

    # TODO: почему мы не можем использовать ex для этого ? - Нужно протащить promo_id
    sales = preprocessing.get_simple_SI(filters=filters)
    sales.rename(columns={'KEY': 'key'}, inplace=True)
    sales = sales[['key', 'PROMO_ID']].drop_duplicates()
    _ = gc.collect()

    # actuals promo
    promo_cols = ['PROMO_ID', 'SHIP_START', 'SHIP_END', 'STORE_START', 'STORE_END']
    promo = preprocessing.get_promo_md(promo_cols)
    promo = pd.merge(sales, promo, how='left', on='PROMO_ID').drop_duplicates()
    
    del sales
    _ = gc.collect()
    
    for col in ['SHIP_START', 'SHIP_END', 'STORE_START', 'STORE_END']:
        promo[col] = pd.to_datetime(promo[col], format='%Y%m%d')
        
    promo = promo.groupby(['key', 'SHIP_START'], as_index=False).agg({'SHIP_END': 'max', 'STORE_START': 'min', 'STORE_END': 'max'})
    
    promo_df_dict = defaultdict(list)
    promo = promo[['key', 'SHIP_START', 'SHIP_END', 'STORE_START', 'STORE_END']]
    _ = gc.collect()

    for key, SHIP_START, SHIP_END, STORE_START, STORE_END in promo.itertuples(index=False):
        dates = pd.date_range(start=STORE_START, end=STORE_END)
        n = len(dates)
        
        promo_df_dict['Date'].extend(dates)
        promo_df_dict['key'].extend([key] * n)
        promo_df_dict['SHIP_START'].extend([SHIP_START] * n)
        promo_df_dict['STORE_START'].extend([STORE_START] * n)    
        promo_df_dict['STORE_END'].extend([STORE_END] * n)

    full_promo_df = pd.DataFrame.from_dict(promo_df_dict)
    
    price_with_ShSt = avg_price.merge(full_promo_df, how='left', on=['key', 'Date'])
    
    del full_promo_df
    del avg_price
    
    prices_promo_and_not_in_promo = defaultdict(list)

    for key, df in price_with_ShSt.groupby(['key']):
        promo_prices = df[df['Price_Type'] == 'Promo'].groupby(['SHIP_START'],
                                                                  as_index=False).agg({'Aver Price': 'mean',
                                                                                       'STORE_START': 'max',
                                                                                       'STORE_END': 'max'}).rename(columns={'Aver Price': 'Promo_price'})

        for SHIP_START, promo_price, STORE_START, STORE_END in promo_prices.itertuples(index=False):
            reg_price = df[(df['SHIP_START'].isna()) & (df['Price_Type'] == 'Reg') &
                ((df['Date'] >= STORE_START - pd.Timedelta(days=30)) |
                (df['Date'] <= STORE_END + pd.Timedelta(days=30)))]['Aver Price'].mean()

            prices_promo_and_not_in_promo['key'].extend([key])
            prices_promo_and_not_in_promo['SHIP_START'].extend([SHIP_START])        
            prices_promo_and_not_in_promo['STORE_START'].extend([STORE_START])        
            prices_promo_and_not_in_promo['STORE_END'].extend([STORE_END])        
            prices_promo_and_not_in_promo['Promo_price'].extend([promo_price])        
            prices_promo_and_not_in_promo['Reg_price'].extend([reg_price])


    price_for_coinv_without_reg_in_promo = pd.DataFrame.from_dict(prices_promo_and_not_in_promo)
    
    price_for_coinv_without_reg_in_promo = price_for_coinv_without_reg_in_promo[~price_for_coinv_without_reg_in_promo['Reg_price'].isna()] # TODO: drop na
    price_for_coinv_without_reg_in_promo['price_discount'] = (price_for_coinv_without_reg_in_promo['Reg_price'] - \
                                                       price_for_coinv_without_reg_in_promo['Promo_price']) / \
                                                        price_for_coinv_without_reg_in_promo['Reg_price'] * 100
    
    price_for_coinv_without_reg_in_promo['SHIP_START'] = price_for_coinv_without_reg_in_promo['SHIP_START'].dt.strftime('%Y-%m-%d')
    
    data['SHIP_START'] = data['SHIP_START'].apply(lambda x: str(x))  # TODO: check if columns is string
    SHIP_START = data['SHIP_START'].copy()
    data['SHIP_START'] = data['SHIP_START'].str[:10]
    data = data.merge(price_for_coinv_without_reg_in_promo[['key', 'SHIP_START', 'Reg_price', 'Promo_price', 
                                                            'price_discount']], how='left', on=['key', 'SHIP_START'])

    data['SHIP_START'] = SHIP_START
    
    data.fillna(0, inplace=True)

    data.loc[(data['price_discount'] < 0), 'price_discount'] = 0

    data = add_ft_reg_price(data)
    # esp == ft promo_price
    # ft_reg_price
    
    data['FT_price_discount'] = (data.FT_reg_price - data.FT_ESP) / data.FT_reg_price * 100
    data['FT_price_discount'].clip(lower=0, inplace=True)

    data['price_discount'] = np.where(data.Partition == 'train', data.price_discount, data.FT_price_discount)

    return data


def add_ft_reg_price(df):
    """
    The function adds FT_reg_price column that contains mean value of last 7 Reg_price values in the train.


    :param df: pd.DataFrame
    :return: pd.DataFrame
    """
    values = []
    for key, sample in df.groupby('key'):
        hist_price = sample[sample.Partition == 'train'].Reg_price.values
        last_avg_price = sum(hist_price[-7:]) / 7
        values.extend([last_avg_price] * len(sample))

    df['FT_reg_price'] = values

    return df


def add_coinvest(data, coinvest_threshold=5, discount_threshold=0,):

    """The function adds the availability of co-invest based on price_discount.

    Args:
        data (DataFrame): DataFrame to append columns to. Need to have 'Discount on date', 'price_discount' columns
        coinvest_threshold (int, optional): The difference between the shelf and customer discount (in percentage points) at which we consider that there was a co-investment. Defaults to 5.
        discount_threshold (int, optional): Percentage of discount above which we add co-investments. Defaults to 0.

    Returns:
        [DataFrame]: DataFrame from input with appended column
    """

    data['CoInvest'] = 0
    data.loc[(data['Discount on date'] > discount_threshold) &
             (data['Discount on date'] <= data['price_discount'] - coinvest_threshold), 'CoInvest'] = 1

    return data


def get_avg_price():
    """Converts a shelf pricing dataframe to a sellin format
    The function requires MDM_CUSTOMERS.csv, Product_MD.csv

    Returns:
        [DataFrame]: DataFrame with all shelf prices in a sellin format
    """
    prices = get_processed_prices()
    mdm_price = pd.read_csv('../data/masterdata/MDM_CUSTOMERS.csv', sep=';', encoding='windows-1251',
                            dtype={'ShipToID': 'str', 'TechToID': 'str', 'BillToID': 'str'},
                            usecols=['ShipToID', 'TechToID', 'ChainDesc']).drop_duplicates()
    res = prices.merge(mdm_price, how='inner', on=['ShipToID'])
    del prices
    del mdm_price
    _ = gc.collect()
    prod = pd.read_csv('../data/masterdata/Product_MD.csv', encoding='windows-1251', sep=';',
                       dtype={'PRODUCT_CODE': 'str', 'SKU': 'str'},
                       usecols=['PRODUCT_CODE', 'SKU', 'Sku/линейка'])
    res['SKU'] = res['SKU'].str.zfill(6)
    prod['SKU'] = prod['SKU'].str.zfill(6)
    prod.drop_duplicates(inplace=True)
    res = res.merge(prod, how='inner', on=['SKU'])
    _ = gc.collect()
    res['key'] = res['PRODUCT_CODE'] + '_' + res['TechToID']
    res['Aver Price'] = res['Aver Price'].apply(lambda x: float(str(x).replace(',', '.')))
    res = res.groupby(['key', 'Date', 'Price_Type'], as_index=False).agg({'Aver Price': 'mean'})
    return res


@utils.gc_collect
def get_prices():
    """Results in one format and combines raw data with shelf prices into one dataframe
    Uses raw shelf prices data, customer and product hierarchy

    Returns:
        [DataFtame]: DataFrame with all shelf prices
    """
    from tqdm.auto import tqdm

    tmp = []
    for name in tqdm(glob.glob('../data/raw/shelf price/*.zip')):
    # for name in glob.glob("/home/ubuntu/projects/Active/data/raw/shelf price/*.zip"):
        df = pd.read_csv(name, sep=';', encoding='windows-1251', compression='zip',
                         dtype={'Ship To': 'str', 'SKU': 'str', 'Data': 'str'})

        df['Data'] = df['Data'].apply(lambda x: x.replace('-', ''))
        df['Data'] = pd.to_datetime(df['Data'], format='%Y%m%d')

        tmp.append(df)
    # del df
    prices = pd.concat(tmp)
    del tmp
    _ = gc.collect()

    tmp = []
    cols_name = list(prices.columns)

    def ren_cols(df):
        columns = df.columns
        new_cols = []
        for col in columns:
            new_cols.append(col.strip().upper().replace(' ', '_'))
        df.columns = new_cols
        return df

    for name in tqdm(glob.glob('../data/raw/shelf price/*.csv')):
    # for name in glob.glob('/home/ubuntu/projects/Active/data/raw/shelf price/*.csv'):
        df = pd.read_csv(name, sep=';', encoding='windows-1251',  # compression='zip',
                         dtype={'Ship To': 'str', 'SKU': 'str', 'Data': 'str'})
        df = ren_cols(df)
        i = 0
        while df.columns[0] != 'SHIP_TO':
            df = pd.read_csv(name, sep=';', encoding='windows-1251',  # compression='zip',
                             dtype={'Ship To': 'str', 'SKU': 'str', 'Data': 'str'}, skiprows=i)
            df = ren_cols(df)
            #             print(df.columns)
            i += 1
        df.columns = cols_name

        df['Data'] = df['Data'].apply(lambda x: x.replace('-', ''))
        df['Data'] = pd.to_datetime(df['Data'], format='%Y%m%d')

        tmp.append(df)

    del df
    prices_csv = pd.concat(tmp)
    del tmp
    _ = gc.collect()

    prices = prices.append(prices_csv)

    del prices_csv

    tmp = []

    for file in tqdm(glob.glob(r'../data/raw/shelf price/*.txt')):
    # for file in glob.glob(r'/home/ubuntu/projects/Active/data/raw/shelf price/*.txt'):
        df = pd.read_csv(file, sep=';', dtype={'Ship To': str})
        df['Data'] = pd.to_datetime(df['Data'], format='%Y%m%d')
        # df['Data'] = df['Data'].astype(str)

        tmp.append(df)


    del df
    prices_txt = pd.concat(tmp)
    del tmp
    _ = gc.collect()

    prices = prices.append(prices_txt)

    del prices_txt

    prices.rename(columns={'Data': 'Date', 'Ship To': 'ShipToID'}, inplace=True)

    return prices


def get_processed_prices():
    """
    The function collects the information about prices from the file shelf_price.csv.
    
    :return: pd.DataFrame
    """
    dtype = {'ShipToID': 'str', 'SKU': 'str'}
    price = pd.read_csv(r'../data/processed/shelf_price.csv', dtype=dtype)
    price.Date = pd.to_datetime(price.Date)
    return price
