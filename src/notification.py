import base64
import smtplib
from smtplib import SMTPException, SMTPAuthenticationError
from email.mime.text import MIMEText
import sspi

import functools
import json
import datetime as dt

SMTP_EHLO_OKAY = 250
SMTP_AUTH_CHALLENGE = 334
SMTP_AUTH_OKAY = 235


def send_email(server, port, username,
               to: list, subject: str, body: str, ssl: bool = False, tls: bool = True, ntlm: bool = True,
               password=''):
    """
    The function send email.

    :param to: list of users
    :param subject: subject
    :param body: message
    :param tls:
    :param ssl:
    :param ntlm:

    :return: None
    """
    if ssl:
        server = smtplib.SMTP_SSL(server, port=port)
    else:
        server = smtplib.SMTP(server, port=port)

    if tls:
        print('tls')
        server.starttls()
    print('login')
    if ntlm:
        print('login by ntlm')
        connect_to_exchange_as_current_user(server)
    else:
        server.login(username, password)

    print('send')

    msg = MIMEText(body, 'plain')
    msg['From'] = username
    msg['To'] = ', '.join(to)
    msg['Subject'] = subject
    print(type(msg))
    print(msg)
    server.send_message(msg=msg)
    print('quit')
    server.quit()


def asbase64(msg):
    # encoding the message then convert to string
    return base64.b64encode(msg).decode("utf-8")


def connect_to_exchange_as_current_user(smtp):
    """Example:
    >>> import smtplib
    >>> smtp = smtplib.SMTP("my.smtp.server")
    >>> connect_to_exchange_as_current_user(smtp)
    """

    # Send the SMTP EHLO command
    code, response = smtp.ehlo()
    if code != SMTP_EHLO_OKAY:
        raise SMTPException("Server did not respond as expected to EHLO command")

    sspi_client = sspi.ClientAuth('NTLM')

    # Generate the NTLM Type 1 message
    sec_buffer = None
    err, sec_buffer = sspi_client.authorize(sec_buffer)
    ntlm_message = asbase64(sec_buffer[0].Buffer)

    # Send the NTLM Type 1 message -- Authentication Request
    code, response = smtp.docmd("AUTH", "NTLM " + ntlm_message)

    # Verify the NTLM Type 2 response -- Challenge Message
    if code != SMTP_AUTH_CHALLENGE:
        raise SMTPException("Server did not respond as expected to NTLM negotiate message")

    # Generate the NTLM Type 3 message
    err, sec_buffer = sspi_client.authorize(base64.decodebytes(response))
    ntlm_message = asbase64(sec_buffer[0].Buffer)

    # Send the NTLM Type 3 message -- Response Message
    code, response = smtp.docmd(ntlm_message)
    if code != SMTP_AUTH_OKAY:
        raise SMTPAuthenticationError(code, response)


def notification_decorator(func):
    if func.__name__ == 'copy_income_data':
        func_name = 'copy income data'
        e_mails = ['ivan.solomatin@get-advanced.com']
    elif func.__name__ == 'run_preprocessing':
        func_name = 'preprocessing'
        e_mails = ['ivan.solomatin@get-advanced.com']
    elif func.__name__ == 'run_tournament':
        func_name = 'calculation'
        e_mails = ['ivan.solomatin@get-advanced.com']
    elif func.__name__ == 'run_segmentation':
        func_name = 'segmentation'
        e_mails = ['ivan.solomatin@get-advanced.com']
    elif func.__name__ == 'uploading_for_data':
        func_name = 'prepare data for fintool'
        e_mails = ['ivan.solomatin@get-advanced.com']

    @functools.wraps(func)
    def wrapper(notif_flag, subject, *args, **kwargs):
        if notif_flag:
            with open('notify_settings.json') as fd:
                settings = json.load(fd)

            now_str = dt.datetime.now().strftime('%d.%m.%Y %H:%M:%S')

            settings['to'] = e_mails
            settings['subject'] = subject
            settings['body'] = 'Start ' + func_name + now_str

            send_email(**settings)

            try:
                res = func(*args, **kwargs)
            except BaseException:
                now_str = dt.datetime.now().strftime('%d.%m.%Y %H:%M:%S')
                settings['body'] = 'End ' + func_name + ':' + now_str + '; Result: failed'

                send_email(**settings)

            now_str = dt.datetime.now().strftime('%d.%m.%Y %H:%M:%S')
            settings['body'] = 'End ' + func_name + ':' + now_str + '; Result: done'

            send_email(**settings)
        else:
            res = func(*args, **kwargs)

        return res

    return wrapper


if __name__ == '__main__':
    with open('notify_settings.json') as fd:
        settings = json.load(fd)

    settings['to'] = ['ivan.solomatin@get-advanced.com']
    settings['subject'] = 'smtp testing'
    settings['body'] = 'the ntlm test was done'

    print(settings)
    send_email(**settings)
