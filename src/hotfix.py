import preprocessing
import tournament
import pandas as pd
import click
import logging
import os
import sys
import traceback
import datetime as dt
import json
import shutil

from scanner import OUTPUT_MQ_FOLDER
import segmentation
import postprocessing

DEBUG = False

TARGET = ['Ordered']
X_COLUMNS = ['Discount on date', 'Promo_day_num', 'Promo_day_num_backward', 'Y', 'M', 'W', 'DoM', 'DoW', 'DoY', 'HY',
             'Q', 'S',  # ,'Promo_day_num', 'Promo_day_num_backward'
             'PINC', 'ON_TOP', 'ROTATION', 'count_delist_sku', 'seasonal', 'Profile', 'max_value_day',
             'new_year_holidays', 'lent',
             'may_holidays', 'back_to_school', 'other_holidays', 'covid_lockdown_dates', 'ship_to_stock', 'DC_stock',
             'cust_plan',
             # 'WD',
             # 'ESP',
             # 'CoInvest',
             # 'price_discount',
             ]
CUST_METRIC_COLS = ['SHIP_START', 'FT_SHIP_START']
RESULT_FILE_PATH = r'../results/outputs/UplifttoolOutput.csv'
EVALS_FILE_PATH = r'../results/outputs/Evals.csv'
VERSIONS_DICT_PATH = r'../data/raw/ver_dict_q.csv'
PREV_FC_PATH = r'../results/outputs/.csv'


@click.command()
@click.option("--fc_type", default="PROD", help="")
@click.option("--promo_source", default='Fintool', help='')
@click.option("--save_preprocessed", default='True', help='')
@click.option("--freeze_start", default=None, help='')
@click.option("--freeze_end", default=None, help='')
@click.option("--forecast_end", default=None, help='')
@click.option("--use_prev_fintool", default=False, help='')
@click.option("--use_prev_customer_plans", default=False, help='')
@click.option("--target", default=None, help='')
@click.option("--x_columns", default=None, help='')
@click.option("--cust_metric_columns", default=None, help='')
@click.option("--versions_dict_path", default=None, help='')
@click.option('--filters_path', default=None, help='')
@click.option('--result_file_path', default=RESULT_FILE_PATH, help='')
@click.option('--evals_file_path', default=EVALS_FILE_PATH, help='')
@click.option('--chess', default=True)
@click.option('--segment_run', default=False, is_flag=True)
def main(fc_type='PROD', promo_source='Fintool', save_preprocessed=True,
         freeze_start=None, freeze_end=None, forecast_end=None,
         use_prev_fintool=False, use_prev_customer_plans=False,
         target=None, x_columns=None, cust_metric_columns=None, versions_dict_path=None,
         filters_path=None, result_file_path=RESULT_FILE_PATH,
         evals_file_path=EVALS_FILE_PATH, chess=True,
         segment_run=False
         ):
    """
    This function is a start point of forecasting. It contains checking of the parameters, preprocessing and
    forecast calculation.
    :param segment_run: if True make run for all possible keys for last 3 mont with 1 month on freeze period.
        The forecast outcome save as SEGMENT_RESULT_FILE_PATH and SEGMENT_EVALS_FILE_PATH.
    :param fc_type: string that contains one of the values: HIST, CUSTOM and PROD.
        - HIST should be used for the historical runs. Dates required: freeze_start, freeze_end,
        forecast_end.
        - CUSTOM require filters_path parameter. Only filtered keys will be forecasted. Filtering works with the actuals.
        data
        - PROD - is default value. If passed all keys will be predicted and dates could be calculated automatically.
    :param promo_source:
    :param save_preprocessed: if False preprocessed data will be cleaned up
    :param freeze_start: start date of freeze period
    :param freeze_end: end data of freeze period
    :param forecast_end: last forecast date
    :param use_prev_fintool: if True previously processed fintool data uses, otherwise fintoll data preprocess from raw
    files.
    :param use_prev_customer_plans: if True previously processed customer plans data uses,
    otherwise promo plan data preprocess from raw files.
    :param target:
    :param x_columns: feature list. if None use default list
    :param cust_metric_columns:
    :param versions_dict_path:
    :param filters_path: path to file that contains filters for custom run. File is json according to format below:
        {"Chains": ["CHAIN"], "Products": {"Categories": ["CAT1"], "Product_names": ["PRODUCT", "PRODUCT"]}}
        Filter file could contains Chains or|and Products filters. In Product filters only one type allowed: or Categories
        or Product_names. You can filter data only by Chains or Products if it is needed
    :param result_file_path: path to save forecast
    :param evals_file_path: path to save evaluation data
    :param chess: If True we use chess data else AFT promo source
    :return: None
    """

    logging.info('Starting main')
    # TODO: add into log info about used parameters

    if segment_run:
        logging.info('Got --segment_run option')

        result_file_path = segmentation.RESULT_FILE_PATH
        evals_file_path = segmentation.EVALS_FILE_PATH
        freeze_start = segmentation.freeze_start()
        freeze_end = segmentation.freeze_end()
        forecast_end = segmentation.forecast_end()
        chess = False
        params_info = f'freeze_start={freeze_start}, freeze_end={freeze_end}, forecast_end={forecast_end}, chess={chess}'

        logging.info(f'set up segmentation settings: {params_info}')

    # checking parameters
    dates_list = [freeze_start, freeze_end, forecast_end]
    if fc_type == 'HIST' and any([date is None for date in dates_list]):
        raise ValueError('freeze_start, freeze_end, forecast_end are required for rolling forecast type')

    if fc_type == 'CUSTOM' and filters_path is None:
        raise ValueError("fc_type 'CUSTOM' requires filters_path, but filters_path is None")

    # TODO: check all data

    filters = filters_path if filters_path is None else json.load(open(filters_path))

    x_columns = X_COLUMNS if x_columns is None else x_columns
    target = TARGET if target is None else target
    cust_metric_columns = CUST_METRIC_COLS if cust_metric_columns is None else cust_metric_columns
    versions_dict_path = VERSIONS_DICT_PATH if versions_dict_path is None else versions_dict_path

    versions_dict = pd.read_csv(VERSIONS_DICT_PATH, sep=":", header=None)
    versions_dict = dict(zip(versions_dict[0], versions_dict[1]))

    # data = preprocessing.run_preprocessing(versions_dict, fc_type=fc_type, save_preprocessed=save_preprocessed,
    #                                        freeze_start=freeze_start, freeze_end=freeze_end, forecast_end=forecast_end,
    #                                        use_prev_fintool=use_prev_fintool,
    #                                        use_prev_customer_plans=use_prev_customer_plans,
    #                                        chess=chess,
    #                                        filters=filters)
    #
    # logging.info(f'Starting calculations for {data.key.nunique()} keys')
    # results, evals = tournament.run_tournament(cust_metric_columns, data, target, x_columns)
    # results.to_csv(result_file_path.replace('.csv', '_wo_postprocessing.csv'), index=False)
    #
    # logging.info('starting postprocessing')
    # results = postprocessing.run_postprocessing(results)
    #
    # results.to_csv(result_file_path, index=False)
    # evals.to_csv(evals_file_path, index=False)
    #
    # if segment_run:
    #     logging.info('main is done')
    #     return

    logging.info(f'start copying data into output mq folder ({OUTPUT_MQ_FOLDER})')
    now_str = dt.datetime.now().strftime('%Y%m%d%H%M')
    name = f'MLForecast_{now_str}.csv'
    shutil.copy(result_file_path, os.path.join(OUTPUT_MQ_FOLDER, 'Results_AFT', name))

    logging.info(f'Starting segmentation report')
    segmentation_files = segmentation.run_segmentation()

    logging.info(f'start copying segmentation files into output mq folder ({OUTPUT_MQ_FOLDER})')
    for file in segmentation_files:
        filename = os.path.basename(file)
        shutil.copy(file, os.path.join(OUTPUT_MQ_FOLDER, 'Results_dashboard', filename))

    logging.info('main is done')


def set_up_logging():
    """
    Functions set up logging module:
        - define log file name as log_[current date and time].log
        - set log format
        - add console to log handlers
    :return: None
    """
    log_format = '%(asctime)s - %(levelname)s - %(message)s'
    log_name = f'log_{dt.datetime.today().strftime("%Y-%m-%d_%H_%M_%S")}.log'
    log_name = os.path.join(r'..', 'logs', log_name)
    logging.basicConfig(filename=log_name,
                        level=logging.INFO,
                        format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger('').addHandler(console)


if __name__ == '__main__':
    set_up_logging()

    try:
        args = sys.argv[1:]
        logging.info(f'Started with params: {args}')
        try:
            main()
        except SystemExit as e:
            if e.code == 0:
                logging.info('Successfully done: get 0 exit code')
            else:
                logging.info(f'Got error code : {e.code}')
    except BaseException as e:
        logging.fatal(traceback.format_exc())
