"""Модель Артема: вызов ее в models_pool и класс - сама модель"""
### Это часть из models_pool

#     params_art = dict()
#     pool_item = ModelPoolItem(model_name='xgb_art', params=params_art, constructor=Art_XGB)    
#     pool.append(pool_item)

#     params_pp = {'reg_lambda': 3,
#                      'objective': 'reg:squarederror',
#                      'n_estimators': 30,
#                      'max_depth': 7,
#                      'criterion': 'mse',
#                      'booster': 'gbtree',
#                       'learning_rate':0.3,}
#     pool_item = ModelPoolItem(model_name='xgb_pp', params=params_pp, constructor=XGB_PP)    
#     pool.append(pool_item)

### Это сама модель

# from sklearn.ensemble import GradientBoostingRegressor
# from sklearn import ensemble
# from sklearn.model_selection import TimeSeriesSplit

# class Art_XGB():
    
#     def __init__(self):
#         self.reg_bl = None
#         self.reg_all = None
#         self.best_est = 0
        
#     def custom_CV(self, sample):
#         if (len(sample) < 17) or (sample['Ordered'].sum() == 0):
#             return 30

#         tscv = TimeSeriesSplit(n_splits=15)
#         ress = pd.DataFrame()
#         splitscount  = 0
#         for train_index, test_index in tscv.split(sample):
#             splitscount+=1
#             if (splitscount>10):
#                 X_train = sample.iloc[train_index].drop(columns = ['Ordered'])
#                 y_train = sample.iloc[train_index]['Ordered']
#                 X_test = sample.iloc[test_index].drop(columns = ['Ordered'])
#                 y_test = sample.iloc[test_index]['Ordered']

#                 for estimator in [3,10,30,50,80,100,150,200]:
#                     reg = ensemble.GradientBoostingRegressor(n_estimators = estimator, 
#                                                              max_depth = 2,
#                                                              learning_rate = 0.1, 
#                                                              verbose = False)#,n_iter_no_change=15,validation_fraction=0.05)
#                     reg.fit(X_train, y_train)

#                     ress = ress.append([[estimator,-np.sum(abs(y_test - reg.predict(X_test))),splitscount]])
#         ressg = ress.groupby(0).mean()
#         self.best_est = ressg[1].idxmax()
#         return self.best_est
    
#     def fit(self, X_train, y_train):
#         df = X_train.copy()
#         df['Ordered'] = y_train
#         df_bl = df[df['Discount on date'] == 0]
#         df_bl = df_bl.groupby(['Y','W'],as_index = False).agg(np.mean)
        
#         X_train_bl = df_bl.drop(columns = ['Ordered'])
#         y_train_bl = df_bl['Ordered']

# #         if len(X_train_bl) == 0:
# #             return None

#         est = self.custom_CV(df_bl)

#         self.reg_bl = ensemble.GradientBoostingRegressor(n_estimators = est, 
#                                                  max_depth = 2,
#                                                  learning_rate = 0.1, 
#                                                  verbose = False)#,n_iter_no_change=15,validation_fraction=0.05)
#         self.reg_bl = self.reg_bl.fit(X_train_bl, y_train_bl)
#         X_train_week = X_train.groupby(['Y','W'], as_index = False).agg(np.mean)
#         X_train_week['pred'] = self.reg_bl.predict(X_train_week)

#         X_train = X_train.merge(X_train_week[['Y','W','pred']], how = 'left')
#         for i in range(-20, 20, 1):
#             X_train['disc-' + str(i)] = X_train['Discount on date'].shift(i).fillna(0)

#         self.reg_all = ensemble.GradientBoostingRegressor(n_estimators = 80, 
#                                                  max_depth = 2,
#                                                  learning_rate = 0.1, 
#                                                  verbose = False)#,n_iter_no_change=15,validation_fraction=0.05)

#         self.reg_all = self.reg_all.fit(X_train, y_train)
#         return self
    
#     def predict(self, X):

#         X_week = X.groupby(['Y','W'], as_index = False).agg(np.mean)
#         X_week['pred'] = self.reg_bl.predict(X_week)

#         X = X.merge(X_week[['Y','W','pred']], how = 'left')
#         for i in range(-20, 20, 1):
#             X['disc-' + str(i)] = X['Discount on date'].shift(i).fillna(0)

#         X['Forecast'] = self.reg_all.predict(X)
#         X['Forecast'] = np.where(X['Forecast']<0, 0.01, X['Forecast'])
#         return X['Forecast'].values


"""Модель Дениса: xgb regressor с добавлением профиля промо и дня наибольших продаж"""

# class XGB_PP(xgb.XGBRegressor):
    
#     def fit(self, X, y):
        
#         X['y'] = y
#         self.PP = X.groupby(['Promo_day_num'], 
#                        as_index=False).agg({'y': 'mean'}).rename(columns={'y': 'Profile Promo'})
# #         self.DOW_profile = X.groupby('DoW', as_index=False).agg({'y':'mean'})
        
        
#         X = X.merge(self.PP, how='left', left_on=['Promo_day_num'], 
#                     right_on=['Promo_day_num']).drop('y', axis=1).fillna(0)
# #         X = X.merge(self.DOW_profile, how='left', left_on=['DoW'], right_on=['DoW'])
        
        
#         max_value = self.PP['Profile Promo'].max()
#         self.max_value_pp_day = self.PP[self.PP['Profile Promo'] == max_value]['Promo_day_num'].values[0]
#         X['is_max_value_pp_day'] = np.where(X['Promo_day_num'] == self.max_value_pp_day, 1, 0)
        
#         super().fit(X, y)
        
#     def predict(self, X):
        
#         X = X.merge(self.PP, how='left', left_on=['Promo_day_num'], 
#                     right_on=['Promo_day_num']).fillna(0)
# #         X = X.merge(self.DOW_profile, how='left', left_on=['DoW'], right_on=['DoW'])

#         X['is_max_value_pp_day'] = np.where(X['Promo_day_num'] == self.max_value_pp_day, 1, 0)
        
#         return super().predict(X)


"""Модель Наташи: xgb regressor на промо-слотах"""

### сборка в mols_pool

# for i in itertools.product([2, 3, 4], [0.01, 0.3, 1, 2]): 
#     params_xgb_promo = dict()
#     params_xgb_promo['max_depth'] = i[0]
#     params_xgb_promo['eta'] = i[1]
#     pool_item = ModelPoolItem(model_name=f'xgb_promo_slots_{i[0]}_{i[1]}', params=params_xgb_promo, constructor=XGBPromoSlots)
#     pool.append(pool_item)

### сама модель
# class XGBPromoSlots():
    
#     def __init__(self, **params):

#         self.model = None
#         self.params = params
#         self.empty_flag = False
#         self.cols = ['M', 'Y', 'S', 'Q', 'Discount on date', 'Promo_day_num']
                
#     def fit(self, X_train, y_train):
#         self.model = xgb.XGBRegressor(**self.params)
#         df = X_train.copy()
#         df['Ordered']=y_train
        
#         #add promo id
#         df = df.reset_index(drop=True)
#         df['ID'] = 0
#         if df.loc[0, 'Discount on date']==0:
#             df.loc[0, 'ID'] = 0
#         else:
#             df.loc[0, 'ID'] = 1
#         for i in range(1, len(df)):
#             if df.loc[i, 'Discount on date']==df.loc[i-1, 'Discount on date']:
#                 df.loc[i, 'ID']=df.loc[i-1, 'ID']
#             else:
#                 df.loc[i, 'ID']=i
#         df['ID'] = np.where(df['Discount on date']==0, 0, df['ID'])
#         df = df[df['Discount on date']>0]
#         df = df.groupby(['ID'], as_index=False).agg({'M':'first',
#                                                     'Y':'first',
#                                                     'S':'first',
#                                                     'Q':'first',
#                                                     'Discount on date':'max',
#                                                     'Ordered':'sum',
#                                                     'Promo_day_num':'max'})

#         y = df['Ordered']
#         X = df[self.cols]
        
#         if len(X_train)==0:
#             self.empty_flag = True
#         if self.empty_flag == False:
#             self.model.fit(X, y)
#             ### !!!! убрать константу!!!
# #             h=30
# #             self.model.fit(X[:-h], y[:-h],
# #                            eval_metric='mae', 
# #                            eval_set=[(X[-h:], y[-h:])], 
# #                            early_stopping_rounds=1000)
# #             best_iter = self.model.get_booster().best_iteration
# #             self.model = xgb.XGBRegressor(**self.params, n_estimators=max(10, best_iter))
# #             self.model.fit(X, y)

                
#     def predict(self, X):
        
#         X = X.reset_index(drop=True)
#         X['ID'] = 0
#         if X.loc[0, 'Discount on date']==0:
#             X.loc[0, 'ID'] = 0
#         else:
#             X.loc[0, 'ID'] = 1
#         for i in range(1, len(X)):
#             if X.loc[i, 'Discount on date']==X.loc[i-1, 'Discount on date']:
#                 X.loc[i, 'ID']=X.loc[i-1, 'ID']
#             else:
#                 X.loc[i, 'ID']=i
#         X['ID'] = np.where(X['Discount on date']==0, 0, X['ID'])
        
#         df = X.copy()
#         df = df[df['Discount on date']>0]

#         df = df.groupby(['ID'], as_index=False).agg({'M':'first',
#                                                     'Y':'first',
#                                                     'S':'first',
#                                                     'Q':'first',
#                                                     'Discount on date':'max',
#                                                     'Promo_day_num':'max'
#                                                     })

#         pred = self.model.predict(df[self.cols])
#         df['Forecast'] = pred
#         df['Forecast'] = df['Forecast']/df['Promo_day_num']
#         X = pd.merge(X, df[['ID', 'Forecast']], how='left', on=['ID']).fillna(0)
# #         df_copy['FT_SHIP_START'] = df_copy['FT_SHIP_START'].apply(lambda x: str(x)[:10])
# #         df_copy['SHIP_START'] = df_copy['SHIP_START'].astype(str)
#         if self.empty_flag == False:
#             predict = X['Forecast'].values
#         else:
#             predict = []
#         return predict

"""Модели Вани: mlp в двух вариациях"""

### сборка в mols_pool - ничего не меняла, так было!

# params_mlp = {'max_iter': 350, 'hidden_layer_sizes': (50, 50), 'activation': 'relu',
#               'shuffle' :  False, 'n_iter_no_change': 25,'verbose': False}       
# pool_item = ModelPoolItem(model_name='mlp', params=params_mlp, constructor=Custom_MLPRegressor)    
# pool.append(pool_item)

# params_mlp = {'max_iter': 1000, 'hidden_layer_sizes': (900, 100), 'activation': 'logistic',
#               'shuffle' :  False, 'n_iter_no_change': 50,'verbose': False}       
# pool_item = ModelPoolItem(model_name='mlp_hard', params=params_mlp, constructor=Hard_MLPRegressor)    
# pool.append(pool_item)

### сами модели

#from sklearn.neural_network import MLPRegressor
# from sklearn.ensemble import RandomForestRegressor
# from sklearn.tree import DecisionTreeRegressor
# from sklearn.model_selection import GridSearchCV
# from sklearn.decomposition import KernelPCA
# from sklearn.preprocessing import MinMaxScaler
# from sklearn.model_selection import TimeSeriesSplit

# class Custom_MLPRegressor():
#     def __init__(self, **params):
#         self.model = MLPRegressor(**params)
        
#     def fit(self, x, y):
#         x = x[sorted(x.columns)]
#             # for Art's MLP      
#         self.scalerX = MinMaxScaler(feature_range=(0, 1))
#         self.scalery = MinMaxScaler(feature_range=(0, 1))
#         self.scalerX.fit(x)
#         x = pd.DataFrame(self.scalerX.transform(x),columns = x.columns)
#         y = self.scalery.fit_transform(y.values.reshape(-1, 1)).reshape(-1)   
        
#         return self.model.fit(x, y)
        
        
#     def predict(self, x):
#         x = x[sorted(x.columns)]
        
#         x = pd.DataFrame(self.scalerX.transform(x), columns = x.columns)
#         fc = self.model.predict(x).clip(max=1)
# #         return fc
#         return self.scalery.inverse_transform(fc.reshape(-1, 1)).reshape(-1)


# class Hard_MLPRegressor():
#     def __init__(self, **params):
#         self.model = MLPRegressor(**params)
        
#     def fit(self, x, y):
#         x = x[sorted(x.columns)]
#             # for Art's MLP      
#         # self.scalerX = MinMaxScaler(feature_range=(0, 1))
#         # self.scalery = MinMaxScaler(feature_range=(0, 1))
#         # self.scalerX.fit(x)
#         # x = pd.DataFrame(self.scalerX.transform(x),columns = x.columns)
#         # y = self.scalery.fit_transform(y.values.reshape(-1, 1)).reshape(-1)

#         len_data_models = int(len(x) / 2)

#         x_models = x.iloc[:len_data_models]
#         y_models = y.iloc[:len_data_models]

#         x_MLP = x.iloc[len_data_models:].copy()
#         x_for_pred = x[len_data_models:].copy()
#         y_MLP = y.iloc[len_data_models:]

#         self.models_dict = dict(
#             LR = Lasso().fit(x_models, y_models),
#             # DS = DecisionTreeRegressor(max_depth=15).fit(x_models, y_models),
#             KN = KNeighborsRegressor(n_neighbors=15).fit(x_models, y_models),
#             RF = RandomForestRegressor(n_estimators=100, max_depth=15).fit(x_models, y_models),
#             XGBT = xgb.XGBRegressor(booster='gbtree', n_estimators=10, max_depth=5).fit(x_models, y_models),
#             XGBL = xgb.XGBRegressor(booster='gblinear', n_estimators=15).fit(x_models, y_models),
#         )

#         # self.models_dict = dict(
#         #     LR = Lasso().fit(x_models, y_models),
#         #     # DS = DecisionTreeRegressor(max_depth=15).fit(x_models, y_models),
#         #     KN = GridSearchCV(KNeighborsRegressor(),
#         #                       param_grid=dict(n_neighbors=[5, 10, 20, 30],), scoring='explained_variance',
#         #                       cv=TimeSeriesSplit(n_splits=3)).fit(x_models, y_models).best_estimator_,
#         #     RF = GridSearchCV(RandomForestRegressor(),
#         #                       param_grid=dict(n_estimators=[50, 100], max_depth=[5, 7, 10]), scoring='explained_variance',
#         #                       cv=TimeSeriesSplit(n_splits=3)).fit(x_models, y_models).best_estimator_,
#         #     XGBT = GridSearchCV(xgb.XGBRegressor(),
#         #                         param_grid=dict(booster=['gbtree'], n_estimators=[5, 10, 15], max_depth=[3, 5, 7]),
#         #                         scoring='explained_variance',
#         #                         cv=TimeSeriesSplit(n_splits=3)).fit(x_models, y_models).best_estimator_,
#         #     XGBL = GridSearchCV(xgb.XGBRegressor(),
#         #                         param_grid=dict(booster=['gblinear'], n_estimators=[5, 15, 20, 30]),
#         #                         scoring='explained_variance',
#         #                         cv=TimeSeriesSplit(n_splits=3)).fit(x_models, y_models).best_estimator_,
#         # )
        
#         for key in self.models_dict:
#             x_MLP[key] = self.models_dict[key].predict(x_for_pred)

#         for key in self.models_dict:
#             self.models_dict[key] = self.models_dict[key].fit(x, y)

#         self.KPCA = KernelPCA(n_components=2 * int(len(x_MLP.columns)), kernel='poly', degree=5)

#         x_MLP = pd.DataFrame(self.KPCA.fit_transform(x_MLP))#, columns = x_MLP.columns)

#         self.scalerX = MinMaxScaler(feature_range=(0, 1))
#         self.scalery = MinMaxScaler(feature_range=(0, 1))
#         self.scalerX.fit(x_MLP)
#         x_MLP = pd.DataFrame(self.scalerX.transform(x_MLP), columns = x_MLP.columns)
#         y_MLP = self.scalery.fit_transform(y_MLP.values.reshape(-1, 1)).reshape(-1)

#         return self.model.fit(x_MLP, y_MLP)
        
        
#     def predict(self, x):
#         x = x[sorted(x.columns)]

#         x_models = x.copy()

#         for key in self.models_dict:
#             x[key] = self.models_dict[key].predict(x_models)

#         x = pd.DataFrame(self.KPCA.transform(x))#, columns = x.columns)
        
#         x = pd.DataFrame(self.scalerX.transform(x), columns = x.columns)

#         fc = self.model.predict(x).clip(max=1)

# #         return fc
#         return self.scalery.inverse_transform(fc.reshape(-1, 1)).reshape(-1)