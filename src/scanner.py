import logging
import os
import time
import subprocess
# from src import notification

import glob
import shutil
import traceback

INPUT_MQ_FOLDER = os.path.join('..', 'input_mq')
OUTPUT_MQ_FOLDER = os.path.join('..', 'output_mq')

TRIGGER_FILE = r'RdyForImport.csv'
CONFIG_FILE = r'ConfigFile.json'
TMP_FOLDER = os.path.join('..', 'data', 'temp')
IMPORT_COMPLETE_TRIGGER_FILE = 'ImportComplete.csv'


def clean_up_trigger(folder, is_custom_run=False):
    """
    Function remove trigger file from folder
    If is_custom_run = True function remove config file too
    Assumption: all files exists
    :param folder: folder where config file and trigger file placed
    :param is_custom_run: if true config file will be removed
    :return: None
    """
    os.remove(os.path.join(folder, TRIGGER_FILE))
    if is_custom_run:
        os.remove(os.path.join(folder, CONFIG_FILE))


def get_last_mq_folder():
    """
    Function scan mq folder and return last folder
    :return: last folder or None if mq_folder does not contain any folder
    """

    dirs = glob.glob(INPUT_MQ_FOLDER + '/*/')
    if not dirs:
        return None
    last_dir = max(dirs)

    return last_dir


def copy_income_data(folder):
    """
    Function copy all files from folder to ..\data\raw
    If the file existed it will be overwritten.
    :param folder: string - folder name path
    :return: None
    """

    for filename in glob.glob(os.path.join(folder, '*.*')):
        shutil.copy(filename, r'..\data\raw')


def create_import_complete_trigger():
    """
    Function create import complete trigger in the mq folder.
    :return: None
    """
    filename = os.path.join(INPUT_MQ_FOLDER, IMPORT_COMPLETE_TRIGGER_FILE)
    fd = open(filename, 'a')
    fd.close()


def main():
    """
    The main function contains the general logic of scanning files to get the trigger file and run forecasting.
    The function scans the input mq folder every 60 seconds.
    If found trigger or filter file calls run_danone() function.
    :return: None
    """

    logging.info('start up')
    calc_process = None

    while True:

        last_dir = get_last_mq_folder()
        if last_dir is None:
            logging.info("there isn't any folder to scan")
            time.sleep(60)
            continue
        files = os.listdir(last_dir)
        logging.info(f'folder {last_dir} was scanned')
        logging.info(f'found files: {files}')

        if TRIGGER_FILE in files:

            logging.info(f'found trigger file {TRIGGER_FILE}')
            # notification.send_email(msg=f'Got trigger file {file}')

            is_custom_run = CONFIG_FILE in files
            logging.info('start copying files into raw folder')
            copy_income_data(last_dir)
            logging.info('copying is done')

            logging.info('clean up triggers')
            clean_up_trigger(last_dir, is_custom_run)

            logging.info('create import complete trigger')
            create_import_complete_trigger()

            path_to_config_file = os.path.join(r'..\data\raw', CONFIG_FILE)
            chess = bool([f for f in files if f.startswith('chess')])
            if calc_process is None:
                calc_process = run_danone(is_custom_run, path_to_config_file, chess)
                logging.info('calculation process was created')
                continue

            if calc_process.poll() is None:
                logging.info('previous calculation process still working, but new trigger was passed')
                calc_process.terminate()
                logging.info('danone.py was terminated')
            elif calc_process.poll() == 0:
                logging.info('previous calculation was successfully done')
            else:
                logging.info('previous calculation was failed')
            calc_process = run_danone(is_custom_run, path_to_config_file, chess)

        time.sleep(60)


def run_danone(is_custom_run, file_path, chess):
    """
    This function runs forecast calculations in a separate process.
    :param is_custom_run: Bool parameter that defines the type of run we should call.
        If True call forecast process with --fc_type=CUSTOM and --filters_path=[filepath] parameters
    :param file_path: path to filter file. See more details about format in the danone.main() docs.
        If is_custom_run is False parameter file_path doesn't affect.
    :param chess: Bool parameter that defines the type of promo source.
        If False call forecast process with --chess=False parameters
    :return: None
    """
    logging.info(f'run_danone({is_custom_run}, {file_path}):')

    fc_type = 'CUSTOM' if is_custom_run else 'PROD'
    options = [] if fc_type == 'PROD' else ["--fc_type='CUSTOM'", f"--filters_path={file_path}"]
    if not chess:
        options.append('--chess=False')

    logging.info('running danone.py')

    command = ['python', 'danone.py'] + options
    calc_process = subprocess.Popen(command)

    return calc_process


if __name__ == '__main__':

    log_format = '%(asctime)s - %(levelname)s - %(message)s'
    log_name = f'scanner_log.log'
    log_name = os.path.join(os.path.join(r'..', 'logs'), log_name)

    logging.basicConfig(
                        level=logging.INFO,
                        format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S',)

    from logging.handlers import TimedRotatingFileHandler

    log_handler = TimedRotatingFileHandler(when='D', filename=log_name, backupCount=30)
    log_handler.setLevel(logging.INFO)
    log_handler.setFormatter(logging.Formatter(log_format))

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(logging.Formatter(log_format))

    logger = logging.getLogger('')
    logger.addHandler(console)
    logger.addHandler(log_handler)

    try:
        main()
    except BaseException as e:
        logging.fatal(traceback.format_exc())
