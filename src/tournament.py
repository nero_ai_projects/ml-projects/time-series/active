import sys
AUX_PATH = '../src'
if AUX_PATH not in sys.path:
    sys.path.insert(0, AUX_PATH)
import utils
import preprocessing
import models

import pandas as pd
import numpy as np
import warnings


from joblib import Parallel, delayed
from psutil import cpu_count

warnings.filterwarnings('ignore')

def custom_ts_cross_val_score(model, model_name, data_x, data_y, h, cust_metric_data, metric_name='promo', n_folds=3, use_adaptive_fold_length=False):  #cols
    """
    The function implements the cross-validation process for given model and data.
    
    :param model: the object storing the model to fit
    :param model_name: str; the name of the model (ML algorithm)
    :param data_x: pd.DataFrame; whole train features data to be divided by train and validating parts
    :param data_y: pd.DataFrame; whole train target data to be divided by train and validating parts
    :param h: int; the length of forecasting horizon (including all non-train periods if any, e.g. freeze period)
    :param cust_metric_data: pd.DataFrame with columns, that are required to calculate the custom metric by promo slots;
                             for the details see the function fa_promo_slots from utils.py
    :param metric_promo: str; the name of validating metric
    :param n_folds: int; the number of folds to use for cross-validation
    :param use_adaptive_fold_length: if True, use the custom fold length, otherwise use h
    
    :return: list of n_folds tuples; every tuple is of the form (model, model_name, number of fold, train metric, validation metric)
    """
    #TODO: подумать про фриз-период
    if use_adaptive_fold_length:
        # Variable fold length - proportion of history length
        h = int(len(data_x) / (n_folds+1))
    else:
        h=60
    metrics_dict = {'fa':utils.fa, 'promo':utils.fa_promo_slots, 'mae':utils.neg_mae}
    results = []
    cust_metric_cols = cust_metric_data.columns
    for i in range(1, n_folds+1):

        X_train, X_test = data_x[:-h*i],  data_x[-h*i:]
        y_train, y_test = data_y[:-h*i],  data_y[-h*i:]
        #TODO: подумать, что может сломать
        if len(X_train) < h:
            continue
        model.fit(X_train, y_train)
        y_pred = model.predict(data_x)
        #соберем все метрики, которые можем

        metric_data = cust_metric_data.copy()
        metric_data['Ordered'] = data_y
        metric_data['Forecast'] = y_pred
        metric_data = metric_data[['Ordered', 'Forecast'] + list(cust_metric_cols)]  ## важен порядок. TODO: сделать нормально
        tuples_train = [tuple(x) for x in metric_data[:-h*i].to_numpy()]
        tuples_val = [tuple(x) for x in metric_data[-h*i:].to_numpy()]
        metrics = (model,
                   model_name, 
                   i, 
                   metrics_dict[metric_name](tuples_train), 
                   metrics_dict[metric_name](tuples_val))
        results.append(metrics)
    return results


def evaluate_models(models, X_train, y_train, h, cust_metric_data, n_folds=3, use_adaptive_fold_length=False):
    """
    The function applied the cross-validation procedure for all models from the given list with given parameters.
    
    :param models: list of models to be cross-validated
    :param X_train: pd.DataFrame; whole train features data to be divided by train and validating parts
    :param y_train: pd.DataFrame; whole train target data to be divided by train and validating parts
    :param h: int; the length of forecasting horizon (including all non-train periods if any, e.g. freeze period)
    :param cust_metric_data: pd.DataFrame with columns, that are required to calculate the custom metric by promo slots;
                             for the detatils see functions fa_promo_slots from utils.py and custom_ts_cross_val_score from tournament.py
    :param n_folds: int; the number of folds to use for cross-validation
    :param use_adaptive_fold_length: if True, use the custom fold length, otherwise use h
    
    :return: list of lists of n_folds tuples; 
    """
    model_results = []
    for model_name, model in models:    
        results = custom_ts_cross_val_score(model=model,
                                            model_name=model_name,
                                            data_x=X_train,
                                            data_y=y_train,
                                            h=h,
                                            cust_metric_data=cust_metric_data,
                                            n_folds=n_folds,
                                            use_adaptive_fold_length=use_adaptive_fold_length)
        model_results.extend(results)

    return model_results


def prepare_sample(mdf, X_COLUMNS):  #добавить чистку аутлайеров
    """
    The function removes constant columns from input data frame with given columns.
    
    :param mdf: pd.DataFrame
    :param X_COLUMNS: list of columns to use
    
    :return: list; the sublist of X_COLUMNS without those columns, which are constant in mdf
    """
    #TODO: вынести в параметры торнамента, чтобы можно было варьировать от проекта к проекту
    # убиение констант
    df =  mdf[X_COLUMNS]
    cols = df.columns
    idx = [df[col].max()==df[col].min() for col in cols]
    features = [a for a, b in zip(cols, idx) if not b] # определяем только пригодные фичи
    X_COLUMNS = features
    
    return X_COLUMNS

def select_features(mdf, TARGET, X_COLUMNS):
    """
    The function removes the columns from input data frame with given columns, that have a weak correlation with the variable in TARGET.
    
    :param mdf: pd.DataFrame (required column: 'Partition')
    :param TARGET: list with the name of target column
    :param X_COLUMNS: list of columns to use
    
    :return: list; the sublist of X_COLUMNS without those columns, which are weakly correlated with the variable from TARGET list
    """
    X_train = mdf[mdf['Partition']=='train'][X_COLUMNS]
    y_train = mdf[mdf['Partition']=='train'][TARGET[0]]
    corr = []
    for column in X_train.columns:
        column_corr = X_train[column].corr(y_train)
        corr.append(dict(Feature = column, Correlation = column_corr))
    all_features = pd.DataFrame(corr).fillna(0)
    features = all_features[(all_features['Correlation']>=0.15)|(all_features['Correlation']<=-0.10)]['Feature'].tolist()

    if len(features)!=0:
        X_COLUMNS = features  

    return X_COLUMNS
    
def predict_sample(mdf, model, X_COLUMNS, TARGET, h):# params,
    """
    The function adds forecast columns by applying specified model to the data frame with specified columns, target and length of horizon.
    Added column: 'Forecast'.
    
    :param mdf: pd.DataFrame
    :param model: model to make a forecast
    :param X_COLUMNS: list of columns to use
    :param TARGET: list with the name of target column
    :param h: int; the length of forecasting horizon (including all non-train periods if any, e.g. freeze period)
    
    :return: pd.DataFrame with added column
    """
    features = X_COLUMNS
    X = mdf[features]
    y = mdf[TARGET]
#     X = prepare_sample(mdf)
    
    X_train, X_test = X[:-h], X[-h:]
    y_train, y_test = y[:-h], y[-h:]

    model.fit(X_train, y_train)
    mdf['Forecast'] = np.maximum(0, model.predict(X))

    return mdf.reset_index(drop=True)


def drop_zeros_features_on_fc(mdf, x_columns, check_cols=['WD', 'ESP']):
    for col in check_cols:
        if col in mdf.columns and col in x_columns:
            data_test = mdf[(mdf['Partition'] == 'fc') & (mdf[col] >= 0.01)]
            if data_test.empty:
                x_columns.remove(col)
    return x_columns


def drop_esp_if_not_for_all_fc_promo(mdf, x_columns, check_cols=['ESP']):
    for col in check_cols:
        if col in mdf.columns and col in x_columns:
            fc_esp_promo = mdf[(mdf['Partition'] == 'fc')].groupby('')
            if data_test.empty:
                x_columns.remove(col)
    return x_columns


def eval_predict_sample(mdf, x_columns, cust_metric_sols, target, use_adaptive_fold_length, chain_by_tbt, calendar_features, inno_flag):  # X_EXT_COLUMNS
    """
    The function does all the actions to make a forecast and add the column with it to the input data frame.
    
    :param mdf: pd.DataFrame (required columns: 'key', 'Partition', 'Profile')
    :param x_columns: list of columns to use
    :param cust_metric_sols: list of columns, that are required to calculate the custom metric by promo slots 
                             for the detatils see functions fa_promo_slots from utils.py and custom_ts_cross_val_score from tournament.py
    :param target: str; name of target variable
    :param use_adaptive_fold_length: if True, use the custom fold length
    
    :return: (pd.DataFrame, pd.DataFrame); first data frame contains the feature with result of forecast,
                                           second data frame contains the information of all evaluated models (facts, forecasts and metrics)
    """
    mdf = preprocessing.add_features_in_sample(mdf)

    if inno_flag:
        res = mdf
        res['Forecast'] = 0.
        eval_results = pd.DataFrame(columns=['Model_name', 'key', 'Model', 'Val', 'Val_std', 'metric', 'Best'])
    else:
        # TODO: to fix actuals data and modify
        mdf_payt = chain_by_tbt['Chain'][mdf['key'].str[5:][0]] == 'PYATEROCHKA'
        if mdf_payt:
            orig_ordered = mdf['Ordered'].values
            mdf = utils.recover_promo_shipments(mdf)

        # TODO: prepare sample делать до eval models
        h = len(mdf[mdf['Partition'] != 'train'])
        key = mdf['key'].values[0]
        x_columns = prepare_sample(mdf, x_columns)
        x_columns = drop_zeros_features_on_fc(mdf, x_columns, check_cols=['WD', 'ESP'])
        x_columns = select_features(mdf, target, list(set(x_columns) - set(calendar_features))) + calendar_features

        X = mdf[x_columns]  ##для проброса в кастомную метрику
        #X = utils.transform_feautures_selector(X)
        #X = utils.sin_cos_transform(X)
        y = mdf[target]
        cust_metric_data = mdf[cust_metric_sols]

        X_train, X_test = X[:-h], X[-h:]
        y_train, y_test = y[:-h], y[-h:]
        models_list = models.init_models()
        eval_results = evaluate_models(models_list, X_train, y_train, h, cust_metric_data[:-h],
                                       use_adaptive_fold_length=use_adaptive_fold_length)

    #     if len(eval_results)==0:
    #         return (pd.DataFrame(columns=list(mdf.columns) + ['Forecast']),
    #                     pd.DataFrame(columns=['Model_name', 'key', 'Model', 'Val', 'Val_std', 'metric']))
        if len(eval_results)==0:
            res = mdf
            res['Forecast'] = res['Profile']
            eval_results = pd.DataFrame(columns=['Model_name', 'key', 'Model', 'Val', 'Val_std', 'metric', 'Best'])
            return res, eval_results
        eval_results = pd.DataFrame(eval_results, columns=['Model', 'Model_name', 'Fold', 'Train', 'Val'])
        eval_results['key']=key
        eval_results['Val_std'] = eval_results['Val']
        eval_results = eval_results.groupby(['Model_name', 'key'], as_index=False).agg({'Model':'first',
                                                                                  'Val':'mean', 'Val_std':'std'})
        eval_results['Val_std'] = eval_results['Val_std'].fillna(0.)
        eval_results['metric'] = eval_results['Val'] - eval_results['Val_std']
        best_model = eval_results.sort_values(by='metric').tail(1)['Model'].values[0]
        eval_results['Best'] = np.where(eval_results['Model']==best_model, 1, 0)
    #     best_score = max(eval_results.keys())  #переписать для датафрейма!!!

    #     best_model_name, best_model = eval_results[best_score]  ## переписать для датафрейма! отсюда нужен только best model

        res = predict_sample(mdf, best_model, x_columns, target, h)
    #     eval_results['Promo_metric']=utils.metrics_promo(res[res['Partition']=='fc'])['Acc'][0]

    # TODO: to fix again actuals data and modify
        if mdf_payt:
            res['Ordered'] = orig_ordered
    
    return res, eval_results


def run_tournament(cust_metric_columns, data, target, x_columns, calendar_features, inno_keys):
    """
    Function run forecasting process using Parallel.
    Tournament predicts every key separately.
    Outcomes are files with results (result_file_path) and evaluation data (evals_file_path).
    :param cust_metric_columns:
    :param data: pandas DataFrame, that contains data
    :param target: name of column that contains target variable
    :param x_columns: list of column names that will be used as features
    :return: None
    """
    adaptive_keys = utils.get_adaptive_keys(data)
    cust_plan_feature_keys = utils.get_use_cust_plan_as_feature(data)
    x_columns_no_custplan = list(x_columns)
    if 'cust_plan' in x_columns_no_custplan:
        x_columns_no_custplan.remove('cust_plan')

    # print(cust_plan_feature_keys)
    
    chain_by_tbt = pd.read_csv('../data/masterdata/Customer_MD_with_Lenta_mapping.csv', 
                                    encoding='windows-1251', sep=';', 
                                    dtype={'Techbillto':'str', 'Chain':'str'}, 
                                    usecols=['Techbillto', 
                                            'Chain']).drop_duplicates(subset='Techbillto').dropna().set_index('Techbillto')
    
    tasks = ((mdf, 
              x_columns if key in cust_plan_feature_keys else x_columns_no_custplan, 
              cust_metric_columns, 
              target, 
              key in adaptive_keys,
              chain_by_tbt, 
              calendar_features,
              True if key in inno_keys else False) for key, mdf in data.groupby('key'))
    res = Parallel(n_jobs=cpu_count(), verbose=1)(delayed(eval_predict_sample)(*task) for task in tasks)
    # res = Parallel(n_jobs=1, verbose=1)(delayed(eval_predict_sample)(*task) for task in tasks)

    # from tqdm.auto import tqdm
    # try:
    #     for task in tqdm(tasks):
    #         res = eval_predict_sample(*task)
    # except:
    #     for i in task:
    #         print(i)
    #     raise

    a, b = zip(*res)
    results = pd.concat(a)
    evals = pd.concat(b)
    
    cust_plan_feature_keys = list(cust_plan_feature_keys)
    if cust_plan_feature_keys:
        tasks = ((mdf, 
                  x_columns_no_custplan, 
                  cust_metric_columns, 
                  target, 
                  key in adaptive_keys,
                  chain_by_tbt,
                  calendar_features,
                  True if key in inno_keys else False) for key, mdf in data[data['key'].isin(cust_plan_feature_keys)].groupby('key'))
        res = Parallel(n_jobs=cpu_count(), verbose=0)(delayed(eval_predict_sample)(*task) for task in tasks)
        # res = Parallel(n_jobs=1, verbose=0)(delayed(eval_predict_sample)(*task) for task in tasks)
        a, b = zip(*res)
        results_for_cust_keys = pd.concat(a)
        evals_for_cust_keys = pd.concat(b)
        
        res = results[~results['key'].isin(cust_plan_feature_keys)]

        diff_res = []
        for key, mdf in results[results['key'].isin(cust_plan_feature_keys)].groupby('key'):
            mdf['Date'] = pd.to_datetime(mdf['Date'], format='%Y-%m-%d')
            max_date_order = mdf.loc[mdf['cust_plan'] > 0, 'Date'].max()
            mdf.loc[mdf['Date'] >= max_date_order, 
                    'Forecast'] = results_for_cust_keys.loc[(results_for_cust_keys['key'] == key) &
                                                            (results_for_cust_keys['Date'] >= max_date_order), 
                                                            'Forecast']
            diff_res.append(mdf)
        
        diff_res = pd.concat(diff_res)

        assert results.shape == res.append(diff_res).shape
        results = res.append(diff_res)

        results['Date'] = results['Date'].astype(str)

    if 'cust_plan' in results.columns:
        cust_plan_forecast_keys = utils.get_use_cust_plan_as_forecast(data)
        results['Forecast'] = np.where(((results['key'].isin(cust_plan_forecast_keys)) & (results['cust_plan'] != 0)), results['cust_plan'], results['Forecast'])

    # TODO: Ask @Nat: Do we really use all columns in the results?
    return results, evals

