import pandas as pd
import numpy as np
import math
import os
import datetime as dt

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler

from scanner import OUTPUT_MQ_FOLDER

import warnings
warnings.filterwarnings('ignore')


def find_anomalies_db(data, time_cols=['Date'], scl=StandardScaler(), train_only=True):
    data['Date'] = pd.to_datetime(data['Date'], format='%Y-%m-%d')
    now_str = dt.datetime.now().strftime('%Y%m%d%H%M')

    report = []
    report_to_file = []

    testing_columns = ['Ordered', 'Discount on date', 'Reg_price', 'Promo_price', 'cust_plan', 'WD']

    data_group = data.loc[data['Partition'] == 'train',
                          time_cols + testing_columns].groupby('Date', as_index=False).agg({'Ordered': 'sum',
                                                                                            'Discount on date': 'mean',
                                                                                            'Reg_price': 'sum',
                                                                                            'Promo_price': 'sum',
                                                                                            'cust_plan': 'sum',
                                                                                            'WD': 'mean'})
    pp = PdfPages(os.path.join(OUTPUT_MQ_FOLDER, 'Results_data_tests', f'test_results_{now_str}.pdf'))

    #     Поиск нулевых значений для каждого признака с помощью DBSCAN
    for feat in data_group.columns:

        if feat not in time_cols and feat in testing_columns:

            #             Поиск фичей, все значения в которых Nan
            if math.isnan(data[feat].mean()):
                report.append([feat, 'Was detected as Nan'])

            #             Разбиение с помощью DBSCAN
            data_group[f'{feat}_add'] = np.where(data_group[feat] > 0, 1, 0)
            model = DBSCAN(eps=0.15, min_samples=4).fit(scl.fit_transform(data_group[[f'{feat}_add']]))

            #             Поиск лэйбла для кластера с нулевыми значениями
            data_group['label'] = model.labels_
            data_clust = data_group[[feat, 'label']].groupby('label', as_index=False).agg('max')

            label = data_clust[data_clust[feat] <= 0]['label']
            data_group.drop(columns=[f'{feat}_add', 'label'], inplace=True)

            #             Для фичей, в которых обнаружены нули/outliers, вывести значения с датой (для проверки)
            if label.shape[0]:
                outliers = data_group[model.labels_ == label.values][['Date', feat]]
                if label.values == -1:
                    message = f'Was found {outliers.shape[0]} outliers'
                    report.append([feat, message])
                else:
                    message = f'Was found {outliers.shape[0]} zero values'
                    report.append([feat, message])

                fig = plt.figure(figsize=(16, 8), dpi=80)
                plt.scatter(data_group['Date'], data_group[feat], c=model.labels_)
                plt.title(feat)

                pp.savefig(fig)

                for i, row in outliers.iterrows():
                    report_to_file.append([feat, message, row['Date'], row[feat]])
    pp.close()

    #      Отчёт по фичам на экран, подробный отчет со значениями - в файл
    report = pd.DataFrame(report, columns=['Feature', 'Warning'])
    report_to_file = pd.DataFrame(report_to_file, columns=['Feature', 'Warning', 'Date', 'Value'])
    report_to_file.to_csv(os.path.join(OUTPUT_MQ_FOLDER, 'Results_data_tests', f'anomalies_{now_str}.txt'), sep='\t')