import pandas as pd
import numpy as np
import datetime as dt
import glob

from collections import defaultdict

import utils


def get_ft_version_from_filename(name: str):
    """
    Extracts fintool version as date from filename
    :param name: fintool's filename
    :return: version as timestamp
    """

    # Uplifttool_AFTSnapshot_200921.xlsx

    version = pd.to_datetime(name[-11: -5])
    return version




@utils.gc_collect
def fintool_to_csv():
    """
    Will be deprecated
    """
    ## ВАЖНО! Были внесены правки в исходные файлы. корректная версия файлов у Наташи
    fc_1_ex = pd.read_excel('../data/raw/Promo Plan 2018.xlsx')
    fc_1_ex.rename(columns={'Версия':'Versions'}, inplace=True)
    fc_2_ex = pd.read_excel('../data/raw/Promo Plan.xlsx', dtype={'Chain':'str'})

    path_to_promo = glob.glob('../data/raw/Promo plan 2020/*.xlsx')
    tmp = []
    for name in path_to_promo:
        df = pd.read_excel(name, skiprows=6)
        df.rename(columns={'date': 'Versions'}, inplace=True)
        tmp.append(df)
        
    promo_2020 = pd.concat(tmp)

    cols = ['Versions',
            'Index',
            '# of promo',
            'customer code',
            'SKU',
            'Format',
            'Trading Terms off-inv',
            'Trading Terms on-inv',
            'Local Brands flag',
            'Promo type',
            'Store Starts',
            'Store Ends',
            '# of days promo',
            'Days before promo start',
            'Days shipment',
            'Shipment start date',
            'Shipment end date',
            'Off-invoice',
            'On-invoice',
            'KAM Volume with uplift',
            'POSM',
            'Catalogue']


    fc_1 = fc_1_ex[cols]
    fc_2 = fc_2_ex[cols]
    promo_2020 = promo_2020[cols]

    fc = pd.concat([fc_1, fc_2, promo_2020])
    fc['Total Discount'] = fc['Off-invoice'] + fc['On-invoice']
    fc['KAM Volume with uplift'] = fc['KAM Volume with uplift'].replace(' ', 0)
    fc['KAM Volume with uplift'] = fc['KAM Volume with uplift'].astype(float)
    fc['customer code'] = fc['customer code'].astype(str)
    fc.to_csv('../data/processed/fintool.csv', index=False)
    

def get_short_fintool_auto_verions(freeze_start=None):

    cols = ['customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift',
            'Total Discount', 'wd', 'esp'] + ['Ship_start_year', 'Ship_start_month', 'Versions']
    fintool = get_processed_fintool(use_columns=cols)

    # Ivan's idea
    fintool['Shipment start date'] =pd.to_datetime(fintool['Shipment start date'], format='%Y-%m-%d')
    fintool['Versions'] =pd.to_datetime(fintool['Versions'], format='%Y-%m-%d')
    fintool['Y_promo'] = fintool['Shipment start date'].dt.year
    fintool['Q_promo'] = (fintool['Shipment start date'].dt.month - 1) // 3 + 1
    Y_and_Q_promo = fintool[['Y_promo', 'Q_promo']].drop_duplicates().sort_values(['Y_promo', 'Q_promo'])

    if freeze_start is not None:
        freeze_start = pd.to_datetime(freeze_start, format="%Y%m%d")
        freeze_start_year = freeze_start.year
        freeze_start_quarter = (freeze_start.month - 1) // 3 + 1

        lag = pd.Timedelta(days=-1000)
        versions_for_fc = None

        for version in fintool['Versions'].unique():
            if (version - freeze_start <= pd.Timedelta(days=0)) and (version - freeze_start > lag):
                lag = version - freeze_start
                versions_for_fc = version

        if versions_for_fc is None:
            print("not version fintoll for fc")
            raise

        short_fintoll = []

        for Y, Q in zip(Y_and_Q_promo['Y_promo'], Y_and_Q_promo['Q_promo']):
            if ((Y < freeze_start_year) or \
                (Y == freeze_start_year and Q < freeze_start_quarter)):
                max_versions = fintool.loc[
                    (fintool['Y_promo'] == Y) & (fintool['Q_promo'] == Q) & 
                    (fintool['Versions'].dt.year == Y) &
                    ((fintool['Versions'].dt.month - 1) // 3 + 1 == Q), \
                    'Versions'].max()
            else:
                max_versions = versions_for_fc

            if pd.isnull(max_versions):
                max_versions = fintool.loc[(fintool['Y_promo'] == Y) & 
                                           (fintool['Q_promo'] == Q), \
                                            'Versions'].max()

            short_fintoll.append(
                fintool[(fintool['Y_promo'] == Y) & 
                        (fintool['Q_promo'] == Q) & 
                        (fintool['Versions'] == max_versions)])

    else:
        short_fintoll = []

        for Y, Q in zip(Y_and_Q_promo['Y_promo'], Y_and_Q_promo['Q_promo']):
            max_versions = fintool.loc[
                    (fintool['Y_promo'] == Y) & (fintool['Q_promo'] == Q) & (fintool['Versions'].dt.year == Y) &
                    ((fintool['Versions'].dt.month - 1) // 3 + 1 == Q), 'Versions'].max()        

            if pd.isnull(max_versions):
                max_versions = fintool.loc[(fintool['Y_promo'] == Y) & (fintool['Q_promo'] == Q), 'Versions'].max()

            short_fintoll.append(
                fintool[(fintool['Y_promo'] == Y) & (fintool['Q_promo'] == Q) & (fintool['Versions'] == max_versions)])

    short_fintoll = pd.concat(short_fintoll)
    del short_fintoll['Y_promo']
    del short_fintoll['Q_promo']

    return short_fintoll


def get_short_fintool(versions_mapping, auto_versions: bool = False, freeze_start=None):
    """
    The function transforms processed fintool (the information about fintool from fintool.csv) according to the correspondence
    from versions_mapping data frame. For more details follow the comments in the code below (comments start with '#').

    :param versions_mapping: pd.DataFrame with columns 'Month', 'Year', 'ft_version' (information about fintool file versions)
    :param auto_versions: Boolean. If True versions_mapping will be ignored. Versions will be detected automatically.
    :return: pd.DataFrame
    """
    if auto_versions:
        fintool = get_short_fintool_auto_verions(freeze_start=freeze_start)
    else:
        cols = ['customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift',
                'Total Discount', 'wd', 'esp'] + ['Ship_start_year', 'Ship_start_month', 'Versions']
        fintool = get_processed_fintool(use_columns=cols)
        versions_mapping['ft_version'] = pd.to_datetime(versions_mapping['ft_version'], format='%d.%m.%Y')
        fintool = pd.merge(fintool, versions_mapping, how='inner', left_on = ['Ship_start_year', 'Ship_start_month', 'Versions'], right_on = ['Year', 'Month', 'ft_version'])

    ## TODO: добавить фичи с расстоянием между отгрузкой и полкой (см. колонки финтула)
    # fintool = fintool[['customer code', 'SKU', 'Shipment start date', 'Shipment end date', 'KAM Volume with uplift', 'Total Discount']]
    for col in ['Ship_start_year', 'Ship_start_month', 'Versions']:
        del fintool[col]

    fintool = fintool.rename(columns={'customer code':'CUSTOMER_CODE',
                                      'SKU':'PRODUCT', 
                                      'Shipment start date':'SHIP_START',
                                      'Shipment end date':'SHIP_END',
                                      'KAM Volume with uplift':'KAM_FC'})
    #мэппинг ленты (догадка о том, что он есть)
    #     fintool['CUSTOMER_CODE'] = fintool['CUSTOMER_CODE'].map(utils.get_lenta_mapping())

    fintool['CUSTOMER_CODE_LENTA'] = fintool['CUSTOMER_CODE'].map(utils.get_lenta_mapping())
    fintool['CUSTOMER_CODE'] = np.where(fintool['CUSTOMER_CODE_LENTA'].notnull(), 
                                     fintool['CUSTOMER_CODE_LENTA'], fintool['CUSTOMER_CODE'])
    fintool.drop(columns=['CUSTOMER_CODE_LENTA'], inplace=True)

    agg_rule = {'Total Discount': 'max', 'KAM_FC': 'sum', 'wd': 'max', 'esp': 'max'}
    fintool = fintool.groupby(['CUSTOMER_CODE', 'PRODUCT', 'SHIP_START', 'SHIP_END'], as_index=False).agg(agg_rule)
    #собираю правильный конфиг кастомерки с учетом кастомер кодов, которые обозначают сеть целиком
    file = pd.read_csv('../data/masterdata/Customer_MD.csv', 
                      sep=';', 
                      encoding='windows-1251', dtype={'Techbillto':'str', 'customer code':'str'}, 
                      usecols=['Techbillto', 'customer code', 'Chain', 'customer name'])
    file = file.rename(columns={'Techbillto':'TECH_BILL_TO', 'customer code':'CUSTOMER_CODE'})
    tbt = file.dropna()
    totals = file[file['TECH_BILL_TO'].isna()]
    totals.drop(columns=['TECH_BILL_TO'], inplace=True)
    totals = pd.merge(tbt[['TECH_BILL_TO', 'Chain']], totals, how='left', on='Chain').dropna()
    cust_hier = pd.concat([tbt, totals])

    #распячиваю на все техбиллту и добавляю продуктовую
    fintool = pd.merge(cust_hier, fintool, how='left', on='CUSTOMER_CODE')
    fintool = fintool.dropna()
    tmp = pd.read_csv('../data/masterdata/Product_MD.csv', sep=';', encoding='windows-1251', dtype={'SKU':'str', 
                                                                                                    'PRODUCT_CODE':'str'})
    tmp = tmp[['Sku/линейка', 'PRODUCT_CODE']].drop_duplicates().rename(columns={'Sku/линейка':'PRODUCT'})
    fintool = pd.merge(fintool, tmp, how='left', on='PRODUCT')
    fintool = fintool.dropna()
    # добавляю ключ и привожу к привычному формату дат
    fintool['KEY'] = fintool['PRODUCT_CODE'] + '_' + fintool['TECH_BILL_TO']
    fintool = fintool.drop(columns=['CUSTOMER_CODE', 'PRODUCT', 'PRODUCT_CODE', 'TECH_BILL_TO'])
    fintool['SHIP_START'] = pd.to_datetime(fintool['SHIP_START'], format='%Y-%m-%d')
    fintool['SHIP_END'] = pd.to_datetime(fintool['SHIP_END'], format='%Y-%m-%d')
    return fintool


def get_processed_fintool(use_columns=None):
    """
    The function collects the information from the file fintool.csv.

    :use_columns: the columns to choose from the file

    :return: pd.DataFrame
    """
    fintool = pd.read_csv('../data/processed/fintool.csv', encoding='windows-1251', usecols=use_columns)

    fintool['Ship_start_month'] = pd.to_datetime(fintool['Shipment start date'], format='%Y-%m-%d').dt.month
    fintool['Ship_start_year'] = pd.to_datetime(fintool['Shipment start date'], format='%Y-%m-%d').dt.year
    fintool['Versions'] = pd.to_datetime(fintool['Versions'], format='%Y-%m-%d')

    return fintool


def write_fintool_by_dates(versions_dict, auto_version=False, freeze_start=None):
    """
    Converts fintool dataframe into new format.
    :param versions_dict: 
    :return: 
    """
    
    #определяем, нужна ли м-1 сборка
    if '-' in list(versions_dict.keys())[0]:
        monthly_flag = True
    else:
        monthly_flag = False
    #собираем датафрейм мэппинга старта слота на версию финтула
    if '-' in list(versions_dict.keys())[0]:  #M-1 option
        versions_df = pd.DataFrame.from_dict(versions_dict, orient='index')
        versions_df.reset_index(inplace=True)
        versions_df.rename(columns={'index':'Month', 0:'ft_version'}, inplace=True)
        versions_df['Year'] = versions_df['Month'].str[:4].astype(int)
        versions_df['Month'] = versions_df['Month'].str[5:].astype(int)
    elif 'Q' in list(versions_dict.keys())[0]:  #Quarterly
        versions_df = pd.DataFrame.from_dict(versions_dict, orient='index')
        tmp_list = []
        for i in range(len(versions_df)):  #tmp.index.str[-1:]:
            mdf = pd.DataFrame(columns = ['Q', 'Month', 'Year'])
            q = int(versions_df.index[i][-1:])
            mdf['Month'] = [3*q-2, 3*q-1, 3*q-0]
        #     mdf['M'] = mdf['M'].apply(lambda x: f"{x:02d}")  ##просто на всякий случай пусть тут полежит
            mdf['Q'] = versions_df.index[i]
            mdf['Year'] = int(versions_df.index[i][:-2])
            tmp_list.append(mdf)
        month_df = pd.concat(tmp_list)
        versions_df.reset_index(inplace=True)
        versions_df.rename(columns={'index':'Q', 0:'ft_version'}, inplace=True)
        versions_df = pd.merge(month_df, versions_df, how='left', on=['Q'])
        versions_df.drop(columns=['Q'], inplace=True)
    else:
        print('Wrong versions mapping')
        raise    

    #разворачивам финтулы по времени
    fintool = get_short_fintool(versions_df, auto_versions=auto_version, 
                                freeze_start=freeze_start)

    fintool.drop(columns=['KAM_FC'], inplace=True)
    fintool.rename(columns={'Total Discount':'DISCOUNT'}, inplace=True)

    ft_dict = defaultdict(list)
    for row in fintool.itertuples(index=False):
        dates = pd.date_range(start=row.SHIP_START, end=row.SHIP_END)
        n = len(dates)
        ft_dict['PGI_DATE'].extend(dates)
        ft_dict['FT_Promo_day_num'].extend(np.arange(1, n+1))
        ft_dict['FT_Promo_day_num_backward'].extend(np.arange(1, n+1)[::-1])
        ft_dict['Total Discount'].extend([row.DISCOUNT] * n)
        ft_dict['KEY'].extend([row.KEY] * n)
        ft_dict['SHIP_START'].extend([row.SHIP_START] * n)
        ft_dict['wd'].extend([row.wd] * n)
        ft_dict['esp'].extend([row.esp] * n)
    full_ft = pd.DataFrame.from_dict(ft_dict)

    full_ft.to_csv(f'../data/processed/fintool_by_dates{"_M-1" if monthly_flag else ""}.csv', index=False)


def load_customer_plans(location_to_save='../data/processed/Заказы от сетей/customer_plans.csv'):
    '''
    Loads customer plans from Excel files to a single DataFrame with defined columns and saves it in csv file
    
    Tuned for Dixy / X5 / Tander files (in first 20 lines of code) - as their format is quite different from each other
    Writes a DataFrame with customer plans in a format:
    ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']
    to a location_to_save
    Runtime ~43s
    
    :PARAMETERS:
    location_to_save: str - target location/name of output file
    
    :OUTPUT:
    None, except the saved preprocessed file
    
    :NOTES:
    - Fit ONLY for the existing files (Dixy / X5 / Tander), in case they change (name and/or format) - this function will require updating. Can be avoided if all new data will be still in same files for 2020
    - customer forecast is saved in Pieces ('Объем, шт'), as most of customers provide their forecast in this unit of measure. Conversion to kg and split by days / Techbilltos is done when this data is actually appended to the main history/forecast DataFrame (features.add_customer_plans())
    '''

    # Master Data files
    product_md_file = '../data/masterdata/Product_MD.csv'
    mdm_cust_file = '../data/masterdata/MDM_CUSTOMERS.csv'
    
    # plans from chains
    chain_forecast_file_dixy = '../data/raw/Заказы от сетей/2020_План Дикси.xlsx'
    chain_forecast_file_X5 = '../data/raw/Заказы от сетей/Объемы 2020_Пятерочка_Перекресток.xlsx'
    X5_5ka_sheet_name = 'Пятерочка'
    X5_X_sheet_name = 'Перекресток'
    X5_Karusel_sheet_name = 'Карусель'
    chain_forecast_file_X5_old = '../data/raw/Заказы от сетей/Перекресток план.xlsx'
    chain_forecast_file_dixy_old = '../data/raw/Заказы от сетей/План Дикси.xlsx'
    chain_forecast_file_X5_5ka_old = '../data/raw/Заказы от сетей/Пятерочка промо план.xlsx'
    chain_forecast_file_Tander_new = '../data/raw/Заказы от сетей/РЦ Тандер Промо_2020.xlsx'
    chain_forecast_file_Tander_new_worksheet = 'Заказы'
    chain_forecast_file_Tander = '../data/raw/Заказы от сетей/РЦ Тандер промо план.xlsx'
    
    # load Master Data
    product_md = pd.read_csv(product_md_file, sep=';', decimal=',', encoding='windows-1251', usecols=[0, 13, 34])
    mdm_cust = pd.read_csv(mdm_cust_file, sep=';', decimal=',', encoding='windows-1251', usecols=[0, 2])

    # all plans data
    customer_plans = []
    
    # --- DIXY ---
    plan = get_customer_plan_dixy(chain_forecast_file_dixy, product_md)
    customer_plans.append(plan)
    
    # --- X5_5ka ---
    plan = get_customer_plan_5(X5_5ka_sheet_name, chain_forecast_file_X5, product_md)
    customer_plans.append(plan)
    
    # --- X5_X --- (same code except sheet name)
    plan = get_customer_plan_perekrestok(X5_X_sheet_name, chain_forecast_file_X5, product_md)
    customer_plans.append(plan)
    
    # --- X5_Karusel ---
    plan = pd.read_excel(chain_forecast_file_X5, sheet_name=X5_Karusel_sheet_name)
    plan.drop(columns=['Неделя', 'Скидка'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan['SAPCode'] = plan['Sku'].str.split(' ').str[0]
    plan = plan[plan['SAPCode'].apply(lambda x: x.isnumeric())]
    plan['SAPCode'] = plan['SAPCode'].astype('int')
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode', right_on='SKU') # product code from Product_MD
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan['Region'] = plan['Регион'] + ' ' + plan['Формат']
    plan['Chain'] = 'KARUSEL'
    plan['Techbillto'] = ''
    plan = plan[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], as_index=False)['Объем, шт'].sum()
    customer_plans.append(plan)
    
    # --- X5_X_old ---
    plan = pd.read_excel(chain_forecast_file_X5_old)
    plan['Region'] = plan['customer name']
    plan.drop(columns=['customer name', 'Неделя', 'Бренд', 'Линейка', 'PLU', 'Sku', 'Скидка', 'год', 'Регион', 'Формат'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode', right_on='SKU') # product code from Product_MD
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan['Chain'] = 'PEREKRESTOK'
    plan['Techbillto'] = ''
    plan = plan[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], as_index=False)['Объем, шт'].sum()
    customer_plans.append(plan)
    
    # --- DIXY_old ---
    plan = pd.read_excel(chain_forecast_file_dixy_old)
    plan.drop(columns=['Год', 'ЦФО (Москва, Центр), СЗФО (СПб)', 'Наименование продукта'], inplace=True)
    plan.rename(columns={'Объемы, шт': 'Объем, шт', 'Код SAP Данон': 'SAPCode'}, inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode', right_on='SKU') # product code from Product_MD
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan.rename(columns={'techbillto':'Techbillto'}, inplace=True)
    plan['Chain'] = 'DIXY'
    plan['Region'] = ''
    plan = plan[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], as_index=False)['Объем, шт'].sum()
    customer_plans.append(plan)
    
    # --- X5_5ka_old ---
    plan = pd.read_excel(chain_forecast_file_X5_5ka_old)
    plan.rename(columns={'customer name (BluePlanner)': 'Region'}, inplace=True)
    plan.drop(columns=['Неделя', 'Бренд', 'Линейка', 'PLU', 'Sku', 'Скидка', 'год'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode', right_on='SKU') # product code from Product_MD
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan['Techbillto'] = ''
    plan['Chain'] = 'PYATEROCHKA'
    plan = plan[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], as_index=False)['Объем, шт'].sum()
    customer_plans.append(plan)
    
    # --- Tander-2020 ---
    plan = pd.read_excel(chain_forecast_file_Tander_new, sheet_name=chain_forecast_file_Tander_new_worksheet)
    plan.drop(columns=['РЦ'], inplace=True)
    plan.rename(columns={'SKU': 'SAPCode', 'Shipment_Start': 'Shipment Start', 'Shipment_End': 'Shipment End', 'Store_Start': 'Store Start', 'Store_End': 'Store End'}, inplace=True)
    plan = plan.merge(product_md, left_on='SAPCode', right_on='SKU') # product code from Product_MD
    plan['BP_WnitWeight'] = plan['BP_WnitWeight'].astype('float')
    plan['Объем, шт'] = (plan['Total'] / plan['BP_WnitWeight']) * 1000
    plan = plan.merge(mdm_cust, how='left', left_on='Ship_To', right_on='ShipToID')
    plan.rename(columns={'TechToID': 'Techbillto'}, inplace=True)
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan['Chain'] = 'MAGNIT'
    plan['Region'] = ''
    plan = plan[['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region', 'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], as_index=False)['Объем, шт'].sum()
    customer_plans.append(plan)
    
    # --- Tander ---
    plan = get_customer_plan_tander(chain_forecast_file_Tander, product_md)
    customer_plans.append(plan)
    
    # Concatenate all customer plans in a single DataFrame
    customer_plans = pd.concat(customer_plans, axis=0, ignore_index=True)
    # Remove duplicates
    customer_plans.drop_duplicates(subset=['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'], keep='first', inplace=True)
    
    customer_plans.to_csv(location_to_save, index=False, encoding='windows-1251')


def get_customer_plan_perekrestok(X5_X_sheet_name, chain_forecast_file_X5, product_md):
    """
    The function collects the promo plan from specified sheet of raw file with specified path.
    Then processes it using masterdata file Product_MD.csv.

    :param X5_X_sheet_name: str; name of sheet in file with specified path
    :param chain_forecast_file_X5: str; path of file with promo plan of several chains
    :param product_md: pd.DataFrame; data frame with required information from masterdata file Product_MD.csv

    :return: pd.DataFrame; processed data frame with information about promo for Chain='PEREKRESTOK'
    """
    plan = pd.read_excel(chain_forecast_file_X5, sheet_name=X5_X_sheet_name)
    plan.drop(columns=['Неделя', 'Sku', 'Скидка'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode',
                      right_on='SKU')  # product code from Product_MD
    plan['Region'] = plan['Регион'] + ' ' + plan['Формат']
    plan['Chain'] = 'PEREKRESTOK'
    plan['Techbillto'] = ''
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan = plan[
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region',
         'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'],
        as_index=False)['Объем, шт'].sum()
    return plan


def get_customer_plan_5(X5_5ka_sheet_name, chain_forecast_file_X5, product_md):
    """
    The function collects the promo plan from specified sheet of raw file with specified path.
    Then processes it using masterdata file Product_MD.csv.

    :param X5_5ka_sheet_name: str; name of sheet in file with specified path
    :param chain_forecast_file_X5: str; path of file with promo plan of several chains
    :param product_md: pd.DataFrame; data frame with required information from masterdata file Product_MD.csv

    :return: pd.DataFrame; processed data frame with information about promo for Chain='PYATEROCHKA'
    """
    plan = pd.read_excel(chain_forecast_file_X5, sheet_name=X5_5ka_sheet_name)
    plan.drop(columns=['Неделя', 'Sku', 'Скидка'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode',
                      right_on='SKU')  # product code from Product_MD
    plan['Region'] = plan['Регион'] + ' ' + plan['Формат']
    plan['Chain'] = 'PYATEROCHKA'
    plan['Techbillto'] = ''
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan = plan[
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region',
         'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'],
        as_index=False)['Объем, шт'].sum()
    return plan


def get_customer_plan_tander(sheet, file, chain, product_md):
# def get_customer_plan_tander(file, product_md):
    """
    The function collects the promo plan from raw file with specified path.
    Then processes it using masterdata file Product_MD.csv.

    :param file: str; path of file with promo plan for Chain='MAGNIT'
    :param product_md: pd.DataFrame; data frame with required information from masterdata file Product_MD.csv

    :return: pd.DataFrame; processed data frame with information about promo for Chain='MAGNIT'
    """
    plan = pd.read_excel(file)
    plan.drop(columns=['РЦ', 'Ship To'], inplace=True)
    # expected column for customer plan was renamed from 'customer code' to 'Сustomer code'
    # renamed according to last confirmation
    plan.rename(columns={'Сustomer code': 'Techbillto', 'sap код': 'SAPCode', 'Shipment_Start': 'Shipment Start',
                         'Shipment_End': 'Shipment End', 'Store_Start': 'Store Start', 'Store_End': 'Store End'},
                inplace=True)
    # plan.rename(columns={'customer code': 'Techbillto', 'sap код': 'SAPCode', 'Shipment_Start': 'Shipment Start',
    #                      'Shipment_End': 'Shipment End', 'Store_Start': 'Store Start', 'Store_End': 'Store End'},
    #             inplace=True)
    plan = plan.merge(product_md, left_on='SAPCode', right_on='SKU')  # product code from Product_MD
    plan['BP_WnitWeight'] = plan['BP_WnitWeight'].astype('float')
    plan['Объем, шт'] = (plan['План, кг'] / plan['BP_WnitWeight']) * 1000
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan['Chain'] = 'MAGNIT'
    plan['Region'] = ''
    plan = plan[
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region',
         'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'],
        as_index=False)['Объем, шт'].sum()
    return plan


def get_customer_plan_dixy(sheet_name, file, chain, product_md):
# def get_customer_plan_dixy(file, product_md):
    """
    The function collects the promo plan from raw file with specified path.
    Then processes it using masterdata file Product_MD.csv.

    :param file: str; path of file with promo plan for Chain='DIXY'
    :param product_md: pd.DataFrame; data frame with required information from masterdata file Product_MD.csv

    :return: pd.DataFrame; processed data frame with information about promo for Chain='DIXY'
    """
    plan = pd.read_excel(file)
    plan.drop(columns=['Год', 'ЦФО (Москва, Центр), СЗФО (СПб)', 'Наименование продукта'], inplace=True)
    plan.rename(columns={'Объемы, шт': 'Объем, шт', 'Код SAP Данон': 'SAPCode'}, inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode',
                      right_on='SKU')  # product code from Product_MD
    plan['Chain'] = 'DIXY'
    plan['Region'] = ''
    plan.rename(columns={'techbillto': 'Techbillto'}, inplace=True)
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan = plan[
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region',
         'Объем, шт']]
    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')
    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'],
        as_index=False)['Объем, шт'].sum()
    return plan


def get_customer_plan_chain_x5(sheet_name, file, chain, product_md):
    plan = pd.read_excel(file, sheet_name=sheet_name)
    plan.drop(columns=['Неделя', 'Sku', 'Скидка'], inplace=True)
    plan.dropna(axis=0, subset=['Объем, шт'], inplace=True)
    plan = plan.merge(product_md[['SKU', 'PRODUCT_CODE']], left_on='SAPCode',
                      right_on='SKU')  # product code from Product_MD
    plan['Region'] = plan['Регион'] + ' ' + plan['Формат']
    plan['Chain'] = chain.upper()
    plan['Techbillto'] = ''
    plan['PRODUCT_CODE'] = plan['PRODUCT_CODE'].astype(str).str.zfill(4)
    plan = plan[
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region',
         'Объем, шт']]

    # Filter out non-numeric volume forecasts
    if plan.dtypes['Объем, шт'] == 'O':
        plan.drop(plan['Объем, шт'].str.isalpha().dropna().index.tolist(), axis=0, inplace=True)
    plan['Объем, шт'] = plan['Объем, шт'].astype('float')

    # Combine multiple SKUs to a single line per PRODUCT_CODE
    plan = plan.groupby(
        ['Shipment Start', 'Shipment End', 'Store Start', 'Store End', 'PRODUCT_CODE', 'Chain', 'Techbillto', 'Region'],
        as_index=False)['Объем, шт'].sum()

    return plan
