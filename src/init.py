
import features


def gen_processed_prices():

    prices = features.get_prices()

    # srez = prices[(prices['Date'] > '2020-07-31') & (prices['Date'] < '2020-10-05')]
    # print(srez)

    prices['Aver Price'] = prices['Aver Price'].apply(lambda x: str(x).replace(',', '.'))
    prices['Aver Price'] = prices['Aver Price'].astype(float)

    # srez = prices[(prices['Date'] > '2020-07-31') & (prices['Date'] < '2020-10-05')]
    # print(srez)

    prices.dropna(inplace=True)
    prices.to_csv(r'../data/processed/shelf_price.csv', index=False)


if __name__ == '__main__':
    gen_processed_prices()
